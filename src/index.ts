import 'reflect-metadata';

import fastify from 'fastify';

import cors from '@fastify/cors';
import helmet from '@fastify/helmet';
import multipart from '@fastify/multipart';
import compression from '@fastify/compress';

import { config } from './config';

import { GraphQL } from './graphql';
import { Database } from './models';
import { Services } from './services';
import { Passport } from './passport';
import { logger } from './services/log.service';


async function bootstrap() {
  logger.info({ message: 'Initalizing the server' })
  const app = fastify()
    .register(compression)
    .register(cors, { optionsSuccessStatus: 200 })
    .register(helmet, { hsts: false  })
    .register(multipart, {  })

  logger.info({ message: 'Starting up services' })
  await Promise.all([
    Database.initialize(config.db, config.isProduction),
    GraphQL.initialize(app, config.isProduction),
    Passport.initialize(app),
    Services.initialize()
  ]);
  console.log();



  app.listen({ port: config.server.port, host: config.server.interface }, (error, address) => { 
    if (error) {
      logger.error({ message: 'Error while starting the Fastify server', error })
      process.exit();
    }
    
    logger.info({ message: `Server is listening on ${address}` })
  });
}

bootstrap();