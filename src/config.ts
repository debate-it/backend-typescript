import * as dotenv from 'dotenv';

import * as fs from 'fs'

const production = process.env.NODE_ENV === 'production';

if (!production) {
  dotenv.config();
}

const readSecret = (key: string) => {
  if (production) {
    return readConfigFile(process.env[`${key}_FILE`]);
  }

  return process.env[key];
}

export const config = {
  isProduction: production,

  db: {
    url: readSecret('MONGO_URL'),
  },

  redis: {
    url: process.env.REDIS_URL,
  },

  search: {
    host: process.env.MEILISEARCH_URL,
    key: process.env.MEILISEARCH_KEY
  },

  server: {
    port: parseInt(process.env.PORT),

    interface: production
      ? '0.0.0.0'
      : '127.0.0.1',

    url: production
      ? 'https://api.debateit.io'
      : 'http://localhost:9000'
  },

  mailgun: {
    api: process.env.MAILGUN_API_KEY,
    domain: process.env.MAILGUN_DOMAIN,
    username: process.env.MAILGUN_USERNAME
  },

  secrets: {
    JWT: readSecret('JWT_SECRET')
  },

  firebase: {
    credentials: JSON.parse(readConfigFile(process.env.FIREBASE_CREDENTIALS_PATH))
  },

  constants: {
    dayInSeconds: 5 * 60 * 1e3,
    passwordResetTimeout: 24 * 3600 * 1e3
  },

  oauth: {
    google: {
      id: process.env.GOOGLE_OAUTH_CLIENT_ID,
      secret: process.env.GOOGLE_OAUTH_SECRET
    },

    facebook: {
      id: process.env.FACEBOOK_OAUTH_CLIENT_ID,
      secret: process.env.FACEBOOK_OAUTH_SECRET
    }
  },

  monitor: {
    loki: {
      url: process.env.LOKI_URL
    }
  }
};

function readConfigFile(path: string): string {
  return fs.readFileSync(path, 'utf-8');
}
