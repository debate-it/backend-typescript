import Container from "typedi";

import { config } from "@/config";
import { Database } from "@/models";

import { SearchService } from "@/services/search.service";

import { UserRepository } from "@/repositories/user.repository";
import { DiscussionRepository } from "@/repositories/discussion.repository";


const search = Container.get(SearchService);

const users = Container.get(UserRepository);
const discussions = Container.get(DiscussionRepository);

async function run() {
  await Database.initialize(config.db);

  await search.clean();
  await search.initialize();

  await search.initialize();

  console.log(':: Creating indexes for users...');
  await search.addUsers(await users.find({}));
  console.log('\t=> Finished indexes for users');

  console.log(':: Creating indexes for discussions...');
  await search.addDiscussions(await discussions.find({}));
  console.log('\t=> Finished indexes for discussions');

  process.exit();
}

run();
