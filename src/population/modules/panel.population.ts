import Container from "typedi";

import { Chance } from 'chance';
import { Types } from 'mongoose';

import { User, UserModel } from "@/models/user";
import { Panel, PanelModel } from "@/models/panel";
import { PanelRequest, PanelRequestModel, RequestStatus } from "@/models/request";

import { RequestRepository } from "@/repositories/request.repository";
import { PanelRepository } from '@/repositories/panel.repository';

import { Discussions } from "./discussion.population";


export namespace Panels {
  const requests = Container.get(RequestRepository);
  const service = Container.get(PanelRepository);

  const chance = new Chance();

  export async function create(n: number) {
    const batch = await PanelRequestModel
      .aggregate<PanelRequest>()
      .match({ closed: false })
      .sample(n);

    // await PanelRequestModel.findByIdAndUpdate(request._id, { $set: { closed: true } });

    for (const request of batch) {
      const recipients = await UserModel.find({
        _id: {
          $in: request.recipients.map(r => (r.user as Types.ObjectId).toHexString())
        }
      });

      for (const recipient of recipients) { 
        await requests.resolvePanelRequest(
          request._id.toHexString(),
          RequestStatus.accepted,
          recipient,
          chance.sentence({ words: 100, punctuation: true })
        );
      }
    }
  }

  export async function vote() {
    const [voter] = await UserModel.aggregate<User>().sample(1);
    const [target] = await PanelModel.aggregate<Panel>().sample(1);

    const side = Math.floor(Math.random() * target.participants.length);

    return await service.vote(target._id.toHexString(), voter, side);
  }
}