import Container from 'typedi';
import { Chance } from 'chance';

import { User, UserModel, } from '@/models/user';
import { Debate, DebateModel } from "@/models/debate";
import { DebateRequest, DebateRequestModel, RequestStatus } from '@/models/request';

import { DebateRepository } from '@/repositories/debate.repository';
import { Discussions } from './discussion.population';
import { CategoryModel } from '@/models';

export namespace Debates {
  const service = Container.get(DebateRepository);
  const chance = new Chance();

  export async function create(n) {
    const batch = await DebateRequestModel
      .aggregate<DebateRequest>()
      .match({ status: RequestStatus.submitted })
      .sample(n);

    for (const request of batch) {
      request.info.category = await CategoryModel.findById(request.info.category);

      const debate = await service.createFromRequest(request);

      debate.created = new Date(Date.now() - Math.random() * 10000);

      await debate.save();
    }

  }

  export async function vote() {
    const [voter] = await UserModel.aggregate<User>().sample(1);
    const [target] = await DebateModel.aggregate<Debate>().sample(1);

    const side = Math.random() > 0.5 ? 0 : 1;

    return await service.vote(target._id.toHexString(), side, voter);
  }

  export async function addArgument() {
    const [debate] = await DebateModel
      .aggregate<Debate>()
      .match({ status: "active" })
      .sample(1);

    const [left, right] = await Promise.all([
      UserModel.findById(debate.participants[0].user),
      UserModel.findById(debate.participants[1].user),
    ]);

    await service.addArgument(debate._id.toHexString(), left, chance.sentence({ words: 20 + Math.floor(Math.random() * 80), punctuation: true }));
    await service.addArgument(debate._id.toHexString(), right, chance.sentence({ words: 20 + Math.floor(Math.random() * 80), punctuation: true }));
  }
}