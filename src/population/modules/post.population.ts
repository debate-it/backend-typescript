import Container from 'typedi';
import { Chance } from 'chance';

import { User, UserModel } from '@/models/user';
import { Post, PostAlignmentSide, PostModel}  from '@/models/post';

import { PostRepository } from '@/repositories/post.repository';

import { Discussions } from './discussion.population';


export namespace Posts {
  const service = Container.get(PostRepository);
  
  const chance = new Chance();

  export async function create(n: number) {
    const authors = await UserModel.aggregate<User>().sample(n);

    for (const author of authors) {
      await service.initiate(author, { 
        discussion: {
          ...Discussions.shallow(),
          topic: chance.sentence({ 
            words: Math.floor(Math.random() * 80 + 50), punctuation: true
          }),
        },
      });
    }

  }

  export async function vote() {
    const [target] = await PostModel.aggregate<Post>().sample(1);
    const [voter] = await UserModel.aggregate<User>().sample(1);

    service.align(voter, {
      id: target._id.toHexString(),
      alignment: Math.random() > 0.5
        ? PostAlignmentSide.agree
        : PostAlignmentSide.disagree
    });
  }
}