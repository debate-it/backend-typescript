import Container from 'typedi';
import { Chance } from 'chance';

import { User, UserModel } from '@/models/user';
import { CategoryModel, DiscussionCategory } from '@/models';

import { AuthService } from '@/services';

import { SubscriptionRepository } from '@/repositories/subscription.repository';
import { SearchService } from '@/services/search.service';
import { UserRoleModel } from '@/models/user/user.role';


export namespace Users {
  const auth = Container.get(AuthService);
  const subscription = Container.get(SubscriptionRepository);
  const search = Container.get(SearchService);
  
  const chance = new Chance();

  export async function create(username?: string, roles: string[] = []) {
    const userRoles = await UserRoleModel.find({ name: { $in: roles } });

    const user = await UserModel.create({
      username: username ?? chance.sentence({ words: 2 }).split(' ').join('.'),
      email: chance.email(),
      info: {
        about: chance.sentence(),
        firstName: chance.first(),
        lastName: chance.last(),
        country: chance.country(),
        education: chance.sentence({ words: 3 }),
        occupation: chance.profession(),
        politics: chance.word(),
        religion: chance.word()
      },
      auth: {
        active: true,
        activationCode: 'CODE',
        password: await auth.hash("password"),
        roles: userRoles
      }
    });

    await search.addUsers([ user ]);

    return user;
  }

  export async function follow() {
    const [follower, target] = await UserModel.aggregate<User>().sample(2);

    follower.id = follower._id.toHexString();
    target.id = target._id.toHexString();

    if (follower._id.equals(target._id)) {
      return null;
    }

    try {
      return subscription.followUser(follower, target._id.toHexString());
    }
    catch {
      return null;
    }
  }

  export async function followUsers(user: User) {
    const targets = await UserModel.aggregate<User>().sample(10);

    for (const target of targets) {
      target.id = target._id.toHexString();

      if (target._id.equals(user._id)) {
        continue;
      }

      try {
        await subscription.followUser(user, target._id.toHexString());
      } catch {
        continue;
      }
    }
  }

  export async function addFollowers(user: User) {
    const followers = await UserModel.aggregate<User>().sample(20);

    for (const follower of followers) {
      follower.id = follower._id.toHexString();

      if (follower._id.equals(user._id)) {
        continue;
      }

      try {
        await subscription.followUser(follower, user.id);
      } catch {
        continue;
      }
    }
  }

  export async function followCategories(user: User) {
    const categories = await CategoryModel.aggregate<DiscussionCategory>().sample(3);

    for (const category of categories) {
      await subscription.followCategory(user, category._id.toHexString());
    }
  }

  export async function roles() {
    await UserRoleModel.create({ name: 'admin' });
  }
}