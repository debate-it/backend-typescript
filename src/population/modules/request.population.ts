import Container from 'typedi';
import { Chance } from 'chance';

import { User, UserModel } from "@/models/user";
import { Discussions } from './discussion.population';

import { RequestRepository } from '@/repositories/request.repository';


export namespace Requests {
  const service = Container.get(RequestRepository);
  
  const chance = new Chance();

  export async function debate() {
    const [initiator, recipient] = await UserModel.aggregate<User>().sample(2);

    return service.initiateDebate(initiator, {
      recipient: recipient._id.toHexString(),
      discussion: Discussions.shallow()
    });
  }

  export async function panel() {
    const [initiator, ...recipients] = await UserModel.aggregate<User>().sample(10);

    return service.initiatePanel(initiator, {
      discussion: Discussions.shallow(),
      recipients: recipients.map(r => r._id.toHexString()),
      position: chance.sentence({ words: 100, punctuation: true })
    });
  }
}