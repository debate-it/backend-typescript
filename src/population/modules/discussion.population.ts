import Container from 'typedi';

import { Chance } from 'chance';

import { SearchService } from '@/services/search.service';

import { 
  Discussion, 
  DiscussionCategory, 
  DiscussionModel, 
  DiscussionSize, 
  CategoryModel,
  TagModel 
} from '@/models/dicsussion';
import { DocumentType } from '@typegoose/typegoose';


export namespace Discussions {
  export const categories = [
    { name: "Entertainment", description: "Entertainment Category" },
    { name: "Politics", description: "Politics Category" },
    { name: "Science", description: "Science Category" },
    { name: "Theology", description: "Theology Category" },
    { name: "Sports", description: "Sports Category" },
    { name: "Business", description: "Business Category" },
    { name: "Philosophy", description: "Philosophy Category" },
    { name: "Other", description: "Other Category" }
  ];

  const chance = new Chance();

  const search = Container.get(SearchService);

  export async function create() {
    const [category] = await CategoryModel.aggregate<DiscussionCategory>().sample(1);

    return {
      topic: chance.sentence({ words: 20 + Math.random() * 80, punctuation: true }),
      category: category,
      size: chance.integer({ min: 0, max: DiscussionSize.long }),
      tags: await Promise.all([ tag(), tag(), tag() ])
    };
  }

  export async function fake() {
    const [category] = await CategoryModel.aggregate<DiscussionCategory>().sample(1);

    return {
      topic: chance.sentence({ words: 20 + Math.floor((Math.random() * 80)), punctuation: true }),
      category: category,
      size: chance.integer({ min: 0, max: DiscussionSize.long }),
      tags: chance.unique(chance.word, 3, { syllables: 3 })
    };
  }
  
  export function shallow() {
    return {
      topic: chance.sentence({ words: 20 + Math.floor((Math.random() * 80)), punctuation: true }),
      category: Discussions.categories[Math.floor(Math.random() * Discussions.categories.length)].name,
      size: chance.integer({ min: 0, max: DiscussionSize.long }),
      tags: chance.unique(chance.word, 3, { syllables: 3 })
    };
  }

  export async function randomizeDates() {
    const docs = await DiscussionModel.find();

    for (const doc of docs) {
      doc.created = new Date(Date.now() - Math.floor(Math.random() * 10 * 24 * 3600 * 1000));
    }

    await Promise.all(docs.map(doc => doc.save()));
  }

  export async function createCategories () {
    await CategoryModel.insertMany(categories);
  }

  async function tag() {
    const name = `sample-tag-${chance.natural({ max: 10 })}`; 
    const tag = await TagModel.findOne({ name });

    if (!tag) {
      return TagModel.create({ name });
    }

    return tag;
  }

  export async function index(item: DocumentType<Discussion>) {
    await search.addDiscussions([ item ]);

    return item;
  }
}