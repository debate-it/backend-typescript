import Container from 'typedi';
import { Chance } from 'chance';

import { User, UserModel } from "@/models/user";
import { Post, PostModel } from "@/models/post";
import { Debate, DebateModel } from "@/models/debate";

import { CommentRepository } from '@/repositories/comment.repository';

import { Comment, CommentModel, CommentTargetType } from "@/models/comment";
import { Challenge } from '@/models/challenge';

export namespace Comments {
  const service = Container.get(CommentRepository);
  const chance = new Chance();

  export async function create() {
    const model = pickType();
    const [author] = await UserModel.aggregate<User>().sample(1);
    const [target] = await findTarget(model);

    const content = chance.sentence({ words: Math.floor(Math.random() * 100 + 50), punctuation: true });

    return await CommentModel.create({ author, target, targetModel: model, content });
  }

  export async function vote() {
    const [voter] = await UserModel.aggregate<User>().sample(1);
    const [target] = await CommentModel.aggregate<Comment>().sample(1);

    return service.vote(target._id.toHexString(), voter);
  }

  function pickType() {
    return Math.random() > 0.5
      ? CommentTargetType.Debate
      : CommentTargetType.Post;
  }

  function findTarget(model: CommentTargetType) {
    switch (model) {
      case CommentTargetType.Debate:
        return DebateModel.aggregate<Debate>().sample(1);

      case CommentTargetType.Challenge:
        return PostModel.aggregate<Challenge>().sample(1);

      case CommentTargetType.Post:
        return PostModel.aggregate<Post>().sample(1);
    }
  }
}