import { ChallengeStatus, Database } from '@/models';
import { config } from '@/config';

import { Users } from './modules/user.population';
import { Posts } from './modules/post.population';
import { Panels } from './modules/panel.population';
import { Debates } from './modules/debate.population';
import { Requests } from './modules/request.population';
import { Comments } from './modules/comment.population';
import { Discussions } from './modules/discussion.population';
import { Services } from '@/services';
import Container from 'typedi';
import { SearchService } from '@/services/search.service';


async function batch(n: number, generator: () => Promise<any>) {
  const size = 20;
  const iterations = Math.ceil(n / size);

  for (let i = 0; i < iterations; i++) {
    await Promise.all(
      Array(size).fill(null).map(generator)
    )
  }
}

async function main() {
  const connection = await Database.initialize(config.db);
  await connection.connection.db.dropDatabase();

  const search = Container.get(SearchService);
  await search.clean();

  await Discussions.createCategories();
  console.log(':: Created categories');

  await Users.roles();
  console.log(':: Created user roles');

  await Promise.all(Array(100).fill(null).map(() => Users.create()));
  console.log(':: Created users');
  
  const users = await Promise.all([
    Users.create('nowem.iv', ['admin']),
    Users.create('shaya', ['admin']),

    ...Array(10).fill(null).map((_, idx) => Users.create(`test.user.${idx + 1}`))
  ]);
  
  console.log(':: Added pre-configured users');

  await Promise.all([
    ...users.map(user => Users.addFollowers(user)),
    ...users.map(user => Users.followUsers(user)),
    ...users.map(user => Users.followCategories(user))
  ]);

  console.log(':: Added subscriptions for pre-configured users');

  await batch(100, Requests.debate);
  console.log(':: Created debate requests');
  await batch(100, Requests.panel);
  console.log(':: Created panel requests');


  await Debates.create(50);
  await Panels.create(50);
  await Posts.create(50);
  await Discussions.randomizeDates();
  console.log(':: Created discussions');

  await batch(500, Comments.create);
  console.log(':: Created comments');

  for (let i = 0; i < 150; i++) {
    await Debates.addArgument();
  }
  console.log(':: Added arguments');

  await batch(150, Posts.vote);
  await batch(150, Debates.vote);
  await batch(150, Comments.vote);
  console.log(':: Added votes');

  await batch(150, Users.follow);
  console.log(':: Added follows');

  console.log(':: Population population finished');

  process.exit();
}

main();