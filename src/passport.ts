import jwt from '@fastify/jwt';
import oauth, { OAuth2Namespace } from '@fastify/oauth2';

import { config } from './config';
import { UserModel } from './models/user';
import { FastifyInstance } from 'fastify';
import { logger } from './services/log.service';

interface JWTPayload {
  id: string;
}

declare module 'fastify' {
  interface FastifyInstance {
    googleOAuth: OAuth2Namespace;
    facebookOAuth: OAuth2Namespace;
  }
}



export namespace Passport {
  export function initialize(fastify: FastifyInstance) {
    fastify.register(jwt, { secret: config.secrets.JWT }); 

    fastify.addHook('onRequest', async (request, reply) => {
      if (!request.headers.authorization) {
        return;
      }

      try {
        const payload = await request.jwtVerify();

        // Inject the user 
        request.user = await UserModel
          .findById((payload as JWTPayload).id);
      } catch (err) {
        // Redirect auth error handling to GraphQL:
        //  1. If token is not present, resolvers will throw an error
        //  2. Recover resolver will manage to issue a new token
        return;
      }
    });

    logger.info({ message: 'Passport is initialized' })
  }

  function setupGoogleOAuth(fastify: FastifyInstance) {
    fastify.register(oauth, {
      name: 'googleOAuth',

      scope: ['profile', 'email'],

      credentials: {
        client: config.oauth.google,
        auth: oauth.GOOGLE_CONFIGURATION
      },

      startRedirectPath: '/oauth/google',
      callbackUri: `${config.server.url}/oauth/google/callback`
    });

    fastify.get('/oauth/google/callback', async (request, reply) => {
      const token = await fastify.googleOAuth.getAccessTokenFromAuthorizationCodeFlow(request);
    });

  }

  function setupFacebookOAuth(fastify: FastifyInstance) {
    fastify.register(oauth, {
      name: 'facebookOAuth',

      scope: [''],

      credentials: {
        client: config.oauth.facebook,
        auth: oauth.FACEBOOK_CONFIGURATION
      },

      startRedirectPath: '/oauth/facebook',
      callbackUri: `${config.server.url}/oauth/facebook/callback`
    });

    fastify.get('/oauth/facebook/callback', async (request, reply) => {
      const token = await fastify.facebookOAuth.getAccessTokenFromAuthorizationCodeFlow(request);
    })
  }
}
