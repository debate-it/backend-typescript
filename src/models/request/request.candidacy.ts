import autopopulate from 'mongoose-autopopulate';

import { getDiscriminatorModelForClass, getModelForClass, Index, ModelOptions, plugin, prop as Property, Ref } from "@typegoose/typegoose";
import { Field, ID, InterfaceType, ObjectType } from "type-graphql";

import { User } from "@/models/user";
import { BaseModel } from "@/models/base.model";

import { Request, RequestStatus } from "./request";

enum RequestCandidacyType {
  Debate = 'DebateRequestCandidacy',
  Panel = 'PanelRequestCandidacy'
}

@Index({ request: 1, created: -1 })
@plugin(autopopulate)
@InterfaceType({ resolveType: (value: RequestCandidacy) => value.kind })
@ModelOptions({ schemaOptions: { discriminatorKey: 'kind' } })
export class RequestCandidacy extends BaseModel {

  public readonly kind: RequestCandidacyType;
  
  @Field(() => ID)
  public readonly id: string;


  @Field(() => Date)
  @Property({ default: Date.now })
  public created?: Date;

  @Property({ required: true, ref: () => Request })
  public request: Ref<Request>

  @Field(() => User)
  @Property({ required: true, autopopulate: true, ref: () => User })
  public candidate: Ref<User>;

  @Field(() => RequestStatus)
  @Property({ enum: RequestStatus, default: RequestStatus.submitted, type: String })
  public status?: RequestStatus;
}

@ObjectType({ implements: RequestCandidacy })
export class DebateRequestCandidacy extends RequestCandidacy {
}

@ObjectType({ implements: RequestCandidacy })
export class PanelRequestCandidacy extends RequestCandidacy {
  @Property({ required: true, trim: true })
  public argument: string;
}

export const RequestCandidacyModel = getModelForClass(RequestCandidacy);
export const PanelRequestCandidacyModel = getDiscriminatorModelForClass(RequestCandidacyModel, PanelRequestCandidacy);
export const DebateRequestCandidacyModel = getDiscriminatorModelForClass(RequestCandidacyModel, DebateRequestCandidacy);
