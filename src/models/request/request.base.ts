import { Field, ObjectType } from "type-graphql";
import { ModelOptions, prop as Property, Ref } from '@typegoose/typegoose';
import { DiscussionCategory, DiscussionSize, DiscussionTag } from "../dicsussion";

@ObjectType()
@ModelOptions({ schemaOptions: { _id: false }})
export class RequestBase {
  @Field()
  @Property({ required: true })
  public topic: string;

  @Field(() => DiscussionSize)
  @Property({ required: true, enum: DiscussionSize, type: Number })
  public size: DiscussionSize;

  @Field(() => DiscussionCategory)
  @Property({ required: true, autopopulate: true, ref: () => DiscussionCategory })
  public category: Ref<DiscussionCategory>;

  @Field(() => [DiscussionTag])
  @Property({ default: () => [], autopopulate: true, ref: () => DiscussionTag })
  public tags?: Ref<DiscussionTag>[]
}