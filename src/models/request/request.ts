import autopopulate from 'mongoose-autopopulate';

import { Field, ID, Int, InterfaceType, ObjectType, registerEnumType } from 'type-graphql';
import { getDiscriminatorModelForClass, getModelForClass, Index, ModelOptions, plugin, prop as Property, Ref } from '@typegoose/typegoose';

import { User } from '@/models/user';
import { Panel } from '@/models/panel';
import { BaseModel } from '@/models/base.model';

import { RequestBase } from './request.base';

export enum OpenRequestType {
  Debate = 'Debate',
  Panel = 'Panel',
}

export enum RequestType {
  Debate = 'DebateRequest',
  Panel = 'PanelRequest',
  Open = 'OpenRequest'
}

export enum RequestStatus {
  submitted = 'submitted',
  accepted = 'accepted',
  rejected = 'rejected'
}

@InterfaceType({ resolveType: (value: Request) => value.kind })
@plugin(autopopulate)
export class Request extends BaseModel {

  public readonly kind: RequestType;

  @Field(() => ID)
  public readonly id: string;

  @Field(() => Date)
  @Property({ default: Date.now })
  public created?: Date;

  @Field(() => RequestBase)
  @Property({ required: true, type: () => RequestBase })
  public info: RequestBase;

  @Field(() => User)
  @Property({ required: true, autopopulate: true, index: true, ref: () => User })
  public initiator: Ref<User>;
}

@ObjectType({ implements: Request })
export class DebateRequest extends Request {
  @Field(() => RequestStatus)
  @Property({ enum: RequestStatus, default: RequestStatus.submitted })
  public status?: RequestStatus;

  @Field(() => User)
  @Property({ required: true, autopopulate: true, index: true, ref: () => User })
  public recipient: Ref<User>;
}

@ObjectType()
@Index({ user: -1, status: 1 })
@ModelOptions({ schemaOptions: { _id: false } })
export class PanelRequestRecipient {
  @Field(() => User)
  @Property({ required: true, autopopulate: true, index: true, ref: () => User })
  public user: Ref<User>;

  @Field(() => RequestStatus)
  @Property({ enum: RequestStatus, default: RequestStatus.submitted })
  public status?: RequestStatus;
}

@ObjectType({ implements: Request })
export class PanelRequest extends Request {
  @Field(() => Date, { nullable: true })
  @Property({ required: false })
  public expiration?: Date;

  @Field(() => Int)
  @Property({ default: 0 })
  public accepted?: number;

  @Field()
  @Property({ default: false })
  public closed?: boolean;

  @Property({ requried: true, ref: () => Panel })
  public panel: Ref<Panel>

  @Field(() => [PanelRequestRecipient])
  @Property({ required: true, type: () => PanelRequestRecipient })
  public recipients: PanelRequestRecipient[]
}

@ObjectType({ implements: Request })
export class OpenRequest extends Request {
  @Field(() => OpenRequestType)
  @Property({ required: true, enum: OpenRequestType })
  public type: OpenRequestType;

  @Field()
  @Property({ default: false })
  public resolved?: boolean;

  @Field(() => Int)
  @Property({ default: 0 })
  public candidacies?: number;
}


export const RequestModel = getModelForClass(Request, { schemaOptions: { discriminatorKey: 'kind' } });

export const OpenRequestModel = getDiscriminatorModelForClass(RequestModel, OpenRequest);
export const PanelRequestModel = getDiscriminatorModelForClass(RequestModel, PanelRequest);
export const DebateRequestModel = getDiscriminatorModelForClass(RequestModel, DebateRequest);

registerEnumType(RequestStatus, { name: 'RequestStatus' });
registerEnumType(OpenRequestType, { name: 'OpenRequestType' });
