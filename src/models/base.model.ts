import { Base } from "@typegoose/typegoose/lib/defaultClasses";
import { Types } from "mongoose";
import { Field, ID, ObjectType } from "type-graphql";

@ObjectType({ isAbstract: true })
export class BaseModel implements Base {
  public _id: Types.ObjectId;

  @Field(() => ID)
  public id: string
}
