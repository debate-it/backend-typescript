import { getModelForClass, prop as Property } from "@typegoose/typegoose";

export class UserRole {
  @Property()
  public name: string;
}

export const UserRoleModel = getModelForClass(UserRole);
