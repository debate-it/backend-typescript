import autopopulate from 'mongoose-autopopulate';

import { ModelOptions, plugin, prop as Property } from '@typegoose/typegoose';
import { UserRole } from './user.role';

@plugin(autopopulate)
@ModelOptions({
  schemaOptions: { _id: false }
})
export class UserAuth {
  @Property({ require: true })
  public password: string;

  @Property({ default: false })
  public active?: boolean = false;

  @Property({ required: true })
  public activationCode?: string;

  @Property({ required: false })
  public firebaseToken?: string;

  @Property({ default: 5 })
  public activationAttempts?: number = 5;

  @Property({ default: 3 })
  public activationResends?: number = 3;

  @Property({ ref: () => UserRole, autopopulate: true, default: () => [], required: false })
  public roles?: UserRole[];
}