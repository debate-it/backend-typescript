import { ModelOptions, prop as Property } from "@typegoose/typegoose";
import { Field, ObjectType } from "type-graphql";

@ObjectType({ simpleResolvers: true })
@ModelOptions({
  schemaOptions: { _id: false }
})
export class UserProgress {
  @Field()
  @Property({ default: 1 })
  level: number;

  @Field()
  @Property({ default: 0 })
  earned: number;

  @Field({ nullable: true })
  @Property({ default: 50, required: false })
  needed?: number;
}
