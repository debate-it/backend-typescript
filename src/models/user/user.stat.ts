import autopopulate from 'mongoose-autopopulate';

import { ModelOptions, plugin, prop as Property, Ref } from '@typegoose/typegoose';
import { Field, Int, ObjectType } from 'type-graphql';

import { DiscussionCategory } from '@/models/dicsussion/discussion.category';

@ObjectType()
@plugin(autopopulate)
@ModelOptions({
  schemaOptions: { _id: false }
})
export class UserStat {
  @Field(() => Int)
  @Property({ default: 0 })
  public points: number;

  @Field(() => Int)
  @Property({ default: 0 })
  public followers: number;

  @Field(() => Int)
  @Property({ default: 0 })
  public wins: number;

  @Field(() => DiscussionCategory, { nullable: true })
  @Property({ autopopulate: true, ref: () => DiscussionCategory })
  public bestCategory?: Ref<DiscussionCategory>
}
