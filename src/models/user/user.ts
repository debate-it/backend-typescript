import autopopulate from 'mongoose-autopopulate';

import { Field, ObjectType, Root } from 'type-graphql';
import { DocumentType, getModelForClass, Index, plugin, prop as Property } from '@typegoose/typegoose';

import { UserAuth } from './user.auth';
import { UserInfo } from './user.info';
import { UserStat } from './user.stat';
import { BaseModel } from '../base.model';
import { UserProgress } from './user.progress';


@plugin(autopopulate)
@ObjectType()
@Index({ _id: 1, blocked: -1 })
export class User extends BaseModel{
  @Field()
  @Property({ requried: true, unique: true })
  public username: string;

  @Property({ required: true, unique: true })
  public email: string;

  @Field(() => UserProgress)
  @Property({ type: () => UserProgress, default: () => new UserProgress() })
  public progress: UserProgress;

  @Field(() => UserInfo)
  @Property({ type: () => UserInfo, default: () => new UserInfo() })
  public info?: UserInfo;

  @Field(() => UserStat)
  @Property({ type: () => UserStat, default: () => new UserStat() })
  public stat?: UserStat;

  @Property()
  public auth: UserAuth;

  @Property({ default: false, index: true })
  public blocked?: boolean;
  
  @Field(() => String, { nullable: true })
  fullname(@Root() user: DocumentType<User>): string {
    return `${user.info?.firstName ?? ''} ${user.info?.lastName ?? ''}`.trim() || null;
  }
}

export const UserModel = getModelForClass(User);
