import { ModelOptions, prop as Property } from '@typegoose/typegoose';
import { Field, Int, ObjectType } from 'type-graphql';

// avatar - https://i.pravatar.cc/250

@ObjectType({ simpleResolvers: true })
@ModelOptions({
  schemaOptions: { _id: false }
})
export class UserInfo {
  @Property()
  public firstName?: string;

  @Property()
  public lastName?: string;

  @Field({ nullable: true })
  @Property()
  public about?: string;

  @Field({ nullable: true}) 
  @Property()
  public avatar?: string;

  @Field({ nullable: true}) 
  @Property()
  public education?: string;
  
  @Field({ nullable: true}) 
  @Property()
  public occupation?: string;

  @Field({ nullable: true}) 
  @Property()
  public country?: string;

  @Field({ nullable: true}) 
  @Property()
  public politics?: string;

  @Field({ nullable: true}) 
  @Property()
  public religion?: string;
}
