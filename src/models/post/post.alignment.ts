import { ModelOptions, prop as Property } from '@typegoose/typegoose';
import { Field, Int, ObjectType, registerEnumType } from 'type-graphql';

export enum PostAlignmentSide {
  agree = 'agree',
  disagree = 'disagree'
}

@ObjectType()
@ModelOptions({ schemaOptions: { _id: false } })
export class PostAlignment {
  @Field(() => Int)
  @Property({ default: 0 })
  public agree: number;

  @Field(() => Int)
  @Property({ default: 0 })
  public disagree: number;
}

registerEnumType(PostAlignmentSide, { name: 'PostAlignmentSide' });
