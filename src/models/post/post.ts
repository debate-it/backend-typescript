import autopopulate from 'mongoose-autopopulate';

import { Field, ObjectType } from 'type-graphql';
import { getDiscriminatorModelForClass, Index, plugin, prop as Property, Ref } from '@typegoose/typegoose';

import { User } from '@/models/user';
import { Discussion, DiscussionModel } from '@/models/dicsussion/discussion';

import { PostAlignment } from './post.alignment';

@ObjectType({ implements: Discussion })
@plugin(autopopulate)
@Index({ author: 1, created: -1 })
export class Post extends Discussion {
  public __typename = 'Post'

  @Field(() => User)
  @Property({ required: true, autopopulate: true, index: true, ref: () => User })
  public author: Ref<User>;

  @Field(() => PostAlignment)
  @Property({ type: PostAlignment, default: () => new PostAlignment() })
  public alignment?: PostAlignment;
}

export const PostModel = getDiscriminatorModelForClass(DiscussionModel, Post);