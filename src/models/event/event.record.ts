import { DocumentType, ModelOptions, prop as Property } from "@typegoose/typegoose";
import { Field, ObjectType, registerEnumType } from "type-graphql";

export const MAX_LENGTH_EXTRA = 80;

export enum EventType {
  DebateRequestSent,
  DebateCreated,
  DebateArgumentAdded,
  DebateFinished,

  PanelRequestSent,
  PanelRequestAccepted,
  PanelStarted,
  PanelFinished,

  RequestRejected,

  ChallengeResponse,
  ChallengeFinished,
  ChallengeWinnerChosen,

  UserFollow,
  UserLevelUp,

  CommentCreated,

  Vote,

  RequestCandidacySubmitted
}

export enum EventTrigger {
  Debate = 'Debate',
  Post = 'Post',
  Panel = 'Panel',
  Challenge = 'Challenge',
  Request = 'Request',

  // User is the trigger for both requests and votes
  // as it is the user, who sends those and will be the
  // used in the notification link
  User = 'User',

  Comment = 'Comment'
}

@ModelOptions({ schemaOptions: { _id: false } })
export class EventExtra {
  @Property({ required: false })
  public username?: string;

  @Property({ required: false, trim: true, maxlength: MAX_LENGTH_EXTRA })
  public topic?: string;

  @Property({ required: false })
  public discussion?: EventTrigger;
}

@ObjectType()
@ModelOptions({ schemaOptions: { _id: false } })
export class EventRecord {
  @Field(() => EventType)
  @Property({ required: true, enum: EventType })
  public type: EventType;

  @Field(() => Date)
  @Property({ default: Date.now })
  public time?: Date;

  @Property({ required: true, type: EventExtra })
  public extra: EventExtra;
  
  @Field()
  @Property({ required: true, validate: validateLink })
  public link: string

  
  get title(): string {
    return getEventTitle(this);
  }

  get message(): string {
    return getEventMessage(this);
  }
}

export function getEventTitle(event: EventRecord | DocumentType<EventRecord>) {
  switch (event.type) {
    case EventType.UserFollow:
      return 'User followed you';
    case EventType.UserLevelUp:
      return 'You have reached a new level!';

    case EventType.RequestCandidacySubmitted:
      return 'New discussion candidacy submitted'; 


    case EventType.DebateRequestSent:
      return 'New debate request received';

    case EventType.DebateCreated:
      return 'New debate started';
      
    case EventType.DebateArgumentAdded:
      return 'Debate argument added';
    
    case EventType.DebateFinished:
      return 'Debate finished';


    case EventType.PanelRequestSent:
      return 'New panel request received';
    case EventType.PanelRequestAccepted:
      return 'Panel request has been accepted';
    case EventType.PanelStarted:
      return 'Panel has started';
    case EventType.PanelFinished:
      return 'Panel has finished';


    case EventType.RequestRejected:
      return 'Your request was rejected';
      

    case EventType.ChallengeResponse:
      return 'New challenge response';

    case EventType.ChallengeFinished:
      return 'Challenge has finished';



    case EventType.Vote:
      return 'New vote received';
      
    case EventType.CommentCreated:
      return 'New comment received';
  }
}

export function getEventMessage(event: EventRecord | DocumentType<EventRecord>) {
  switch (event.type) {
    case EventType.UserFollow:
      return `${event.extra.username} has followed you`;
      
    case EventType.UserLevelUp:
      return `Congratulations, you have reached a new level. Onto new achievments!`;

    case EventType.RequestCandidacySubmitted:
      return `${event.extra.username} has submitted a candidacy for your request '${event.extra.topic}'`; 


    case EventType.DebateRequestSent:
      return `${event.extra.username} has sent you a debate request`;

    case EventType.DebateCreated:
      return `Your debate '${event.extra.topic}' has started`;
      
    case EventType.DebateArgumentAdded:
      return `${event.extra.username} has submitted an argument for your debate '${event.extra.topic}'`;
    
    case EventType.DebateFinished:
      return `Your debate '${event.extra.topic}' has finished`;


    case EventType.PanelRequestSent:
      return `${event.extra.username} has sent you a panel request`;
    case EventType.PanelRequestAccepted:
      return `${event.extra.username} has accepted your panel request`;
    case EventType.PanelStarted:
      return `Your panel '${event.extra.topic}' has started`;
    case EventType.PanelFinished:
      return `Your panel '${event.extra.topic}' has finished`;


    case EventType.RequestRejected:
      return `${event.extra.username} has rejected your request`;


      
    case EventType.ChallengeResponse:
      return `${event.extra.username} has submitted response to your challenge '${event.extra.topic}'`;

    case EventType.ChallengeFinished:
      return `Your challenge '${event.extra.topic}' has finished`;



    case EventType.Vote:
      return `${event.extra.username} has voted on your ${event.extra.discussion.toLowerCase()} '${event.extra.topic}'`;
      
    case EventType.CommentCreated:
      return `${event.extra.username} has commented on your ${event.extra.discussion.toLowerCase()} '${event.extra.topic}'`;


  }
}

function validateLink(value: string): boolean {
  const [entity, id] = value.split('/');
  const regexp = new RegExp('[0-9a-fA-F]{24}', 'g');

  const valid = entity && id &&
    ['Debate', 'Post', 'Panel', 'Challenge', 'User', 'Request', 'Comment'].includes(entity) &&
    regexp.test(id);

  return valid;
}

registerEnumType(EventType, { name: 'EventType' })
