import { getModelForClass, Index, mongoose, prop as Property, Ref } from '@typegoose/typegoose';
import { Field, ObjectType } from 'type-graphql';

import { User } from '@/models/user';
import { BaseModel } from '@/models/base.model';

import { EventRecord } from './event.record';

// MAX_EVENT_RECORDS is a soft bound, it won't be checked
// by validation as some events might be written while the
// new bucket is still not written to the database.
export const MAX_EVENT_RECORDS = 300;


@ObjectType()
@Index({ to: -1, from: -1, target: -1 })
export class Event extends BaseModel {
  @Field(() => Date)
  @Property({ default: getEventBucketStartDate })
  public from?: Date;

  // `to` property is by default is the end of the day, but can be changed,
  // as the limit of the notifications can be reached before the day end,
  // it should be shifted and new event bucket should be created
  @Property({ default: getEventBucketEndDate })
  public to?: Date;

  @Property({ requried: true, ref: () => User})
  public target: Ref<User>;

  // One new as the event bucket is created only if there's at least one event
  // that doesn't fit into previous bucket
  @Property({ default: 1 })
  public new?: number;
  
  @Property({ default: 1 })
  public total?: number;

  @Property({ requried: true, type: EventRecord })
  public events: mongoose.Types.Array<EventRecord>;

}

export function getEventBucketStartDate() {
  const date = new Date(Date.now());

  date.setHours(0);
  date.setMinutes(0);
  date.setSeconds(0);

  return date;
}

export function getEventBucketEndDate() {
  const date = new Date(Date.now());

  date.setHours(23);
  date.setMinutes(59);
  date.setSeconds(59);

  return date;
}

export const EventModel = getModelForClass(Event);

