import autopopulate from 'mongoose-autopopulate';

import { ModelOptions, Ref, prop as Property, plugin } from '@typegoose/typegoose';
import { Field, Int, ObjectType } from 'type-graphql';

import { User } from '@/models/user';


@ObjectType()
@ModelOptions({
  schemaOptions: { _id: false }
})
@plugin(autopopulate)
export class DebateParticipant {
  @Field(() => Int)
  @Property({ default: 0 })
  public score?: number;

  @Field(() => User)
  @Property({ required: true, autopopulate: true, ref: () => User })
  public user: Ref<User>;
}
