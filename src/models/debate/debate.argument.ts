import { ModelOptions, prop as Property } from '@typegoose/typegoose';
import { Field, Int, ObjectType } from 'type-graphql';

@ObjectType()
@ModelOptions({
  schemaOptions: { _id: false }
})
export class DebateArgument {
  @Field(() => Int)
  @Property({ requried: true })
  public participant: number;

  @Field()
  @Property({ requried: true })
  public argument: string;

  @Field(() => Date, { defaultValue: Date.now }) // TODO: change after repopulating
  @Property({ default: Date.now })
  public time?: Date;
}
