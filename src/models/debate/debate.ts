import autopopulate from 'mongoose-autopopulate';

import { getDiscriminatorModelForClass, plugin, prop as Property } from '@typegoose/typegoose';
import { Field, ObjectType, registerEnumType } from 'type-graphql';


import { Discussion, DiscussionModel } from '@/models/dicsussion';
import { DebateArgument } from './debate.argument';
import { DebateParticipant } from './debate.participant';


export enum DebateStatus {
  active = 'active',
  voting = 'voting',
  finished = 'finished'
}

@ObjectType({ implements: Discussion })
@plugin(autopopulate)
export class Debate extends Discussion {
  public __typename = 'Debate'

  @Field(() => Date, { nullable: true })
  @Property({ required: false })
  public deadline?: Date;

  @Field(() => DebateStatus)
  @Property({ enum: DebateStatus, default: DebateStatus.active })
  public status: DebateStatus

  @Field(() => [DebateParticipant])
  @Property({ type: DebateParticipant, default: () => [], required: false })
  public participants?: DebateParticipant[]

  @Field(() => [DebateArgument])
  @Property({ type: DebateArgument, default: () => [], required: false })
  public arguments?: DebateArgument[];

  public get currentTurn() {
    return this.arguments.length % 2;
  }
}

export const DebateModel = getDiscriminatorModelForClass(DiscussionModel, Debate);


registerEnumType(DebateStatus, {
  name: 'DebateStatus'
});