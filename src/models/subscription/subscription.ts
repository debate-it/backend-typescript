import { getModelForClass, prop as Property, Ref } from "@typegoose/typegoose";

import { User } from "@/models/user";
import { DiscussionCategory } from "../dicsussion";
import { registerEnumType } from "type-graphql";

export type SubscriptionTarget = User | DiscussionCategory;
export enum SubscriptionTargetModel {
  User = 'User',
  Category = 'DiscussionCategory'
}

export class Subscription {
  public readonly id: string;
  
  @Property({ default: Date.now })
  public date?: Date;

  @Property({ required: true, index: true, ref: () => User })
  public follower: Ref<User>;

  @Property({ default: true })
  public active?: boolean;

  @Property({ required: true, index: true, refPath: 'subscriptionModel' })
  public subscription: Ref<SubscriptionTarget>

  @Property({ required: true, enum: SubscriptionTargetModel })
  public subscriptionModel: SubscriptionTargetModel;
}

export const SubscriptionModel = getModelForClass(Subscription);

registerEnumType(SubscriptionTargetModel, { name: 'SubscriptionType' });
