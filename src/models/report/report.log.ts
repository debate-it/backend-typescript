import autopopulate from 'mongoose-autopopulate';

import { ModelOptions, plugin, prop as Property, Ref } from "@typegoose/typegoose";
import { Field, ObjectType } from "type-graphql";
import { User } from "../user";

export enum ReportReason {
  hateSpeech = 'hate-speech',
  ruleViolation = 'rule-violation',
  spam = 'spam',
  other = 'other'
}

@ObjectType()
@plugin(autopopulate)
@ModelOptions({ schemaOptions: { _id: false }})
export class ReportLog {
  @Field()
  @Property({ required: true })
  public description: string;

  @Field(() => [ReportReason])
  @Property({ required: true, type: () => String, enum: ReportReason, default: [] })
  public reasons: ReportReason[];

  @Field(() => User)
  @Property({ required: true, ref: () => User, autopopulate: true })
  public author: Ref<User>;
  
  @Field(() => [User])
  @Property({ required: true, ref: () => User, autopopulate: true })
  public offenders: Ref<User>[];

  @Field(() => Date)
  @Property({ default: Date.now })
  public date?: Date;
}