import autopopulate from 'mongoose-autopopulate';

import { getModelForClass, Index, ModelOptions, plugin, prop as Property, Ref } from "@typegoose/typegoose";
import { Field, ObjectType, registerEnumType } from "type-graphql";

import { ReportLog, ReportReason } from "./report.log";

import { BaseModel } from "../base.model";
import { Discussion } from "../dicsussion";
import { User } from "../user";

export enum ReportResolutionDecision {
  none,
  warning,
  ban
}

@ObjectType()
@plugin(autopopulate)
@ModelOptions({ schemaOptions: { _id: false }})
export class ReportResolution {
  @Property({ required: true, ref: () => User, autopopulate: true })
  @Field(() => User)
  public user: User;

  @Property({ required: true })
  @Field(() => ReportResolutionDecision)
  public decision: ReportResolutionDecision;

  @Property({ required: false })
  @Field({ nullable: true })
  public comment?: string;
}


@ObjectType()
@plugin(autopopulate)
@Index({ resolved: -1, target: 1, "log.author": 1 })
@Index({ "resolution.user": 1, "resolution.decision": 1 })
export class Report extends BaseModel {
  @Field(() => Discussion)
  @Property({ required: true, ref: () => Discussion, autopopulate: true })
  public target: Ref<Discussion>;

  @Property({ default: false })
  public resolved?: boolean;

  @Field(() => [ReportLog])
  @Property({ default: [], type: () => ReportLog })
  public log?: ReportLog[];

  @Field(() => [ReportResolution])
  @Property({ default: [], type: () => ReportResolution })
  public resolution: ReportResolution[];
}

export const ReportModel = getModelForClass(Report);

registerEnumType(ReportReason, { name: 'ReportReason' });
registerEnumType(ReportResolutionDecision, { name: 'ReportResolutionDecision' });
