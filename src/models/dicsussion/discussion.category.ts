import { prop as Property, getModelForClass, ModelOptions } from '@typegoose/typegoose';
import { Field, Int, ObjectType } from 'type-graphql';
import { BaseModel } from '../base.model';

@ObjectType()
@ModelOptions({ schemaOptions: { _id: false } })
export class DiscussionCategoryStat {
  @Property({ default: 0 })
  @Field(() => Int)
  public discussions?: number;

  @Property({ default: 0 })
  @Field(() => Int)
  public followers?: number;
}

@ObjectType()
export class DiscussionCategory extends BaseModel {
  @Field()
  @Property({ required: true, unique: true })
  public name: string;
  
  @Field()
  @Property({ required: true })
  public description: string;

  @Property({ type: DiscussionCategoryStat, default: () => new DiscussionCategoryStat() })
  @Field(() => DiscussionCategoryStat)
  public stat: DiscussionCategoryStat;
}

export const CategoryModel = getModelForClass(DiscussionCategory);