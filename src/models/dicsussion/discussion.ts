import autopopulate from 'mongoose-autopopulate';
import { Document } from 'mongoose';

import { Field, ID, InterfaceType, registerEnumType } from 'type-graphql';
import { getModelForClass, Index, plugin, prop as Property, Ref } from '@typegoose/typegoose';

import { BaseModel } from '../base.model';

import { DiscussionTag } from './discussion.tag';
import { DiscussionCategory } from './discussion.category';


export enum DiscussionSize {
  short,
  medium,
  long
}

export enum DiscussionType {
  Debate = 'Debate',
  Challenge = 'Challenge',
  Post = 'Post'
}

const resolveType = async (value: Document) => {
  switch (value.get('kind')) {
    case 'Post':
      return import('../post').then(x => x.Post)
    case 'Panel':
      return import('../panel').then(x => x.Panel)
    case 'Debate':
      return import('../debate').then(x => x.Debate)
    case 'Challenge':
      return import('../challenge').then(x => x.Challenge)
  }

  throw new Error('Unknown discussion type')
}


@InterfaceType({ resolveType })
@plugin(autopopulate)

@Index({ topic: 'text', created: -1, blocked: 1 })
@Index({ category: 1, created: -1, blocked: 1 })
@Index({ tags: 1, created: -1, blocked: 1 })

@Index(
  { author: 1, created: -1, blocked: 1 },
  { partialFilterExpression: { author: { $exists: true } } }
)
@Index(
  { "participants.user": 1, created: -1, status: 1, blocked: 1 },
  { partialFilterExpression: { "participants.user": { $exists: true } } }
)
export class Discussion extends BaseModel {

  @Field(() => ID)
  public readonly id: string;

  @Field(() => Date)
  @Property({ default: Date.now, required: false })
  public created?: Date;

  @Field()
  @Property({ required: true })
  public topic: string;

  @Field(() => DiscussionSize)
  @Property({ required: true, enum: DiscussionSize })
  public size: DiscussionSize;

  @Field(() => DiscussionCategory)
  @Property({ required: true, autopopulate: true, ref: () => DiscussionCategory })
  public category: Ref<DiscussionCategory>;

  @Field(() => [DiscussionTag])
  @Property({ default: () => [], autopopulate: true, ref: () => DiscussionTag })
  public tags?: Ref<DiscussionTag>[]

  @Property({ default: false })
  public blocked?: boolean;

  public get wordLimit(): number {
    switch (this.size) {
      case DiscussionSize.short:
        return 100;
      case DiscussionSize.medium:
        return 150;
      case DiscussionSize.long:
        return 200;
    }
  }

  public get numberOfRounds(): number {
    switch (this.size) {
      case DiscussionSize.short:
        return 2;
      case DiscussionSize.medium:
        return 3;
      case DiscussionSize.long:
        return 4;
    }
  }
}

export const DiscussionModel = getModelForClass(Discussion, { schemaOptions: { discriminatorKey: 'kind' } });


registerEnumType(DiscussionSize, { name: 'DiscussionSize' });
registerEnumType(DiscussionType, { name: 'DiscussionType' });
