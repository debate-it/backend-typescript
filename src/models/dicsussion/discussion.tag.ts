import { prop as Property, getModelForClass } from '@typegoose/typegoose';
import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class DiscussionTag {
  @Field()
  @Property({ required: true, unique: true })
  public name: string;
}

export const TagModel = getModelForClass(DiscussionTag);
