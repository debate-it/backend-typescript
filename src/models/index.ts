import { logger } from '@/services/log.service';
import { mongoose } from '@typegoose/typegoose';

export * from './dicsussion';
export * from './user';
export * from './request';

export * from './post';
export * from './panel';
export * from './debate';
export * from './challenge';

export * from './comment';

export * from './vote';
export * from './event';

export namespace Database {
  type DatabaseConfig = { url: string };

  export async function initialize(config: DatabaseConfig, isProduction?: boolean) {
    const options: mongoose.ConnectOptions = {
      compressors: ['zlib'],
      autoIndex: isProduction,
      authMechanism: 'SCRAM-SHA-1',
    };

    const connection = await mongoose.connect(config.url, options);

    logger.info({ message: 'Database connection initialized' })

    return connection;
  }
}
