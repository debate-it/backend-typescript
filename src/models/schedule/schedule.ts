import { getModelForClass, prop as Property, Ref } from '@typegoose/typegoose';

import { Discussion } from '../dicsussion';
import { PanelRequest } from '../request';


export enum SchedulableType {
  Debate = 'Debate',
  Challenge = 'Challenge',

  Panel = 'Panel',
  PanelRequest = 'PanelRequest',
}

export class Schedule {
  @Property({ required: true })
  public updateAt: Date;

  @Property({ refPath: 'type' })
  public entity: Ref<Discussion | PanelRequest>

  @Property({ required: true, enum: SchedulableType })
  public type: SchedulableType;
}

export const ScheduleModel = getModelForClass(Schedule);