import autopopulate from 'mongoose-autopopulate';

import { getDiscriminatorModelForClass, Index, plugin, prop as Property, Ref } from "@typegoose/typegoose";
import { Field, Int, ObjectType, registerEnumType } from "type-graphql";

import { User } from "../user";
import { Discussion, DiscussionModel } from "../dicsussion";


export enum ChallengeStatus {
  active,
  waitingForOpponent,
  finished
}

@ObjectType({ implements: Discussion })
@plugin(autopopulate)
export class Challenge extends Discussion {
  public __typename = 'Challenge'

  @Field()
  @Property({ required: true })
  public position: string;

  @Field(() => User)
  @Property({ required: true, autopopulate: true, ref: () => User })
  public author: Ref<User>

  @Field(() => Date)
  @Property({ requried: true })
  public deadline: Date;

  @Property({ required: true })
  public level: number;

  @Field(() => Int)
  @Property({ default: 0 })
  public responses?: number;

  @Field(() => ChallengeStatus)
  @Property({ default: ChallengeStatus.active, enum: ChallengeStatus })
  public status?: ChallengeStatus;
}

export const ChallengeModel = getDiscriminatorModelForClass(DiscussionModel, Challenge);

registerEnumType(ChallengeStatus, { name: 'ChallengeStatus' });
