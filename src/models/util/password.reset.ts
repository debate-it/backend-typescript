import { getModelForClass, Index, prop as Property, Ref } from "@typegoose/typegoose";
import { User } from "../user";

@Index({ hash: 1, user: 1, activated: -1 }, { unique: true })
export class PasswordReset {
  @Property({ default: Date.now })
  public date?: Date;

  @Property({ required: true, index: { unique: true } })
  public hash: string;

  @Property({ required: true, ref: () => User })
  public user: Ref<User>;

  @Property({ default: false })
  public activated: boolean;
}

export const PasswordResetModel = getModelForClass(PasswordReset);
