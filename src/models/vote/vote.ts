import { getDiscriminatorModelForClass, getModelForClass, Index, prop as Property, Ref } from '@typegoose/typegoose';

import { User } from '@/models/user';
import { Panel } from '@/models/panel';
import { Comment } from '@/models/comment';
import { Debate } from '@/models/debate';
import { Post, PostAlignmentSide } from '@/models/post';

import { BaseModel } from '../base.model';

export type VoteTarget = Comment | Debate | Panel | Post;

export enum VoteTargetModel {
  Comment,
  Debate,
  Panel,
  Post
}

export enum VoteType {
  Post = 'PostVote',
  Discussion = 'DiscussionVote'
}

@Index({ voter: -1, target: -1, targetModel: 1 })
export class Vote extends BaseModel {
  public kind?: VoteType;

  @Property({ default: Date.now })
  public created?: Date;

  @Property({ default: true })
  public active?: boolean;

  @Property({ ref: () => User })
  public voter: Ref<User>;

  @Property({ requried: true })
  public value: number;

  @Property({ refPath: 'targetModel' })
  public target: Ref<VoteTarget>;

  @Property({ required: true, enum: VoteTargetModel })
  public targetModel: VoteTargetModel;
};

export class DiscussionVote extends Vote {
  @Property({ requried: true })
  public participant: number;
}

export class PostVote extends Vote {
  @Property({ required: true, enum: PostAlignmentSide, type: String })
  public alignment: PostAlignmentSide;
}


export const VoteModel = getModelForClass(Vote, { schemaOptions: { discriminatorKey: 'kind'}});
export const PostVoteModel = getDiscriminatorModelForClass(VoteModel, PostVote);
export const DiscussionVoteModel = getDiscriminatorModelForClass(VoteModel, DiscussionVote);
