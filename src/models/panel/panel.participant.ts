import autopopulate from 'mongoose-autopopulate';

import { ModelOptions, plugin, prop as Property, Ref } from "@typegoose/typegoose";
import { Field, Int, ObjectType } from "type-graphql";


import { User } from "../user";

@ObjectType()
@plugin(autopopulate)
@ModelOptions({ schemaOptions: { _id: false }}) 
export class PanelParticipant {
  @Field(() => User)
  @Property({ required: true, autopopulate: true, ref: () => User})
  public user: Ref<User>;
  
  @Field(() => Int)
  @Property({ default: 0 })
  public score?: number;

  @Field()
  @Property({ required: true })
  public argument: string;
}