import { getDiscriminatorModelForClass, prop as Property } from "@typegoose/typegoose";
import { Field, ObjectType, registerEnumType } from "type-graphql";

import { Discussion, DiscussionModel } from "@/models/dicsussion";

import { PanelParticipant } from "./panel.participant";

export const PANEL_MAX_PARTICIPANTS = 10;

export enum PanelStatus {
  waiting = 'waiting',
  active = 'active',
  finished = 'finished'
}

@ObjectType({ implements: Discussion })
export class Panel extends Discussion {
  public __typename = 'Panel'

  @Field(() => PanelStatus)
  @Property({ enum: PanelStatus, default: PanelStatus.waiting })
  public status?: PanelStatus;

  @Field(() => Date, { nullable: true })
  @Property({ requried: false })
  public deadline?: Date;

  @Field(() => [PanelParticipant])
  @Property({ default: [], type: () => PanelParticipant })
  public participants?: PanelParticipant[]
}

registerEnumType(PanelStatus, { name: 'PanelStatus' });

export const PanelModel = getDiscriminatorModelForClass(DiscussionModel, Panel);
