import autopopulate from 'mongoose-autopopulate';
import { getModelForClass, Index, plugin, prop as Property, Ref } from "@typegoose/typegoose";
import { Field, Int, ObjectType, registerEnumType } from "type-graphql";

import { BaseModel } from '../base.model';

import { User } from "@/models/user";
import { Discussion } from '@/models/dicsussion';


export enum CommentTargetType {
  Challenge = 'Challenge',
  Debate = 'Debate',
  Panel = 'Panel',
  Post = 'Post',
}

@ObjectType()
@Index({ target: -1, votes: -1 })
@plugin(autopopulate)
export class Comment extends BaseModel {
  @Field(() => User)
  @Property({ required: true, autopopulate: true, index: true, ref: () => User })
  public author: Ref<User>;

  @Field()
  @Property({ required: true })
  public content: string;

  @Field(() => Int, { nullable: true })
  @Property({  })
  public alignment?: number;

  @Field(() => Date)
  @Property({ default: Date.now })
  public created?: Date;

  @Field(() => Int)
  @Property({ default: 0 })
  public votes?: number;

  @Property({ required: true, index: true, ref: () => Discussion })
  public target: Ref<Discussion>;

  @Field(() => CommentTargetType, { name: 'type' })
  @Property({ required: true, enum: CommentTargetType })
  public targetModel: CommentTargetType;
}

export const CommentModel = getModelForClass(Comment);

registerEnumType(CommentTargetType, { name: "CommentType" });
