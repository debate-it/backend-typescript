import DataLoader from "dataloader";
import Container from "typedi";

import { DocumentType, isDocument } from "@typegoose/typegoose";
import { Types } from "mongoose";

import { VoteRepository } from "@/repositories/vote.repository";
import { DiscussionVote, VoteTargetModel } from "@/models/vote";
import { SubscriptionRepository } from "@/repositories/subscription.repository";
import { hasUncaughtExceptionCaptureCallback } from "process";
import { Subscription, SubscriptionTargetModel } from "@/models/subscription";
import { UserRelation } from "./types/user.types";

interface CommentVoteLoaderPayload {
  user: string;
  comment: string
}

interface ParticipantVoteLoaderPayload {
  voter: string;
  target: string;
  targetModel: VoteTargetModel;
}

interface RelationLoaderPayload {
  current: string;
  target: string;
}

interface CategorySubscriptionPayload {
  target: string;
  user: string;
}

type CommentVoteLoader = DataLoader<CommentVoteLoaderPayload, boolean, CommentVoteLoaderPayload>;
type ParticipantVoteLoader = DataLoader<ParticipantVoteLoaderPayload, number, ParticipantVoteLoaderPayload>;
type UserRelationLoader = DataLoader<RelationLoaderPayload, UserRelation, RelationLoaderPayload>;
type CategorySubscriptionLoader = DataLoader<CategorySubscriptionPayload, any, CategorySubscriptionPayload>;

export interface DataLoaders {
  commentVotes: CommentVoteLoader;
  participantVotes: ParticipantVoteLoader;
  userRelations: UserRelationLoader;
  categorySubscriptions: CategorySubscriptionLoader;
}

export namespace Loaders {
  export function create(): DataLoaders {
    return {
      commentVotes: createCommentVoteLoader(),
      participantVotes: createParticipantVoteLoader(),
      userRelations: createRelationLoader(),
      categorySubscriptions: createCategorySusbcriptionLoader()
    };
  }

  function createParticipantVoteLoader() {
    return new DataLoader(async (payload: ParticipantVoteLoaderPayload[]) => {
      const repository = Container.get(VoteRepository);

      const votes = await repository.find({
        voter: { $in: payload.map(key => key.voter) },
        target: { $in: payload.map(key => key.target) },
        targetModel: { $in: payload.map(key => key.targetModel) }
      }) as DocumentType<DiscussionVote>[];

      const map = new Map<string, number>();

      for (const vote of votes) {
        map[(vote.target as Types.ObjectId).toHexString()] = vote.active ? vote.participant : null;
      }

      return payload.map<number | null>(key => map[key.target] ?? null);
    })
  }

  function createCommentVoteLoader() {
    return new DataLoader(async (payload: CommentVoteLoaderPayload[]) => {
      const repository = Container.get(VoteRepository);

      const voter = payload[0].user;
      const keys = payload.map(p => p.comment);

      const votes = await repository.find({
        voter, 
        target: { $in: keys },
        targetModel: VoteTargetModel.Comment,
      });

      const map = new Map<string, boolean>(); 

      for (const vote of votes) {
        map[(vote.target as Types.ObjectId).toHexString()] = vote.active;
      }

      return payload.map(p => map[p.comment] ?? false);
    });
  }

  function createCategorySusbcriptionLoader() {
    return new DataLoader(async (payload: CategorySubscriptionPayload[]) => {
      const repository = Container.get(SubscriptionRepository);
      const user = payload[0].user;

      if (payload.some(entry => entry.user !== user)) {
        throw new Error('Invalid category subscription loader payload: current user varies');
      }

      const records = await repository.find({
        active: true,
        follower: user,
        subscription: { $in: payload.map(p => p.target) },
        subscriptionModel: SubscriptionTargetModel.Category
      });

      const map = new Map<string, boolean>();

      for (const record of records) {
        map[(record.subscription as Types.ObjectId).toHexString()] = record.active;
      }

      return payload.map(p => map[p.target] ?? false);
    });
  }

  function createRelationLoader() {
    return new DataLoader(async (payload: RelationLoaderPayload[]) => {
      const repository = Container.get(SubscriptionRepository);

      const current = payload[0].current;

      if (payload.some(entry => entry.current !== current)) {
        throw new Error('Invalid relation loader payload: current user varies');
      }

      // Find subscription records for whether current users
      // is followed by or subscribed to each of the entries
      const [followers, subscriptions] = await Promise.all([
        repository.find({ 
          active: true,
          follower: { $in: payload.map(entry => entry.target )},
          subscription: current,
          subscriptionModel: SubscriptionTargetModel.User
        }),
        
        repository.find({ 
          active: true,
          follower: current,
          subscription: { $in: payload.map(entry => entry.target )},
          subscriptionModel: SubscriptionTargetModel.User
        }),
      ]);

      interface RelationEntry {
        follower?: Subscription;
        subscription?: Subscription;
        self: boolean;
      }

      // Map each entry to corresponding subscription record
      const relationEntries: RelationEntry[] = payload.map(entry => ({
          follower: followers.find(e => 
            isDocument(e.follower) 
              ? e.follower.id === entry.target 
              : (e.follower as Types.ObjectId).toHexString() === entry.target),

          subscription: subscriptions.find(e => 
            isDocument(e.subscription) 
              ? e.subscription.id === entry.target 
              : (e.subscription as Types.ObjectId).toHexString() === entry.target),

          self: current === entry.target
        })
      );

      return relationEntries.map(entry => {
        if (entry.self) {
          return UserRelation.self;
        }

        if (entry.follower && entry.subscription) {
          return UserRelation.friend;
        }

        if (entry.follower) {
          return UserRelation.follower;
        }

        if (entry.subscription) {
          return UserRelation.subscription;
        }

        return UserRelation.none;
      });
    });
  }
}