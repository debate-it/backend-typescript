import plugin from 'fastify-plugin'

import { FastifyPluginAsync } from 'fastify';
import { processRequest, UploadOptions } from 'graphql-upload';


const fastifyGQLUpload: FastifyPluginAsync<UploadOptions> = async (fastify, options = {}) => {
  fastify.addHook('preValidation', async function(request, reply) {
    if (!request.isMultipart()) {
      return
    }

    request.body = await processRequest(request.raw, reply.raw, options);
  });
}

export default plugin(fastifyGQLUpload);
