import { Arg, Args, Authorized, Ctx, FieldResolver, Int, Mutation, Publisher, PubSub, Query, Resolver, ResolverFilterData, Root, Subscription } from 'type-graphql';
import { DocumentType } from '@typegoose/typegoose';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { Inject, Service } from 'typedi';

import { Debate, DebateArgument } from '@/models/debate';
import { DebateRepository } from '@/repositories/debate.repository';

import { Context } from '@/graphql/context';
import { AddArgumentArgs, DebateRealtimeArgs, DebateRealtimeArgumentResponse, DebateVoteArgs } from '@/graphql/types';
import { VoteTargetModel } from '@/models';
import { IdArgs } from '../types/shared.types';

@Service()
@Resolver(() => Debate)
export class DebateResolver {

  @Inject() private repository: DebateRepository;

  @Query(() => Debate, { nullable: true })
  async debate(@Args() args: IdArgs): Promise<Debate> {
    const d  = this.repository.get(args.id);
    return d;
  }

  @Query(() => [Debate])
  async debates(): Promise<Debate[]> {
    return this.repository.find({});
  }

  @Authorized()
  @Mutation(() => Debate)
  async addArgument(
    @Args() args: AddArgumentArgs, 
    @Ctx() context: Context,
    @PubSub('debate:real-time') pubsub: Publisher<DebateRealtimeArgumentResponse>
  ): Promise<Debate> {
    return this.repository.addArgument(args.id, context.user, args.argument, pubsub);
  }

  @Authorized()
  @Mutation(() => Debate)
  async voteOnDebate(@Args() args: DebateVoteArgs, @Ctx() context: Context): Promise<Debate> {
    return this.repository.vote(args.id, args.participant, context.user);
  }

  @Authorized()
  @Subscription(() => DebateRealtimeArgumentResponse, {
    topics:`debate:real-time`,
    filter: ({ payload, args }: ResolverFilterData<DebateRealtimeArgumentResponse, DebateRealtimeArgs, Context>) => 
      payload.debate === args.id && 
      payload.argument.participant !== args.participant,
  })
  debateRealtimeArguments(@Root() payload: DebateRealtimeArgumentResponse, @Args() args: DebateRealtimeArgs)  {
    if (typeof payload.argument.time === 'string') {
      payload.argument.time = new Date(Date.parse(payload.argument.time));
    }

    return payload;
  }

  @Authorized()
  @FieldResolver(() => Int, { nullable: true })
  votedOn(@Root() debate: DocumentType<Debate>, @Ctx() context: Context) {
    return context.loaders.participantVotes.load({
      target: debate.id,
      targetModel: VoteTargetModel.Debate,
      voter: context.user.id
    });
  }
}