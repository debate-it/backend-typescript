import { Arg, FieldResolver, Query, Resolver } from "type-graphql";
import { Inject, Service } from "typedi";

import { Leaderboard as LeaderboardType, LeaderboardService } from "@/services/leaderboard.service";

import { Leaderboard, LeaderboardPagination } from '../types/leaderboard.types';
import { PaginationInfo } from "../types/pagination.types";

@Service()
@Resolver(() => Leaderboard)
export class LeaderboardResolver {

  @Inject() private readonly leaderboard: LeaderboardService;

  @Query(() => Leaderboard) 
  leaderboards() {
    return {};
  }

  @FieldResolver(() => LeaderboardPagination)
  async debates(@Arg('page') page: PaginationInfo) {
    return await this.leaderboard.get(LeaderboardType.Debates, page.cursor, page.limit);
  }
  
  @FieldResolver(() => LeaderboardPagination)
  async posts(@Arg('page') page: PaginationInfo) {
    return this.leaderboard.get(LeaderboardType.Posts, page.cursor, page.limit);
  }
  
  @FieldResolver(() => LeaderboardPagination)
  async users(@Arg('page') page: PaginationInfo) {
    return await this.leaderboard.get(LeaderboardType.Users, page.cursor, page.limit);
  }
  
  @FieldResolver(() => LeaderboardPagination)
  async challenges(@Arg('page') page: PaginationInfo) {
    return this.leaderboard.get(LeaderboardType.Challenges, page.cursor, page.limit);
  }
}