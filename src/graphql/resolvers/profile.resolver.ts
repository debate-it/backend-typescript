import { Inject, Service } from "typedi";
import { GraphQLBoolean } from "graphql";
import { FileUpload, GraphQLUpload } from "graphql-upload";

import { Arg, Args, Authorized, Ctx, Field, FieldResolver, Int, Mutation, ObjectType, Query, Resolver } from "type-graphql";

import { AuthService } from "@/services/auth.service";
import { FeedService } from "@/services/feed.service";

import { UserRepository } from "@/repositories/user.repository";
import { EventRepository } from "@/repositories/event.repository";
import { DebateRepository } from "@/repositories/debate.repository";
import { RequestRepository } from "@/repositories/request.repository";
import { SubscriptionRepository } from "@/repositories/subscription.repository";

import { Context } from '@/graphql/context';

import { User } from "@/models/user";
import { DebateStatus } from "@/models/debate";
import { RequestStatus, RequestType } from "@/models/request";
import { SubscriptionTargetModel } from "@/models/subscription";

import { RequestSource } from "../types/request.types";
import { SubscriptionArgs, SubscriptionType } from "../types/subscription.types";
import { CategoryPagination, DebatePagination, DiscussionPagination, PageArgs, PaginationInfo, RequestPagination, UserPagination } from "../types/pagination.types";

import {
  UserInfoUpdateInput,
  ProfileRequestsArgs,
  ProfileFeedPagination,
  ProfileActivityPagination,
  ProfileSubscriptionPagination,
  ProfileDebatesArgs
} from "../types/profile.types";
import { DiscussionRepository } from "@/repositories/discussion.repository";


@ObjectType()
class ProfileIndicators {
  @Field(() => Int)
  notifications: number;

  @Field(() => Int)
  requests: number;

  @Field(() => Int)
  debates: number;
}

@ObjectType()
class Profile {
  @Field(() => User)
  info: User;

  @Field(() => ProfileIndicators)
  indicators: ProfileIndicators;

  @Field(() => ProfileFeedPagination)
  feed: ProfileFeedPagination;

  @Field(() => DebatePagination)
  debates: DebatePagination;

  @Field(() => RequestPagination)
  requests: RequestPagination;

  @Field(() => ProfileActivityPagination)
  activity: ProfileActivityPagination;

  @Field(() => ProfileSubscriptionPagination)
  subscriptions: ProfileSubscriptionPagination;
}


@Service()
@Resolver(() => Profile)
export class ProfileResolver {
  @Inject() private readonly subscriptions: SubscriptionRepository;
  @Inject() private readonly discussions: DiscussionRepository;
  @Inject() private readonly requests: RequestRepository;
  @Inject() private readonly debates: DebateRepository;
  @Inject() private readonly events: EventRepository;
  @Inject() private readonly users: UserRepository;

  @Inject() private readonly auth: AuthService;

  @Inject() private readonly feed: FeedService;

  @Authorized()
  @Query(() => Profile)
  profile() {
    return {};
  }

  @FieldResolver(() => ProfileIndicators)
  indicators() {
    return {};
  }

  @Authorized()
  @FieldResolver(() => [String])
  roles(@Ctx() context: Context) {
    return context.user.auth.roles.map(r => r.name);
  }

  @Authorized()
  @FieldResolver(() => User)
  info(@Ctx() context: Context) {
    return context.user;
  }

  @Authorized()
  @FieldResolver(() => ProfileFeedPagination, { name: 'feed' })
  async profileFeed(@Args() args: PageArgs, @Ctx() context: Context) {
    return await this.feed.fetch(context.user, args.page);
  }

  @Authorized()
  @FieldResolver(() => DebatePagination, { name: 'debates' })
  async profileDebates(@Args() args: ProfileDebatesArgs, @Ctx() context: Context) {
    return await this.debates.paginate(
      { "participants.user": context.user, "status": args.status },
      { ...args.page, sortBy: 'created', sortDir: 'dsc' }
    );
  }

  @Authorized()
  @FieldResolver(() => DiscussionPagination, { name: 'discussions' })
  async profileDiscussions(@Args() args: ProfileDebatesArgs, @Ctx() context: Context) {
    const query = { 
      $or: [
        { 'kind': 'Post', 'author': context.user },
        { 'kind': 'Debate', 'participants.user': context.user, 'status': args.status },
        { 'kind': 'Panel', 'participants.user': context.user, 'status': args.status }
      ]
    };

    return await this.discussions.paginate(query, { ...args.page, sortBy: 'created', sortDir: 'dsc' });
  }

  @Authorized()
  @FieldResolver(() => RequestPagination, { name: 'requests' })
  async profileRequests(@Args() args: ProfileRequestsArgs, @Ctx() context: Context) {
    const status = args.status ?? RequestStatus.submitted;

    let query: any[] = [
      { 
        kind: { $in: [RequestType.Debate, RequestType.Panel] }, 
        initiator: context.user 
      },
      { 
        kind: RequestType.Debate, 
        recipient: context.user, 
        status: status 
      },
      { 
        kind: RequestType.Panel, 
        recipients: { $elemMatch: { user: context.user, status: status } },
        $or: [ { expiration: null }, { expiration: { $gt: Date.now() } } ]
      }
    ];

    switch (args.source) {
      case RequestSource.incoming:
        query.shift();
        break;

      case RequestSource.outgoing:
        query = query.slice(0, 1)
        break;
    }

    const records = await this.requests
      .paginate({ $or: query }, { ...args.page, sortBy: 'created', sortDir: 'dsc' });

    return records;
  }

  @Authorized()
  @FieldResolver(() => ProfileActivityPagination)
  async activity(@Args() args: PageArgs, @Ctx() context: Context) {
    return this.events.paginate({ target: context.user }, { ...args.page, limit: 5, sortBy: 'to', sortDir: 'dsc' });
  }
  
  @Authorized()
  @Query(() => Int)
  async profileCountNewNotifications(@Ctx() context: Context) {
  }


  @Authorized()
  @FieldResolver(() => UserPagination, { name: 'userSubscriptions' })
  async profileUserSubscriptions(@Arg('page') page: PaginationInfo, @Ctx() context: Context) {
    const records = await this.subscriptions .paginate(
      { follower: context.user, subscriptionModel: SubscriptionTargetModel.User, active: true },
      { ...page, sortDir: 'dsc', populate: { path: 'subscription' } }
    );

    return {
      ...records,
      page: records.page
        .map(record => record.subscription)
    };
  }
  
  @Authorized()
  @FieldResolver(() => CategoryPagination, { name: 'categorySubscriptions' })
  async profileCategorySubscriptions(@Arg('page') page: PaginationInfo, @Ctx() context: Context) {
    const records = await this.subscriptions .paginate(
      { follower: context.user, subscriptionModel: SubscriptionTargetModel.Category, active: true },
      { ...page, sortDir: 'dsc', populate: { path: 'subscription' } }
    );

    return {
      ...records,
      page: records.page
        .map(record => record.subscription)
    };
  }

  @Authorized()
  @Mutation(() => SubscriptionType, { nullable: true })
  async subscribe(@Args() args: SubscriptionArgs, @Ctx() context: Context) {
    switch (args.type) {
      case SubscriptionTargetModel.User:
        return this.subscriptions.followUser(context.user, args.target);

      case SubscriptionTargetModel.Category:
        return this.subscriptions.followCategory(context.user, args.target);
    }
  }
  @Authorized()
  @Mutation(() => GraphQLBoolean)
  async updateNotificationsSeen(@Ctx() context: Context) {
    await this.events.update({ target: context.user, new: { $gt: 0 }}, { $set: { new: 0 }});
    
    return true;
  }

  @Authorized()
  @Mutation(() => User)
  async updateInfo(@Arg('options') args: UserInfoUpdateInput, @Ctx() context: Context) {
    return this.users.updateInfo(context.user, args);
  }

  @Authorized()
  @Mutation(() => User)
  async updateEmail(@Arg('email') email: string, @Ctx() context: Context) {
    return this.auth.changeEmail(context.user, email);
  }

  @Authorized()
  @Mutation(() => Boolean)
  async setDeviceToken(@Arg('token', { nullable: false }) token: string, @Ctx() context: Context) {
    if (!token || token.length === 0) {
      throw new Error('Invalid token');
    }

    try {
      await this.users.updateById(context.user.id, { $set: { 'auth.firebaseToken': token } });

      return true;
    } catch (err) {
      throw new Error('Error occured when saving token');
    }
  }

  @Authorized()
  @Mutation(() => Boolean)
  async unsetDeviceToken(@Ctx() context: Context) {
    await this.users.updateById(context.user.id, { $set: { 'auth.firebaseToken': null } });

    return true;
  }

  @Authorized()
  @Mutation(() => Boolean)
  async updateProfileImage(@Arg('file', () => GraphQLUpload) file: FileUpload, @Ctx() context: Context) {
    return this.users.updateProfilePicture(context.user, file);
  }
}

@Service()
@Resolver(() => ProfileIndicators)
export class ProfileIndicatorResolver {
  
  @Inject() private readonly requests: RequestRepository;
  @Inject() private readonly debates: DebateRepository;
  @Inject() private readonly events: EventRepository;

  
  @FieldResolver(() => Int)
  async notifications(@Ctx() context: Context) {
    const events = await this.events.find({ target: context.user, new: { $gt: 0 }});

    const count = events.reduce((acc, e) => acc + e.new, 0);

    return count;
  }
  
  @FieldResolver(() => Int, { name: 'debates' })
  async activeDebates(@Ctx() context: Context) {
    return await this.debates.count({
      'participants.user': context.user.id,
      status: DebateStatus.active 
    });
  }
  
  @FieldResolver(() => Int, { name: 'requests' })
  async activeRequests(@Ctx() context: Context) {
    return await this.requests.count({ 
      $or: [
        {
          kind: RequestType.Debate,
          recipient: context.user,
          status: RequestStatus.submitted
        },
        {
          kind: RequestType.Panel,
          recipients: { $elemMatch: { user: context.user, status: RequestStatus.submitted } },
          $or: [ { expiration: null }, { expiration: { $gt: Date.now() } } ]
        }
      ]
    });
  }
}