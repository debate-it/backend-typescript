import { Args, Authorized, Ctx, FieldResolver, Int, Mutation, Query, Resolver, Root } from "type-graphql";
import { Inject, Service } from "typedi";

import { Panel, VoteTargetModel } from "@/models";

import { PanelRepository } from "@/repositories/panel.repository";

import { IdArgs } from "../types/shared.types";
import { Context } from "../context";
import { PanelVoteArgs } from "../types/panel.types";
import { DocumentType } from "@typegoose/typegoose";

@Service()
@Resolver(() => Panel)
export class PanelResolver {
  @Inject() private readonly repository: PanelRepository;

  @Query(() => Panel, { nullable: true })
  panel(@Args() args: IdArgs ) {
    return this.repository.get(args.id);
  }

  @Authorized()
  @Mutation(() => Panel, { nullable: true })
  voteOnPanel(@Args() args: PanelVoteArgs, @Ctx() context: Context) {
    return this.repository.vote(args.id, context.user, args.participant)
  }
  
  @Authorized()
  @FieldResolver(() => Int, { nullable: true })
  async votedOn(@Root() panel: DocumentType<Panel>, @Ctx() context: Context) {
    return context.loaders.participantVotes.load({
      target: panel.id,
      targetModel: VoteTargetModel.Panel,
      voter: context.user.id
    });
  }
}