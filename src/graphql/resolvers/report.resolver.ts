import { Args, Authorized, Ctx, Mutation, Query, Resolver } from "type-graphql";

import { GraphQLBoolean } from "graphql";
import { Inject, Service } from "typedi";

import { Report } from "@/models/report/report";
import { ReportRepository } from "@/repositories/report.repository";

import { Context } from "../context";

import { IdArgs } from "../types/shared.types";
import { ReportOverview, ReportResolutionArgs, ReportSubmissionInput } from "../types/report.types";
import { PageArgs, ReportPagination } from "../types/pagination.types";


@Service()
@Resolver(() => Report)
export class ReportResolver {
  @Inject() private readonly repository: ReportRepository;

  @Authorized(['admin'])
  @Query(() => ReportPagination)
  async reports(@Args() args: PageArgs) {
    return await this.repository.paginate({ resolved: false }, args.page);
  }

  @Authorized(['admin'])
  @Query(() => Report)
  async report(@Args() args: IdArgs) {
    return await this.repository.get(args.id);
  }

  @Authorized(['admin'])
  @Query(() => ReportOverview)
  async reportOverview(): Promise<ReportOverview> {
    const [pending, resolved] = await Promise.all([
      this.repository.count({ resolved: false }),
      this.repository.count({ resolved: true }),
    ]);

    return { pending, resolved }
  }

  @Authorized(['admin'])
  @Mutation(() => Report)
  async resolveReport(@Args() args: ReportResolutionArgs) {
    return await this.repository.resolve(args.id, args.decisions);
  }

  @Authorized()
  @Mutation(() => GraphQLBoolean)
  async submitReport(@Args() args: ReportSubmissionInput, @Ctx() context: Context) {
    return await this.repository.submit(context.user, args);
  }
}
