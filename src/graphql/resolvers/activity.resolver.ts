import { FieldResolver, Resolver, Root } from "type-graphql";
import { Service } from "typedi";

import { Event, EventRecord, getEventMessage, getEventTitle } from "@/models/event";
import { DocumentType } from "@typegoose/typegoose";

@Service()
@Resolver(() => Event)
export class ActivityResolver {
  @FieldResolver(() => [EventRecord])
  activity(@Root() parent: DocumentType<Event>)  {
    return parent.events.reverse();
  }

}

@Service()
@Resolver(() => EventRecord)
export class EventResolver {

  @FieldResolver()
  title(@Root() parent: DocumentType<EventRecord>): string {
    return getEventTitle(parent);
  }

  @FieldResolver()
  message(@Root() parent: DocumentType<EventRecord>): string {
    return getEventMessage(parent);
  }
}