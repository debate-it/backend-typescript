import { Inject, Service } from 'typedi';
import { DocumentType } from '@typegoose/typegoose';
import { Resolver, Query, Mutation, Ctx, Authorized, Args, FieldResolver, Root } from 'type-graphql';

import { Post, PostAlignmentSide } from '@/models/post';
import { PostRepository } from '@/repositories/post.repository';

import { Context } from '@/graphql/context';
import { AlignWithPostArgs, CreatePostArgs } from '@/graphql/types';
import { IdArgs } from '../types/shared.types';

@Service()
@Resolver(() => Post)
export class PostResolver {
  @Inject()
  private readonly repository: PostRepository;

  @Query(() => Post, { nullable: true })
  async post(@Args() args: IdArgs): Promise<Post> {
    return this.repository.get(args.id);
  }

  @Query(() => [Post])
  async posts() {
    return this.repository.find({});
  }

  @Authorized()
  @Mutation(() => Post, { nullable: true })
  async createPost(@Args() args: CreatePostArgs, @Ctx() context: Context) {
    return this.repository.initiate(context.user, args);
  }

  @Authorized()
  @Mutation(() => Post, { nullable: true })
  async alignWithPost(@Args() args: AlignWithPostArgs, @Ctx() context: Context) {
    return this.repository.align(context.user, args);
  }

  @Authorized()
  @FieldResolver(() => PostAlignmentSide, { nullable: true })
  votedOn(@Root() post: DocumentType<Post>, @Ctx() context: Context) {
    return this.repository.findUserVote(post, context.user);
  }
}