import { GraphQLBoolean } from 'graphql';
import { Inject, Service } from 'typedi';
import { Arg, Args, Authorized, Ctx, Mutation, Query, Resolver } from 'type-graphql';
import { EnvelopError } from '@envelop/core';

import { AuthService } from '@/services/auth.service';

import {
  AuthTokens,
  AuthStatus,
  AuthResponse,
  LoginInfo,
  SignUpInfo,
  PasswordResetRequestInfo,
  PasswordResetInfo
} from '@/graphql/types';
import { Context } from '../context';

import { DocumentType } from '@typegoose/typegoose/lib/types';
import { BeAnObject } from '@typegoose/typegoose/lib/types';
import { User } from '@/models/user';

type AuthFunction = () => Promise<AuthTokens>

@Service()
@Resolver()
export class AuthResolver {
  @Inject()
  private auth: AuthService;

  @Query(() => AuthResponse)
  async authenticate(@Arg('token') token: string, @Ctx() context: Context): Promise<AuthResponse> {
    const response = await this.auth.authenticate(token);

    if (response.user) {
      context.user = response.user as DocumentType<User, BeAnObject>;
    }

    return response;
  }

  @Query(() => GraphQLBoolean)
  async verifyUserCredentials(@Arg('username') username: string, @Arg('email') email: string) {
    return await this.auth.findDuplicates(username, email);
  }

  @Query(() => Boolean)
  async verifyUsername(@Arg('username') username: string) {
    return await this.auth.verifyUsername(username);
  }
  
  @Query(() => Boolean)
  async verifyEmail(@Arg('email') email: string) {
    return await this.auth.verifyEmail(email);
  }

  @Mutation(() => AuthResponse)
  async signup(@Args() payload: SignUpInfo, @Ctx() context: Context): Promise<AuthResponse> {
    const response = await this.auth.signup(payload);

    if (response.user) {
      context.user = response.user as DocumentType<User, BeAnObject>;
    }

    return response;
  }

  @Mutation(() => AuthResponse)
  async login(@Args() payload: LoginInfo, @Ctx() context: Context): Promise<AuthResponse> {
    const response = await this.auth.login(payload);

    if (response.user) {
      context.user = response.user as DocumentType<User, BeAnObject>;
    }

    return response;
  }

  @Authorized('INACTIVE')
  @Mutation(() => GraphQLBoolean)
  async activate(@Arg('code') code: string, @Ctx() context: Context) {
    return this.auth.activate(context.user, code);
  }
  
  @Authorized('INACTIVE')
  @Mutation(() => GraphQLBoolean)
  async resendActivationCode(@Ctx() context: Context) {
    return this.auth.resendActivationCode(context.user);
  }

  @Mutation(() => GraphQLBoolean)
  async forgotPassword(@Args() payload: PasswordResetRequestInfo) {
    return this.auth.createPasswordChangeRequest(payload.email);
  }

  @Query(() => GraphQLBoolean)
  async verifyPasswordReset(@Arg('id') id: string ) {
    return this.auth.verifyPasswordChangeRequest(id);
  }

  @Mutation(() => GraphQLBoolean)
  async resetPassword(@Args() payload: PasswordResetInfo) {
    return this.auth.resetPassword(payload.id, payload.password);
  }

  @Authorized()
  @Mutation(() => GraphQLBoolean)
  async changePassword(@Arg('password') password: string, @Ctx() context: Context) {
    return this.auth.changePassword(context.user, password);
  }
}