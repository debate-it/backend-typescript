import { Args, Authorized, Ctx, FieldResolver, Mutation, Query, Resolver, Root } from "type-graphql";

import { FilterQuery } from "mongoose";
import { GraphQLBoolean } from "graphql";
import { Inject, Service } from "typedi";
import { DocumentType } from "@typegoose/typegoose";
import { EnvelopError } from "@envelop/core";

import { SubscriptionTargetModel } from "@/models/subscription";
import { Discussion, DiscussionCategory } from "@/models/dicsussion";

import { SubscriptionRepository } from "@/repositories/subscription.repository";
import { DiscussionRepository } from "@/repositories/discussion.repository";
import { CategoryRepository } from "@/repositories/category.repository";

import { CategoryArgs, CategoryFeedArgs, InitialSubscriptionArgs } from "../types/category.types";
import { DiscussionPagination, PageArgs, UserPagination } from "../types/pagination.types";

import { Context } from "../context";

@Service()
@Resolver(() => DiscussionCategory)
export class CategoryResolver {
  @Inject() private readonly discussions: DiscussionRepository;
  @Inject() private readonly subscriptions: SubscriptionRepository;
  @Inject() private readonly categories: CategoryRepository;

  @Query(() => DiscussionCategory)
  async category(@Args() args: CategoryArgs) {
    if (!args.id && !args.name) {
      throw new EnvelopError('ID and name not provided', { code: 'NOT_FOUND' });
    }

    if (args.id) {
      return this.categories.get(args.id);
    }

    return this.categories.one({ name: args.name });
  }

  @Query(() => [DiscussionCategory], { name: 'categories' })
  async fetchCategories() {
    return this.categories.find({});
  }


  @Authorized()
  @Mutation(() => [DiscussionCategory])
  async initialSubscription(@Args() args: InitialSubscriptionArgs, @Ctx() context: Context) {
    const categories = await this.categories.find({ _id: { $in: args.categories }});

    await Promise.all(categories.map(category => 
      this.subscriptions.followCategory(context.user, category.id)));

    return categories;
  }

  @FieldResolver(() => DiscussionPagination)
  async feed(@Root() category: DocumentType<DiscussionCategory>, @Args() args: CategoryFeedArgs) {
    const query: FilterQuery<DocumentType<Discussion>> = {
      category: category.id,
    };

    if (args.filter?.length > 0) {
      query.kind = { $in: args.filter };
    }

    return this.discussions.paginate(query, { ...args.page, sortDir: 'dsc' });
  }

  @FieldResolver(() => UserPagination)
  async followers(@Root() category: DocumentType<DiscussionCategory>, @Args() args: PageArgs) {
    const subscriptions = await this.subscriptions.paginate(
      { 
        subscription: category, 
        subscriptionModel: SubscriptionTargetModel.Category 
      },
      { 
        ...args.page,
        sortDir: 'dsc', 
        populate: { path: 'follower'} 
      }
    );

    return {
      ...subscriptions,
      page: subscriptions.page.map(s => s.follower)
    }
  }

  @Authorized()
  @FieldResolver(() => GraphQLBoolean)
  async isSubscribed(@Root() category: DocumentType<DiscussionCategory>, @Ctx() context: Context) {
    return context.loaders.categorySubscriptions.load({ target: category.id, user: context.user.id });
  }
}