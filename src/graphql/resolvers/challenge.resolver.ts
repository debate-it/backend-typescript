import { GraphQLBoolean } from "graphql";
import { Inject, Service } from "typedi";
import { DocumentType } from "@typegoose/typegoose";
import { Args, Authorized, Ctx, FieldResolver, Mutation, Query, Resolver, Root,  } from "type-graphql";


import { Debate } from "@/models/debate";
import { Challenge, ChallengeStatus } from "@/models/challenge";

import { ChallengeRepository } from "@/repositories/challenge.repository";

import { Context } from "@/graphql/context";

import {
  AddChallengeResponseArgs,
  ChooseChallengeOpponentArgs,
  CreateChallengeArgs
} from "@/graphql/types/challenge.types";

import { IdArgs } from "@/graphql/types/shared.types";

import { ChallengePagination, PageArgs } from "@/graphql/types/pagination.types";


@Service()
@Resolver(() => Challenge)
export class ChallengeResolver {

  @Inject()
  private readonly repository: ChallengeRepository;

  @Query(() => Challenge, { nullable: true })
  async challenge(@Args() args: IdArgs) {
    return this.repository.get(args.id);
  }

  @Query(() => ChallengePagination)
  async challenges(@Args() args: PageArgs) {
    return this.repository.paginate({}, { ...args.page, sortBy: 'created', sortDir: 'dsc' });
  }

  @Authorized()
  @Mutation(() => Challenge, { nullable: true })
  async initiateChallenge(@Args() args: CreateChallengeArgs, @Ctx() context: Context) {
    return this.repository.initiate(context.user, args);
  }

  @Authorized()
  @Mutation(() => Challenge, { nullable: true })
  async addChallengeResponse(@Args() args: AddChallengeResponseArgs, @Ctx() context: Context) {
    return this.repository.addReponse(args.id, context.user, args.response);
  }

  @Authorized()
  @Mutation(() => Debate, { nullable: true })
  async chooseChallengeOpponent(@Args() args: ChooseChallengeOpponentArgs, @Ctx() context: Context) {
    return this.repository.chooseOpponent(args.id, context.user, args.opponent);
  }

  @FieldResolver(() => GraphQLBoolean)
  finished(@Root() challenge: DocumentType<Challenge>) {
    return challenge.status === ChallengeStatus.finished;
  }
}
