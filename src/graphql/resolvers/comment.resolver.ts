import { Inject, Service } from "typedi";
import { GraphQLBoolean } from "graphql";
import { DocumentType } from "@typegoose/typegoose";
import { Arg, Args, Authorized, Ctx, FieldResolver, ID, Mutation, Query, Resolver, Root } from "type-graphql";

import { Comment } from "@/models/comment";

import { CommentRepository } from "@/repositories/comment.repository";

import { Context } from "@/graphql/context";
import { IdArgs } from "@/graphql/types/shared.types";
import { CommentPagination } from "@/graphql/types/pagination.types";

import { 
  CommentSearchArgs,
  CommentWithDiscussion,
  CreateCommentArgs, 
  UserCommentArgs 
} from "@/graphql/types/comment.types";


@Service()
@Resolver(() => Comment)
export class CommentResolver {
  @Inject() private readonly repository: CommentRepository;

  @Query(() => Comment, { nullable: true })
  async comment(@Arg('id') id: string ) {
    return this.repository.get(id);
  }

  @Query(() => CommentPagination) 
  async comments(@Args() args: CommentSearchArgs, @Ctx() context: Context) {
    return this.repository.findFor(context.user, args);
  }

  @Query(() => Comment, { nullable: true })
  @Authorized()
  async userComment(@Args() args: UserCommentArgs, @Ctx() context: Context) {
    return await this.repository.one({ 
      target: args.target,
      targetModel: args.type,
      author: context.user
    });
  }
  
  @Query(() => CommentWithDiscussion)
  async commentWithDiscussion(@Args() args: IdArgs) {
    const comment = await this.repository.get(args.id).populate('target');

    return { comment, discussion: comment.target };
  }

  @Authorized()
  @Mutation(() => Comment, { nullable: true })
  async createComment(@Args() args: CreateCommentArgs, @Ctx() context: Context) {
    return this.repository.initiate(context.user, args);
  }

  @Authorized()
  @Mutation(() => Comment, { nullable: true })
  async voteOnComment(@Args() args: IdArgs, @Ctx() context: Context) {
    return this.repository.vote(args.id, context.user);
  }

  @FieldResolver(() => GraphQLBoolean)
  voted(@Root() comment: DocumentType<Comment>, @Ctx() context: Context) {
    return context.loaders.commentVotes.load({ comment: comment.id, user: context.user.id });
  }
}