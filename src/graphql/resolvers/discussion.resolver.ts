import { Args, Query, Resolver } from "type-graphql";
import { Inject, Service } from "typedi";

import { Discussion } from "@/models/dicsussion";
import { IdArgs } from "@/graphql/types/shared.types";
import { DiscussionRepository } from "@/repositories/discussion.repository";

@Service()
@Resolver()
export class DiscussionResolver {
  @Inject() private repository: DiscussionRepository;

  @Query(() => Discussion, { nullable: true })
  async discussion(@Args() args: IdArgs): Promise<Discussion> {
    return this.repository.get(args.id);
  }
}