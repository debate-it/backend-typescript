import { DocumentType } from '@typegoose/typegoose';
import Container, { Inject, Service } from 'typedi';
import { Arg, Args, Ctx, FieldResolver, Query, Resolver, Root } from 'type-graphql';

import { Context } from '@/graphql/context';

import { User, UserModel } from '@/models/user';
import { SubscriptionTargetModel } from '@/models/subscription';
import { CategoryModel, DiscussionCategory } from '@/models/dicsussion';

import { UserRepository } from '@/repositories/user.repository';
import { DiscussionRepository } from '@/repositories/discussion.repository';
import { SubscriptionRepository } from '@/repositories/subscription.repository';

import { UserArgs, UserRelation, UserSearchArgs } from '../types/user.types';
import { DiscussionPagination, PageArgs, UserPagination } from '../types/pagination.types';
import { SearchService } from '@/services/search.service';
import { DebateStatus } from '@/models/debate';
import { PanelStatus } from '@/models/panel';

@Service()
@Resolver(() => User)
export class UserResolver {

  @Inject() private readonly repository: UserRepository;
  @Inject() private readonly discussions: DiscussionRepository;
  @Inject() private readonly subscriptions: SubscriptionRepository;

  @Inject() private readonly search: SearchService;

  @Query(() => UserPagination)
  async users(@Args() args: UserSearchArgs, @Ctx() context: Context) {
    const filters = [];

    if (args.filters.occupation) {
      filters.push({ "info.occupation": args.filters.occupation });
    }
    
    if (args.filters.education) {
      filters.push({ "info.education": args.filters.education });
    }
    
    if (args.filters.country) {
      filters.push({ "info.country": args.filters.country });
    }
    
    if (args.filters.politics) {
      filters.push({ "info.politics": args.filters.politics });
    }
    
    if (args.filters.religion) {
      filters.push({ "info.religion": args.filters.religion });
    }
    
    if (args.filters.category) {
        filters.push({ "info.category": args.filters.category });
    }
    
    const result = await this.search.filterOpponents(context.user, args.filters, args.page);

    return {
      next: result.next,
      hasMore: result.hasMore,
      page: await this.repository.find({ _id: { $in: result.page.map(d => d.id) } })
    };
  }

  @Query(() => User, { nullable: true })
  async user(@Args() args: UserArgs): Promise<User> {
    if (!args.id && !args.username) {
      throw new Error('ID or username not provided');
    }

    if (args.id) {
      return await this.repository.get(args.id);
    }

    return await this.repository.one({ username: args.username });
  }


  @FieldResolver(() => UserRelation)
  relation(@Root() user: DocumentType<User>, @Ctx() context: Context) {
    return context.loaders.userRelations.load({ current: context.user.id, target: user.id });
  }

  @FieldResolver(() => UserPagination)
  async followers(@Root() user: DocumentType<User>, @Args() args: PageArgs) {
    const subscriptions = await this.subscriptions.paginate(
      { subscription: user, subscriptionModel: SubscriptionTargetModel.User, active: true },
      { ...args.page, populate: [{ path: 'follower' }] }
    );

    return { ...subscriptions, page: subscriptions.page.map(s => s.follower) };
  }
  
  @FieldResolver(() => UserPagination, {name: 'subscriptions'})
  async userSubscriptions(@Root() user: DocumentType<User>, @Args() args: PageArgs) {
    const subscriptions = await this.subscriptions.paginate(
      { follower: user, subscriptionModel: SubscriptionTargetModel.User },
      { ...args.page, populate: [{ path: 'subscription' }] }
    );

    return { ...subscriptions, page: subscriptions.page.map(s => s.subscription) };
  }

  @FieldResolver(() => DiscussionPagination, { name: 'discussions' })
  async userDiscussions(@Root() user: DocumentType<User>, @Args() args: PageArgs) {
    return this.discussions.paginate(
      {
        $or: [
          { kind: 'Challenge', author: user.id },
          { kind: 'Post', author: user.id },
          {
            'kind': 'Debate',
            'participants.user': user.id,
            'status': { $in: [ DebateStatus.voting, DebateStatus.finished ]}
          },
          {
            'kind': 'Panel',
            'participants.user': user.id,
            'status': { $in: [PanelStatus.active, PanelStatus.finished] }
          }
        ]
      },
      { ...args.page, sortBy: 'created', sortDir: 'dsc' }
    );
  }

  // @FieldResolver(() => DiscussionCategory, { name: 'bestCategory', nullable: true })
  // async userBestCategory(@Root() user: DocumentType<User>) {
  //   if (user.stat.bestCategory && !isDocument(user.stat.bestCategory)) {
  //     return this.categories.get((user.stat.bestCategory as ObjectId).toHexString());
  //   }

  //   return user.stat.bestCategory;
  // }

  @Query(() => DiscussionCategory, { nullable: true })
  async bestCategory(@Arg('id') id: string) {
    const { _id } = await Container.get(UserRepository)
      .findBestCategory(await UserModel.findById(id));

    return CategoryModel.findById(_id);
  }
}
