import { Inject, Service } from "typedi";
import { Args, ArgsType, Field, FieldResolver, ObjectType, Query, Resolver } from "type-graphql";

import { DiscussionPagination, Paginated, UserPagination } from "../types/pagination.types";

import { UserRepository } from "@/repositories/user.repository";

import { DiscussionRepository, } from "@/repositories/discussion.repository";
import { SearchService } from "@/services/search.service";
import { PaginatedResult } from "@/repositories/paginated.repository";
import { Discussion } from "@/models/dicsussion";


@ObjectType()
class Search {
  @Field(() => UserPagination)
  users: UserPagination;

  @Field(() => DiscussionPagination)
  discussions: DiscussionPagination;
  
  @Field(() => DiscussionPagination)
  tags: DiscussionPagination;

  @Field(() => DiscussionPagination)
  categories: DiscussionPagination;
}

@ArgsType()
class SearchArgs extends Paginated {
  @Field() query: string;
}

@Service()
@Resolver(() => Search)
export class SearchResolver {

  @Inject() private discussions: DiscussionRepository;
  @Inject() private users: UserRepository;

  @Inject() private service: SearchService;

  @Query(() => Search)
  search() {
    return {};
  }


  @FieldResolver(() => UserPagination, { name: 'users' })
  async searchUsers(@Args() args: SearchArgs) {
    const docs = await this.service.users(args.query, args.page);

    return {
      next: docs.next,
      hasMore: docs.hasMore,
      page: await this.users.find({ _id: { $in: docs.page.map(d => d.id) } })
    };
  }
  
  @FieldResolver(() => DiscussionPagination, { name: 'discussions'})
  async searchDiscussions(@Args() args: SearchArgs): Promise<PaginatedResult<Discussion>>{
    const docs = await this.service.discussion(args.query, args.page);

    return {
      next: docs.next,
      hasMore: docs.hasMore,
      page: await this.discussions.find({ _id: { $in: docs.page.map(d => d.id) }})
    };
  }

  @FieldResolver(() => DiscussionPagination, { name: 'tags' })
  async searchTags(@Args() args: SearchArgs) {
    const docs = await this.service.discussion(null, args.page, { filter: `tags = '${args.query}'` });

    return {
      next: docs.next,
      hasMore: docs.hasMore,
      page: await this.discussions.find({ _id: { $in: docs.page.map(d => d.id) }})
    };
  }

  @FieldResolver(() => DiscussionPagination, { name: 'categories' })
  async searchCategories(@Args() args: SearchArgs) {
    const docs = await this.service.discussion(null, args.page, { filter: `category = '${args.query}'` });

    return {
      next: docs.next,
      hasMore: docs.hasMore,
      page: await this.discussions.find({ _id: { $in: docs.page.map(d => d.id) }})
    };
  }
}