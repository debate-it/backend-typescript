import { Arg, Args, Authorized, Ctx, FieldResolver, ID, Mutation, Query, Resolver, Root } from 'type-graphql';
import { DocumentType, isDocument } from '@typegoose/typegoose';
import { Inject, Service } from 'typedi';
import { Types } from 'mongoose';

import { Context } from '../context';

import { User } from '@/models/user';
import { Panel } from '@/models/panel';
import { Debate } from '@/models/debate';

import {
  Request,
  OpenRequest,
  PanelRequest,
  DebateRequest,
  PanelRequestCandidacy,
  DebateRequestCandidacy,
  OpenRequestType,
  RequestType,
} from '@/models/request';

import { RequestRepository } from '@/repositories/request.repository';
import { RequestCandidacyRepository } from '@/repositories/request.candidacy.repository';

import { IdArgs } from '@/graphql/types/shared.types';
import { ResolvePanelRequestArgs } from '@/graphql/types/panel.types';
import { OpenRequestPagination, PageArgs, RequestCandidacyPagination } from '@/graphql/types/pagination.types';

import {
  AddPanelParticipantArgs,
  CreateDebateCandidacy,
  CreateDebateRequestArgs,
  CreateOpenRequestArgs,
  CreatePanelCandidacy,
  CreatePanelRequestArgs,
  RequestResolveArgs,
  ResolveOpenPanelRequestArgs,
  ResolveOpenRequestArgs
} from '@/graphql/types/request.types';
import { EnvelopError } from '@envelop/core';




@Service()
@Resolver(() => Request)
export class RequestResolver {

  @Inject() private service: RequestRepository;

  @Query(() => Request, { nullable: true })
  async request(@Arg('id', () => ID) id: string) {
    return this.service.get(id);
  }

  @Query(() => [Request])
  async requests() {
    return this.service.find({});
  }

  /* ---------------  Debate Requests  --------------- */

  @Authorized()
  @Mutation(() => DebateRequest, { nullable: true })
  async createDebateRequest(@Args() args: CreateDebateRequestArgs, @Ctx() context: Context): Promise<Request> {
    return this.service.initiateDebate(context.user, args);
  }

  @Authorized()
  @Mutation(() => Debate, { nullable: true })
  async resolveDebateRequest(@Args() args: RequestResolveArgs, @Ctx() context: Context) {
    return this.service.resolveDebateRequest(args.id, args.status, context.user);
  }

  /* ---------------  Panel Requests  --------------- */

  @Authorized()
  @Mutation(() => PanelRequest, { nullable: true })
  async createPanelRequest(@Args() args: CreatePanelRequestArgs, @Ctx() context: Context): Promise<Request> {
    return this.service.initiatePanel(context.user, args);
  }

  @Authorized()
  @Mutation(() => PanelRequest, { nullable: true })
  async resolvePanelRequest(@Args() args: ResolvePanelRequestArgs, @Ctx() context: Context) {
    return this.service.resolvePanelRequest(args.id, args.status, context.user, args.argument);
  }

  @Authorized()
  @Mutation(() => PanelRequest, { nullable: true })
  async addPanelParticipant(@Args() args: AddPanelParticipantArgs, @Ctx() context: Context) {
    return this.service.addPanelParticipant(args.id, context.user, args.participant);
  }
}

@Service()
@Resolver(() => PanelRequest)
export class PanelRequestResolver {

  @FieldResolver()
  panel(@Root() request: DocumentType<PanelRequest>): string {
    if (isDocument(request.panel)) {
      return request.panel.id;
    } else {
      return (request.panel as Types.ObjectId).toHexString();
    }
  }
}

@Service()
@Resolver(() => OpenRequest)
export class OpenRequestResolver {
  @Inject() private candidacies: RequestCandidacyRepository;
  @Inject() private repository: RequestRepository;

  @Query(() => OpenRequest)
  openRequest(@Args() args: IdArgs) {
    return this.repository.get(args.id);
  }

  @Authorized()
  @Query(() => OpenRequestPagination)
  userOpenRequests(@Args() args: PageArgs, @Ctx() context: Context) {
    return this.repository.paginate(
      { kind: RequestType.Open, initiator: context.user, resolved: false },
      { ...args.page, sortBy: 'created', sortDir: 'dsc' }
    );
  }

  @FieldResolver(() => OpenRequestType)
  type(@Root() request: DocumentType<OpenRequest>) {
    return request.type;
  }

  @Authorized()
  @Query(() => [OpenRequest])
  openRequestSample(@Ctx() context: Context) {
    return this.repository.findOpenRequests(context.user);
  }
  
  @Authorized()
  @Mutation(() => OpenRequest, { nullable: true })
  async createOpenRequest(@Args() args: CreateOpenRequestArgs, @Ctx() context: Context) {
    return this.repository.initiateOpenRequest(context.user, args)
  }

  @Authorized()
  @Mutation(() => DebateRequestCandidacy, { nullable: true })
  async submitDebateCandidacy(@Args() args: CreateDebateCandidacy, @Ctx() context: Context) {
    return this.repository.submitDebateCandidacy(args.id, context.user);
  }

  @Authorized()
  @Mutation(() => PanelRequestCandidacy, { nullable: true })
  async submitPanelCandidacy(@Args() args: CreatePanelCandidacy, @Ctx() context: Context) {
    return this.repository.submitPanelCandidacy(args.id, context.user, args.argument);
  }

  @Authorized()
  @Mutation(() => [Debate])
  async resolveOpenDebateRequest(@Args() args: ResolveOpenRequestArgs, @Ctx() context: Context) {
    return this.repository.resolveOpenDebateRequest(args.id, context.user, args.candidates);
  }
  
  @Authorized()
  @Mutation(() => Panel)
  async resolveOpenPanelRequest(@Args() args: ResolveOpenPanelRequestArgs, @Ctx() context: Context) {
    return this.repository.resolveOpenPanelRequest(args.id, context.user, args.candidates, args.argument);
  }

  @Authorized()
  @FieldResolver(() => RequestCandidacyPagination)
  candidates(@Root() request: DocumentType<OpenRequest>, @Args() args: PageArgs, @Ctx() context: Context) {
    if (context.user.id !== (request.initiator as User).id) {
      throw new EnvelopError('Only request initiator can view candidates', { code: 'REQUEST_CANDIDATES_NOT_ALLOWED'});
    }

    return this.candidacies.paginate({ request: request }, { ...args.page });
  }
}