import { AuthChecker } from 'type-graphql';
import { DocumentType } from '@typegoose/typegoose';

import { User } from '@/models/user';

import { DataLoaders, Loaders } from './loaders';


export interface Context {
  user?: DocumentType<User>;
  loaders?: DataLoaders
}

export const authChecker: AuthChecker<Context> = ({ context }, roles) => {
  const userIsFound = !!context.user;
  const userIsActive = userIsFound && context.user?.auth.active;
  const userIsNotBlocked = userIsFound && !context.user?.blocked;

  const markedForActivation = roles.some(role => role === 'INACTIVE');

  const userIsAuthorized = userIsFound && roles
    .map(role => context.user.auth.roles.find(r => r.name === role) !== undefined)
    .reduce((acc, e) => acc && e, true);

  return (userIsActive && userIsNotBlocked && userIsAuthorized) || (userIsFound && markedForActivation);
}

export const contextFactory = ({ req, connection }) => 
  ({ user: req.user, loaders: Loaders.create() });
