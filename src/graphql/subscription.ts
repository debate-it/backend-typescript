import Container from 'typedi';
import plugin from 'fastify-plugin';

import { SubscriptionServer } from 'subscriptions-transport-ws';

import { FastifyPluginAsync } from 'fastify';
import { execute, GraphQLSchema, subscribe } from 'graphql';

import { UserRepository } from '@/repositories/user.repository';
import { AuthService } from '@/services/auth.service';

import { Context } from './context';


declare module 'fastify' {
  interface FastifyInstance {
    subscriptions: SubscriptionServer;
  }
}

interface SubscriptionPluginOptions {
  schema: GraphQLSchema;
  path: string
}

const onConnect = async (connection, websocket, context) => {
  if (connection.hasOwnProperty('Authorization')) {
    const auth = Container.get(AuthService);
    const users = Container.get(UserRepository);

    const payload = auth.decode((connection as any).Authorization);
    const user = await users.get(payload.id);

    return { user } as Context;
  }

  return {};
};

const subscriptions: FastifyPluginAsync<SubscriptionPluginOptions> = async (fastify, options) => {
  const serverOptions = { execute, subscribe, schema: options.schema, onConnect }; 

  fastify.decorate('subscriptions', null);

  fastify.ready(() => {
    fastify.subscriptions = SubscriptionServer
      .create(serverOptions, { server: fastify.server, path: options.path })
  });
};

export default plugin(subscriptions);
