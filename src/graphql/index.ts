import { Container } from 'typedi';
import { envelop, useSchema, useExtendContext, useMaskedErrors, EnvelopError, useLogger, useImmediateIntrospection } from '@envelop/core';

import { getGraphQLParameters, processRequest, renderGraphiQL, sendResult, shouldRenderGraphiQL } from 'graphql-helix';

import { buildSchema } from 'type-graphql';
import { RedisPubSub } from 'graphql-redis-subscriptions';

import { FastifyInstance } from 'fastify';
import { GraphQLError } from 'graphql';

import upload from './upload';
import subscriptions from './subscription';

import { authChecker, contextFactory } from './context';

import { RedisService } from '@/services/redis.service';
import { useDepthLimit } from '@envelop/depth-limit';
import { useDisableIntrospection } from '@envelop/disable-introspection';
import { logger } from '@/services/log.service';


export namespace GraphQL {
  const path = '/graphql';

  export async function initialize(app: FastifyInstance, isProduction: boolean = false) {
    const schema = await compile(isProduction);
    logger.info({ message: 'GraphQL schema has been build' })

    const getEnveloped = envelop({
      plugins: [
        useSchema(schema),
        useExtendContext(contextFactory),
        useDepthLimit({ maxDepth: 13 }),
        useMaskedErrors({
          isDev: !isProduction,
          errorMessage: 'Something went wrong, try again later.',
          formatError: (error: GraphQLError, message, isDev) => {
            if (error.originalError && error.originalError instanceof EnvelopError === false) {
              return new GraphQLError('Something went wrong, try again later.')
            }

            return error
          }
        }),

        isProduction 
          ? useDisableIntrospection()
          : useImmediateIntrospection()
      ]
    });


    app.register(subscriptions, { path, schema });
    app.register(upload, { maxFileSize: 10000000, maxFiles: 10,  });

    app.route({
      method: ['GET', 'POST'],
      url: '/graphql',

      handler: async (req, res) => {
        const context = getEnveloped({ req: { ...req.raw, user: req.user } });

        const request = {
          body: req.body,
          headers: req.headers,
          method: req.method,
          query: req.query
        };
        
        if (shouldRenderGraphiQL(request)) {
          return res
            .type('text/html')
            .send(renderGraphiQL({ 
              useWebSocketLegacyProtocol: true,
              subscriptionsEndpoint: 'ws://localhost:9000/graphql'
            }));
        }

        const params = getGraphQLParameters(request);
        const result = await processRequest({ ...params, ...context, request });

        for (const [header, value] of Object.entries(res.getHeaders())) {
          res.raw.setHeader(header, value);
        }

        if (result.type === 'PUSH') {
          return res
            .status(422)
            .send({ errors: [new GraphQLError('Susbcriptions should be sent over WebSocket')] })
        } 
          
        sendResult(result, res.raw);
      }
    });
    
    logger.info({ message: 'GraphQL handler is initialized' })
  }


  async function compile(isProduction: boolean = false) {
    const pubSub = new RedisPubSub({
      publisher: RedisService.create(),
      subscriber: RedisService.create(),
    });

    return buildSchema({
      resolvers: [ 
        __dirname + "/resolvers/**/*.ts", 
        __dirname + "/resolvers/**/*.js"
      ],
      dateScalarMode: 'timestamp',
      container: Container,
      authChecker,
      pubSub,
      emitSchemaFile: isProduction && {
        path: __dirname + "/schema.gql",
        sortedSchema: false, 
      }
    });
  }
}
