# -----------------------------------------------
# !!! THIS FILE WAS GENERATED BY TYPE-GRAPHQL !!!
# !!!   DO NOT MODIFY THIS FILE BY YOURSELF   !!!
# -----------------------------------------------

type DiscussionTag {
  name: String!
}

type DiscussionCategoryStat {
  discussions: Int!
  followers: Int!
}

type DiscussionCategory {
  id: ID!
  name: String!
  description: String!
  stat: DiscussionCategoryStat!
  feed(page: PaginationInfo!, filter: [DiscussionType!]): DiscussionPagination!
  followers(page: PaginationInfo!): UserPagination!
  isSubscribed: Boolean!
}

enum DiscussionType {
  Debate
  Challenge
  Post
}

type DebateArgument {
  participant: Int!
  argument: String!
  time: DateTime
}

"""
The javascript `Date` as string. Type represents date and time as the ISO Date string.
"""
scalar DateTime

type UserInfo {
  about: String
  avatar: String
  education: String
  occupation: String
  country: String
  politics: String
  religion: String
}

type UserStat {
  points: Int!
  followers: Int!
  wins: Int!
  bestCategory: DiscussionCategory
}

type UserProgress {
  level: Float!
  earned: Float!
  needed: Float
}

type User {
  id: ID!
  username: String!
  progress: UserProgress!
  info: UserInfo!
  stat: UserStat!
  fullname: String
  relation: UserRelation!
  followers(page: PaginationInfo!): UserPagination!
  subscriptions(page: PaginationInfo!): UserPagination!
  discussions(page: PaginationInfo!): DiscussionPagination!
}

enum UserRelation {
  none
  follower
  subscription
  friend
  self
}

type DebateParticipant {
  score: Int!
  user: User!
}

enum DiscussionSize {
  short
  medium
  long
}

enum DebateStatus {
  active
  voting
  finished
}

type EventRecord {
  type: EventType!
  time: DateTime!
  link: String!
  title: String!
  message: String!
}

enum EventType {
  DebateRequestSent
  DebateCreated
  DebateArgumentAdded
  DebateFinished
  PanelRequestSent
  PanelRequestAccepted
  PanelStarted
  PanelFinished
  RequestRejected
  ChallengeResponse
  ChallengeFinished
  ChallengeWinnerChosen
  UserFollow
  UserLevelUp
  CommentCreated
  Vote
  RequestCandidacySubmitted
}

type Event {
  id: ID!
  from: DateTime!
  activity: [EventRecord!]!
}

type PanelParticipant {
  user: User!
  score: Int!
  argument: String!
}

enum PanelStatus {
  waiting
  active
  finished
}

type PostAlignment {
  agree: Int!
  disagree: Int!
}

enum PostAlignmentSide {
  agree
  disagree
}

type Comment {
  id: ID!
  author: User!
  content: String!
  alignment: Int
  created: DateTime!
  votes: Int!
  type: CommentType!
  voted: Boolean!
}

enum CommentType {
  Challenge
  Debate
  Panel
  Post
}

type RequestBase {
  topic: String!
  size: DiscussionSize!
  category: DiscussionCategory!
  tags: [DiscussionTag!]!
}

enum RequestStatus {
  submitted
  accepted
  rejected
}

type PanelRequestRecipient {
  user: User!
  status: RequestStatus!
}

enum OpenRequestType {
  Debate
  Panel
}

enum ChallengeStatus {
  active
  waitingForOpponent
  finished
}

type ReportLog {
  description: String!
  reasons: [ReportReason!]!
  author: User!
  offenders: [User!]!
  date: DateTime!
}

enum ReportReason {
  hateSpeech
  ruleViolation
  spam
  other
}

type ReportResolution {
  user: User!
  decision: ReportResolutionDecision!
  comment: String
}

enum ReportResolutionDecision {
  none
  warning
  ban
}

type Report {
  id: ID!
  target: Discussion!
  log: [ReportLog!]!
  resolution: [ReportResolution!]!
}

type UserPagination {
  page: [User!]!
  next: String
  hasMore: Boolean
}

type DiscussionPagination {
  page: [Discussion!]!
  next: String
  hasMore: Boolean
}

type PostPagination {
  page: [Post!]!
  next: String
  hasMore: Boolean
}

type DebatePagination {
  page: [Debate!]!
  next: String
  hasMore: Boolean
}

type RequestPagination {
  page: [Request!]!
  next: String
  hasMore: Boolean
}

type OpenRequestPagination {
  page: [OpenRequest!]!
  next: String
  hasMore: Boolean
}

type RequestCandidacyPagination {
  page: [RequestCandidacy!]!
  next: String
  hasMore: Boolean
}

type CommentPagination {
  page: [Comment!]!
  next: String
  hasMore: Boolean
}

type ChallengePagination {
  page: [Challenge!]!
  next: String
  hasMore: Boolean
}

type CategoryPagination {
  page: [DiscussionCategory!]!
  next: String
  hasMore: Boolean
}

type ReportPagination {
  page: [Report!]!
  next: String
  hasMore: Boolean
}

type CommentWithDiscussion {
  discussion: Discussion!
  comment: Comment!
}

type AuthInfo {
  active: Boolean!
  token: String!
  refresh: String!
}

type AuthResponse {
  status: AuthStatus!
  error: String
  info: AuthInfo
}

enum AuthStatus {
  success
  error
}

type DebateRealtimeArgumentResponse {
  index: Int!
  argument: DebateArgument!
}

type LeaderboardItem {
  score: Int!
  entry: LeaderboardEntry!
}

union LeaderboardEntry = User | Debate | Post | Challenge

type LeaderboardPagination {
  page: [LeaderboardItem!]!
  next: String
  hasMore: Boolean
}

type Leaderboard {
  debates(page: PaginationInfo!): LeaderboardPagination!
  challenges(page: PaginationInfo!): LeaderboardPagination!
  posts(page: PaginationInfo!): LeaderboardPagination!
  users(page: PaginationInfo!): LeaderboardPagination!
}

type ProfileSubscriptionPagination {
  page: [SubscriptionEntity!]!
  next: String
  hasMore: Boolean
}

union SubscriptionEntity = User | DiscussionCategory

type ProfileFeedPagination {
  page: [Discussion!]!
  next: String
  hasMore: Boolean
}

type ProfileActivityPagination {
  page: [Event!]!
  next: String
  hasMore: Boolean
}

type ProfileIndicators {
  notifications: Int!
  requests: Int!
  debates: Int!
}

type Profile {
  info: User!
  indicators: ProfileIndicators!
  feed(page: PaginationInfo!): ProfileFeedPagination!
  debates(page: PaginationInfo!, status: DebateStatus!): DebatePagination!
  requests(page: PaginationInfo!, source: RequestSource!, status: RequestStatus): RequestPagination!
  activity(page: PaginationInfo!): ProfileActivityPagination!
  subscriptions: ProfileSubscriptionPagination!
  roles: [String!]!
  userSubscriptions(page: PaginationInfo!): UserPagination!
  categorySubscriptions(page: PaginationInfo!): CategoryPagination!
}

enum RequestSource {
  any
  incoming
  outgoing
}

type ReportOverview {
  pending: Float!
  resolved: Float!
}

type Search {
  users(page: PaginationInfo!, query: String!): UserPagination!
  discussions(page: PaginationInfo!, query: String!): DiscussionPagination!
  tags(page: PaginationInfo!, query: String!): DiscussionPagination!
  categories(page: PaginationInfo!, query: String!): DiscussionPagination!
}

interface Discussion {
  id: ID!
  created: DateTime!
  topic: String!
  size: DiscussionSize!
  category: DiscussionCategory!
  tags: [DiscussionTag!]!
}

interface Request {
  id: ID!
  created: DateTime!
  info: RequestBase!
  initiator: User!
}

interface RequestCandidacy {
  id: ID!
  created: DateTime!
  candidate: User!
  status: RequestStatus!
}

input PaginationInfo {
  cursor: String
  limit: Int = 20
}

input UserFilters {
  occupation: String
  education: String
  country: String
  category: String
  religion: String
  politics: String
}

input ExtraInfo {
  fullname: String
  about: String
  avatar: String
  education: String
  occupation: String
  country: String
  politics: String
  religion: String
}

input DiscussionInput {
  topic: String!
  category: String!
  size: DiscussionSize!
  tags: [String!]
}

input UserInfoUpdateInput {
  fullname: String
  about: String
  country: String
  education: String
  occupation: String
  politics: String
  religion: String
}

input ReportResolutionInput {
  user: ID!
  decision: ReportResolutionDecision!
  comment: String
}

type Debate implements Discussion {
  id: ID!
  created: DateTime!
  topic: String!
  size: DiscussionSize!
  category: DiscussionCategory!
  tags: [DiscussionTag!]!
  deadline: DateTime!
  status: DebateStatus!
  participants: [DebateParticipant!]!
  arguments: [DebateArgument!]!
  votedOn: Int
}

type Panel implements Discussion {
  id: ID!
  created: DateTime!
  topic: String!
  size: DiscussionSize!
  category: DiscussionCategory!
  tags: [DiscussionTag!]!
  status: PanelStatus!
  deadline: DateTime
  participants: [PanelParticipant!]!
  votedOn: Int
}

type Post implements Discussion {
  id: ID!
  created: DateTime!
  topic: String!
  size: DiscussionSize!
  category: DiscussionCategory!
  tags: [DiscussionTag!]!
  author: User!
  alignment: PostAlignment!
  votedOn: PostAlignmentSide
}

type DebateRequest implements Request {
  id: ID!
  created: DateTime!
  info: RequestBase!
  initiator: User!
  status: RequestStatus!
  recipient: User!
}

type PanelRequest implements Request {
  id: ID!
  created: DateTime!
  info: RequestBase!
  initiator: User!
  expiration: DateTime
  accepted: Int!
  closed: Boolean!
  recipients: [PanelRequestRecipient!]!
  panel: String!
}

type OpenRequest implements Request {
  id: ID!
  created: DateTime!
  info: RequestBase!
  initiator: User!
  type: OpenRequestType!
  resolved: Boolean!
  candidacies: Int!
  candidates(page: PaginationInfo!): RequestCandidacyPagination!
}

type DebateRequestCandidacy implements RequestCandidacy {
  id: ID!
  created: DateTime!
  candidate: User!
  status: RequestStatus!
}

type PanelRequestCandidacy implements RequestCandidacy {
  id: ID!
  created: DateTime!
  candidate: User!
  status: RequestStatus!
}

type Challenge implements Discussion {
  id: ID!
  created: DateTime!
  topic: String!
  size: DiscussionSize!
  category: DiscussionCategory!
  tags: [DiscussionTag!]!
  position: String!
  author: User!
  deadline: DateTime!
  responses: Int!
  status: ChallengeStatus!
  finished: Boolean!
}

type Query {
  authenticate(token: String!): AuthResponse!
  verifyUserCredentials(email: String!, username: String!): Boolean!
  verifyPasswordReset(id: String!): Boolean!
  category(id: ID, name: String): DiscussionCategory!
  categories: [DiscussionCategory!]!
  challenge(id: ID!): Challenge
  challenges(page: PaginationInfo!): ChallengePagination!
  comment(id: String!): Comment
  comments(page: PaginationInfo!, target: ID!, type: CommentType!, order: CommentOrder!): CommentPagination!
  userComment(target: ID!, type: CommentType!): Comment
  commentWithDiscussion(id: ID!): CommentWithDiscussion!
  debate(id: ID!): Debate
  debates: [Debate!]!
  leaderboards: Leaderboard!
  panel(id: ID!): Panel
  post(id: ID!): Post
  posts: [Post!]!
  profile: Profile!
  profileCountNewNotifications: Int!
  reports(page: PaginationInfo!): ReportPagination!
  report(id: ID!): Report!
  reportOverview: ReportOverview!
  request(id: ID!): Request
  requests: [Request!]!
  openRequest(id: ID!): OpenRequest!
  userOpenRequests(page: PaginationInfo!): OpenRequestPagination!
  openRequestSample: [OpenRequest!]!
  search: Search!
  users(page: PaginationInfo!, filters: UserFilters!): UserPagination!
  user(id: ID, username: String): User
  bestCategory(id: String!): DiscussionCategory
}

enum CommentOrder {
  new
  old
  top
}

type Mutation {
  signup(email: String!, username: String!, password: String!, extra: ExtraInfo): AuthResponse!
  login(username: String!, password: String!): AuthResponse!
  activate(code: String!): Boolean!
  resendActivationCode: Boolean!
  forgotPassword(email: String!): Boolean!
  resetPassword(id: String!, password: String!): Boolean!
  changePassword(password: String!): Boolean!
  initialSubscription(categories: [ID!]!): [DiscussionCategory!]!
  initiateChallenge(discussion: DiscussionInput!, hours: Int!, position: String!): Challenge
  addChallengeResponse(id: ID!, response: String!): Challenge
  chooseChallengeOpponent(id: ID!, opponent: ID!): Debate
  createComment(target: ID!, type: CommentType!, content: String!): Comment
  voteOnComment(id: ID!): Comment
  addArgument(id: ID!, argument: String!): Debate!
  voteOnDebate(id: ID!, participant: Int!): Debate!
  voteOnPanel(id: ID!, participant: Int!): Panel
  createPost(discussion: DiscussionInput!): Post
  alignWithPost(id: ID!, alignment: PostAlignmentSide!): Post
  subscribe(target: ID!, type: SubscriptionType!): SubscriptionEntity
  updateNotificationsSeen: Boolean!
  updateInfo(options: UserInfoUpdateInput!): User!
  updateEmail(email: String!): User!
  setDeviceToken(token: String!): Boolean!
  updateProfileImage(file: Upload!): Boolean!
  resolveReport(id: ID!, decisions: [ReportResolutionInput!]!): Report!
  submitReport(discussionId: ID!, reasons: [ReportReason!]!, description: String!, offenders: [ID!]!): Boolean!
  createDebateRequest(recipient: ID!, discussion: DiscussionInput!): DebateRequest
  resolveDebateRequest(id: ID!, status: RequestStatus!): Debate
  createPanelRequest(recipients: [ID!]!, position: String!, discussion: DiscussionInput!): PanelRequest
  resolvePanelRequest(id: ID!, status: RequestStatus!, argument: String): PanelRequest
  addPanelParticipant(id: ID!, participant: ID!): PanelRequest
  createOpenRequest(discussion: DiscussionInput!, type: OpenRequestType!): OpenRequest
  submitDebateCandidacy(id: ID!): DebateRequestCandidacy
  submitPanelCandidacy(id: ID!, argument: String!): PanelRequestCandidacy
  resolveOpenDebateRequest(id: ID!, candidates: [ID!]!): [Debate!]!
  resolveOpenPanelRequest(candidates: [ID!]!, id: ID!, argument: String!): Panel!
}

enum SubscriptionType {
  User
  Category
}

"""The `Upload` scalar type represents a file upload."""
scalar Upload

type Subscription {
  debateRealtimeArguments(id: ID!, participant: Int!): DebateRealtimeArgumentResponse!
}
