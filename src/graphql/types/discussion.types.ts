import { Field, InputType } from 'type-graphql';

import { Discussion, DiscussionSize } from '@/models/dicsussion';

@InputType()
export class DiscussionInput implements Required<Omit<Discussion, Discussion['id']>> {
  @Field()
  topic: string;

  @Field()
  category: string;

  @Field(() => DiscussionSize)
  size: DiscussionSize;

  @Field(() => [String], { nullable: true })
  tags: string[];
}