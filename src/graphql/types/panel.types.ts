import { ArgsType, Field, ID, Int } from "type-graphql";
import { NotEquals } from "class-validator";

import { RequestStatus } from "@/models/request";

@ArgsType()
export class PanelVoteArgs {
  @Field(() => ID)
  id: string;

  @Field(() => Int)
  participant: number;
}

@ArgsType()
export class ResolvePanelRequestArgs {
  @Field(() => ID)
  id: string;

  @NotEquals(RequestStatus.submitted)
  @Field(() => RequestStatus)
  status: RequestStatus;

  @Field({ nullable: true })
  argument?: string;
}