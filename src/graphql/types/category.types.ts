import { ArgsType, Field, ID } from "type-graphql";

import { DiscussionType } from "@/models/dicsussion";

import { PaginationInfo } from "./pagination.types";

@ArgsType()
export class CategoryArgs {
  @Field(() => ID, { nullable: true })
  id: string;

  @Field({ nullable: true })
  name: string;
}

@ArgsType()
export class CategoryFeedArgs {
  @Field(() => PaginationInfo)
  page: PaginationInfo;

  @Field(() => [DiscussionType], { nullable: true })
  filter?: DiscussionType[];
}

@ArgsType()
export class InitialSubscriptionArgs {
  @Field(() => [ID])
  categories: string[]
}
