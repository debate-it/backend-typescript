import { IsMongoId } from "class-validator";
import { ArgsType, Field, ID } from "type-graphql";

@ArgsType()
export class IdArgs {
  @IsMongoId({ message: 'Invalid id'})
  @Field(() => ID)
  id: string;
}