import { Post } from "@/models/post";
import { Debate } from "@/models/debate";
import { Challenge } from "@/models/challenge";

import { Discussion } from "@/models/dicsussion";


export enum FeedEntryType {
  Challenge,
  Debate,
  Post
}

export function resolveFeedEntry(value: Discussion) {
  if (isDebate(value)) {
    return 'Debate';
  }

  if (isPost(value)) {
    return 'Post';
  }

  if (isChallenge(value)) {
    return 'Challenge';
  }
}

export function isDebate(value: Discussion) {
  const debate = value as Debate;

  return (
    debate.arguments &&
    debate.participants &&
    debate.participants?.length === 2
  );
}

export function isPost(value: Discussion) {
  const post = value as Post;

  return (
    post.alignment &&
    typeof post.alignment.agree === 'number' &&
    typeof post.alignment.disagree === 'number'
  )
}

export function isChallenge(value: Discussion) {
  const challenge = value as Challenge;

  return (
    challenge.responses &&
    typeof challenge.responses === 'number'
  );
}
