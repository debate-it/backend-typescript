import { ArgsType, createUnionType, Field, ID } from "type-graphql";

import { User } from "@/models/user";
import { DiscussionCategory } from "@/models/dicsussion";
import { SubscriptionTargetModel } from "@/models/subscription";

export const SubscriptionType = createUnionType({
  name: 'SubscriptionEntity',
  types: () => [User, DiscussionCategory] as const,
  resolveType: (value) => {
    if ((value as User).username) {
      return 'User';
    }

    return 'DiscussionCategory';
  }
});

@ArgsType()
export class SubscriptionArgs {
  @Field(() => ID)
  public target: string;

  @Field(() => SubscriptionTargetModel)
  public type: SubscriptionTargetModel;
}