import { Discussion } from "@/models";
import { Comment, CommentTargetType } from "@/models/comment";
import { ArgsType, Field, ID, ObjectType, registerEnumType } from "type-graphql";
import { Paginated } from "./pagination.types";

@ArgsType()
export class CreateCommentArgs {
  @Field(() => ID)
  public target: string;

  @Field(() => CommentTargetType)
  public type: CommentTargetType;

  @Field()
  public content: string;
}

export enum CommentOrder { new, old, top }

@ArgsType()
export class UserCommentArgs {
  @Field(() => ID)
  public target: string;

  @Field(() => CommentTargetType)
  public type: CommentTargetType;
}

@ArgsType()
export class CommentSearchArgs extends Paginated {
  @Field(() => ID)
  public target: string;

  @Field(() => CommentTargetType)
  public type: CommentTargetType;

  @Field(() => CommentOrder)
  public order: CommentOrder;
}

@ObjectType()
export class CommentWithDiscussion {
  @Field(() => Discussion)
  discussion: Discussion;

  @Field(() => Comment)
  comment: Comment;
}

registerEnumType(CommentOrder, { name: 'CommentOrder' });