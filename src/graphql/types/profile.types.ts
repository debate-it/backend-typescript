import { ArgsType, Field, InputType, ObjectType } from "type-graphql";

import { SubscriptionTarget } from "@/models/subscription";
import { Discussion } from "@/models/dicsussion";
import { RequestStatus } from "@/models/request";
import { DebateStatus } from "@/models/debate";
import { Event } from "@/models/event";

import { UserInfoUpdate } from "@/repositories/user.repository";

import { Paginated, PaginatedResponse } from "./pagination.types";
import { SubscriptionType } from "./subscription.types";
import { RequestSource } from "./request.types";


@InputType()
export class UserInfoUpdateInput implements UserInfoUpdate {
    @Field({nullable: true})
    public fullname?: string;

    @Field({nullable: true})
    public about?: string;

    @Field({nullable: true})
    public country?: string;

    @Field({nullable: true})
    public education?: string;

    @Field({nullable: true})
    public occupation?: string;

    @Field({nullable: true})
    public politics?: string;

    @Field({nullable: true})
    public religion?: string;
}

@ArgsType()
export class ProfileRequestsArgs extends Paginated {
    @Field(() => RequestSource)
    source: RequestSource; 

    @Field(() => RequestStatus, { nullable: true })
    status: RequestStatus;
}

@ArgsType()
export class ProfileDebatesArgs extends Paginated {
    @Field(() => DebateStatus)
    status: DebateStatus;
}


@ObjectType()
export class ProfileSubscriptionPagination {
    @Field(() => [SubscriptionType])
    page: SubscriptionTarget[];

    @Field({ nullable: true})
    next?: string;

    @Field({ defaultValue: false })
    hasMore: boolean;
}

@ObjectType()
export class ProfileFeedPagination {
    @Field(() => [Discussion])
    page: Discussion[];

    @Field({ nullable: true})
    next?: string;

    @Field({ defaultValue: false })
    hasMore: boolean;
}

@ObjectType()
export class ProfileActivityPagination extends PaginatedResponse(Event) {}