import { ArgsType, Field, ID, InputType, registerEnumType } from "type-graphql";
import { PageArgs } from "./pagination.types";

@ArgsType()
export class UserArgs {
  @Field(() => ID, { nullable: true })
  id: string;
  
  @Field({ nullable: true })
  username: string;
}

@ArgsType()
export class UserFollowersArgs extends PageArgs {
  @Field()
  username: string;
}

@InputType()
export class UserFilters {
  @Field({ nullable: true }) occupation: string;
  @Field({ nullable: true }) education: string;
  @Field({ nullable: true }) country: string;
  @Field({ nullable: true }) category: string;
  @Field({ nullable: true }) religion: string;
  @Field({ nullable: true }) politics: string;
}

@ArgsType()
export class UserSearchArgs extends PageArgs {
  @Field(() => UserFilters)
  filters: UserFilters;
}


export enum UserRelation {
  /**
   * Users have not relation
   */
  none,

  /**
   * User is a follower of the current user
   */
  follower,

  /**
   * User is subscribed by the current user
   */
  subscription,

  /**
   * User is follower of and subscribed by the current user
   */
  friend,

  /**
   * User is the current user
   */
  self
}

registerEnumType(UserRelation, { name: 'UserRelation' });
