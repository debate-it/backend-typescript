import { ArgsType, ClassType, Field, ID, InputType, Int, ObjectType } from "type-graphql";

import { Comment, Challenge, Debate, DiscussionCategory, Post, Request, User, Discussion, OpenRequest, RequestCandidacy } from "@/models";
import { Report } from "@/models/report/report";

export function PaginatedResponse<TItem>(TItemClass: ClassType<TItem>) {
  @ObjectType({ isAbstract: true })
  abstract class PaginatedResponseClass {

    @Field(() => [TItemClass])
    page: TItem[];

    @Field({ nullable: true})
    next?: string;

    @Field({ defaultValue: false })
    hasMore: boolean;
  }

  return PaginatedResponseClass;
}

@InputType()
export class PaginationInfo {
  @Field({ nullable: true })
  cursor: string; 

  @Field(() => Int, { defaultValue: 20 })
  limit: number = 20;
}

@ArgsType()
export abstract class Paginated {
  @Field(() => PaginationInfo)
  page: PaginationInfo;
}

@ArgsType()
export class IdWithPaginationArgs extends Paginated {
  @Field(() => ID)
  id: string;
}

@ArgsType()
export class PageArgs extends Paginated {}

@ObjectType()
export class UserPagination extends PaginatedResponse(User) {}

@ObjectType() 
export class DiscussionPagination extends PaginatedResponse(Discussion) {}

@ObjectType()
export class PostPagination extends PaginatedResponse(Post) {}

@ObjectType()
export class DebatePagination extends PaginatedResponse(Debate) {}

@ObjectType()
export class RequestPagination extends PaginatedResponse(Request) {}

@ObjectType()
export class OpenRequestPagination extends PaginatedResponse(OpenRequest) {}

@ObjectType()
export class RequestCandidacyPagination extends PaginatedResponse(RequestCandidacy) {}

@ObjectType()
export class CommentPagination extends PaginatedResponse(Comment) {}

@ObjectType()
export class ChallengePagination extends PaginatedResponse(Challenge) {}

@ObjectType()
export class CategoryPagination extends PaginatedResponse(DiscussionCategory) {}

@ObjectType()
export class ReportPagination extends PaginatedResponse(Report) {}
