import { createUnionType, Field, Int, ObjectType } from "type-graphql";
 
import { Post } from "@/models/post";
import { User } from "@/models/user";
import { Debate } from "@/models/debate";
import { Challenge } from "@/models/challenge";
import { Discussion } from "@/models/dicsussion";

import { LeaderboardEntry } from '@/services/leaderboard.service'
import { isChallenge, isDebate, isPost } from "./feed.types";

export const LeaderboardEntryUnion = createUnionType({
  name: 'LeaderboardEntry',
  types: () => [User, Debate, Post, Challenge] as const,
  resolveType: (value) => {
    if (isDebate(value as Discussion)) {
      return 'Debate';
    }

    if (isPost(value as Discussion)) {
      return 'Post';
    }
    
    if (isChallenge(value as Discussion)) {
      return 'Challenge';
    }

    const user = value as User; 

    if (user.username && user.email && user.info && user.stat) {
      return 'User';
    }

    throw new Error('Invalid leaderboard object');
  }
})

@ObjectType()
export class LeaderboardItem {
  @Field(() => Int)
  score: number;

  @Field(() => LeaderboardEntryUnion)
  entry: LeaderboardEntry;
}

@ObjectType()
export class LeaderboardPagination { 
  @Field(() => [LeaderboardItem])
  page: LeaderboardItem[];

  @Field({ nullable: true})
  next?: string;

  @Field({ defaultValue: false })
  hasMore: boolean;
}

@ObjectType()
export class Leaderboard {
  @Field(() => LeaderboardPagination)
  debates: LeaderboardPagination;

  @Field(() => LeaderboardPagination)
  challenges: LeaderboardPagination;
  
  @Field(() => LeaderboardPagination)
  posts: LeaderboardPagination;

  @Field(() => LeaderboardPagination)
  users: LeaderboardPagination;
}
