import { ArgsType, Field, ID, registerEnumType } from 'type-graphql';
import { IsMongoId, NotEquals } from 'class-validator';

import { OpenRequestType, RequestStatus } from '@/models/request';

import { DiscussionInput } from './discussion.types';
import { IdArgs } from './shared.types';

export enum RequestSource {
  any = 'any',
  incoming = 'incoming',
  outgoing = 'outgoing'
}

@ArgsType()
export class CreateRequestArgs {
  @IsMongoId({ message: 'Invalid id' })
  @Field(() => ID)
  recipient: string;

  @Field(() => DiscussionInput)
  discussion: DiscussionInput;
}

@ArgsType()
export class CreateOpenRequestArgs {
  @Field(() => DiscussionInput)
  discussion: DiscussionInput;

  @Field(() => OpenRequestType)
  type: OpenRequestType;
}

@ArgsType()
export class CreateDebateCandidacy extends IdArgs { }

@ArgsType()
export class CreatePanelCandidacy extends IdArgs {
  @Field()
  argument: string;
}

@ArgsType()
export class ResolveOpenRequestArgs extends IdArgs {
  @Field(() => [ID])
  candidates: string[];
}

@ArgsType()
export class ResolveOpenPanelRequestArgs extends ResolveOpenRequestArgs {
  @Field()
  argument: string;
}

@ArgsType()
export class CreateDebateRequestArgs extends CreateRequestArgs { }

@ArgsType()
export class CreatePanelRequestArgs implements Omit<CreateRequestArgs, 'recipient'> {
  @IsMongoId({ each: true, message: 'Invalid id'})
  @Field(() => [ID])
  recipients: string[];

  @Field()
  position: string;

  @Field(() => DiscussionInput)
  discussion: DiscussionInput;
}

@ArgsType()
export class AddPanelParticipantArgs extends IdArgs {
  @IsMongoId({ message: 'Invalid id' })
  @Field(() => ID)
  participant: string;
}

@ArgsType()
export class RequestResolveArgs extends IdArgs {
  @NotEquals(RequestStatus.submitted)
  @Field(() => RequestStatus)
  public status: RequestStatus;
}

registerEnumType(RequestSource, { name: 'RequestSource' });