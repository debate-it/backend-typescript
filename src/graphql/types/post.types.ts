import { ArgsType, Field } from 'type-graphql';

import { PostAlignmentSide } from '@/models/post';

import { DiscussionInput } from './discussion.types';
import { IdArgs } from './shared.types';


@ArgsType()
export class CreatePostArgs {
  @Field(() => DiscussionInput)
  public discussion: DiscussionInput;
}

@ArgsType()
export class AlignWithPostArgs extends IdArgs {
  @Field(() => PostAlignmentSide)
  public alignment: PostAlignmentSide;
}


