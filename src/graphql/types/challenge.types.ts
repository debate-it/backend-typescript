import { ArgsType, Field, ID, Int } from "type-graphql";
import { Max, Min } from "class-validator";


import { DiscussionInput } from "./discussion.types";

@ArgsType()
export class CreateChallengeArgs {
  @Field(() => DiscussionInput)
  discussion: DiscussionInput;

  @Field(() => Int)
  @Min(8)
  @Max(48)
  hours: number;

  @Field()
  position: string;
}

@ArgsType()
export class ChooseChallengeOpponentArgs {
  @Field(() => ID)
  id: string;

  @Field(() => ID)
  opponent: string;
}

@ArgsType()
export class AddChallengeResponseArgs {
  @Field(() => ID)
  id: string;

  @Field()
  response: string;
}