import { IsMongoId, IsNotEmpty } from "class-validator";
import { ArgsType, Field, ID, InputType, ObjectType } from "type-graphql";

import { ReportReason } from "@/models/report/report.log";
import { ReportResolutionDecision } from "@/models/report/report";
import { IdArgs } from "./shared.types";

@ArgsType()
export class ReportSubmissionInput {
  @Field(() => ID)
  @IsMongoId()
  discussionId: string;

  @Field(() => [ReportReason])
  reasons: ReportReason[];

  @Field()
  @IsNotEmpty()
  description: string;

  @Field(() => [ID])
  offenders: string[];
}

@ObjectType()
export class ReportOverview {
  @Field()
  pending: number;

  @Field()
  resolved: number;
}

@InputType()
export class ReportResolutionInput {
  @Field(() => ID)
  user: string;

  @Field(() => ReportResolutionDecision)
  decision: ReportResolutionDecision;

  @Field({ nullable: true })
  comment: string;
}

@ArgsType()
export class ReportResolutionArgs extends IdArgs {
  @Field(() => [ReportResolutionInput])
  decisions: ReportResolutionInput[];
}
