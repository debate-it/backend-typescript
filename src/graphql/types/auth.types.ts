import { ArgsType, Field, InputType, ObjectType, registerEnumType } from 'type-graphql';
import { User, UserInfo } from '@/models/user';

@InputType()
class ExtraInfo implements Omit<Partial<UserInfo>, 'level'> {
  @Field({ nullable: true })
  public fullname: string;

  @Field({ nullable: true })
  public about?: string;

  @Field({ nullable: true}) 
  public avatar?: string;

  @Field({ nullable: true}) 
  public education?: string;
  
  @Field({ nullable: true}) 
  public occupation?: string;

  @Field({ nullable: true}) 
  public country?: string;

  @Field({ nullable: true}) 
  public politics?: string;

  @Field({ nullable: true}) 
  public religion?: string;
}

@ArgsType()
export class SignUpInfo {
  @Field()
  public email: string;

  @Field()
  public username: string;

  @Field()
  public password: string;

  @Field({ nullable: true })
  public extra: ExtraInfo;
}

@ArgsType()
export class LoginInfo {
  @Field()
  public username: string;

  @Field()
  public password: string;
}

@ArgsType()
export class PasswordResetRequestInfo {
  @Field()
  public email: string;
}

@ArgsType()
export class PasswordResetInfo {
  @Field()
  public id: string;

  @Field()
  public password: string;
}

export enum AuthStatus {
  success = 'success',
  error = 'error'
}

@ObjectType()
export class AuthTokens {
  @Field()
  public session: string;

  @Field()
  public refresh: string;
}

@ObjectType()
export class AuthResponse {
  @Field()
  public active: boolean;

  @Field(() => AuthTokens)
  public tokens?: AuthTokens;

  @Field(() => User)
  public user?: User;
}


registerEnumType(AuthStatus, { name: 'AuthStatus' });
