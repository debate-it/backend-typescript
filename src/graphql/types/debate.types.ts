import { DebateArgument } from '@/models';
import { IsMongoId } from 'class-validator';
import { ArgsType, Field, ID, Int, ObjectType } from 'type-graphql';
import { IdArgs } from './shared.types';

@ArgsType()
export class AddArgumentArgs extends IdArgs {
  @Field()
  argument: string;
}

@ArgsType()
export class DebateVoteArgs extends IdArgs {
  @Field(() => Int)
  participant: number;
}

@ArgsType()
export class DebateRealtimeArgs extends IdArgs {
  @Field(() => Int)
  participant: number;
}

@ObjectType()
export class DebateRealtimeArgumentResponse {
  @Field(() => Int)
  index: number;

  debate: string;

  @Field(() => DebateArgument)
  argument: DebateArgument;
}
