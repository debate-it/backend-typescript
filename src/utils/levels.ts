export const levels = [
  { level: 1,   start: 0,     end: 50   },
  { level: 2,   start: 50,    end: 125  },
  { level: 3,   start: 125,   end: 200  },
  { level: 4,   start: 200,   end: 300  },
  { level: 5,   start: 300,   end: 450  },
  { level: 6,   start: 450,   end: 600  },
  { level: 7,   start: 600,   end: 800  },
  { level: 8,   start: 800,   end: 1000 },
  { level: 9,   start: 1000,  end: 1300 },
  { level: 10,  start: 1300,  end: 1600 },
  { level: 11,  start: 1600,  end: 2000 },
  { level: 12,  start: 2000,  end: 2400 },
  { level: 13,  start: 2400,  end: 2900 },
  { level: 14,  start: 2900,  end: 3400 },
  { level: 15,  start: 3400,  end: 4000 },
  { level: 16,  start: 4000,  end: 4600 },
  { level: 17,  start: 4600,  end: 5300 },
  { level: 18,  start: 5300,  end: 6000 },
  { level: 19,  start: 6000,  end: 7500 },
  { level: 20,  start: 7500,  end: null }
];
