import { EnvelopError } from "@envelop/core";
import { DiscussionSize } from "@/models/dicsussion";

export namespace WordLimit {
  export function validateArgument(argument: string, size: DiscussionSize) {
    validate(getWordCount(argument), getWordLimit(size));
  }
  
  export function validateComment(comment: string) {
    validate(getWordCount(comment), 250);
  }

  export function validateTopic(topic: string) {
    validate(getWordCount(topic), 100);
  }
  
  export function validatePostTopic(topic: string) {
    validate(getWordCount(topic), 250);
  }

  function validate(count: number, limit: number) {
    if (count > limit) { 
      throw new EnvelopError(`Word limit is surpassed (${count}/${limit})`, { code: 'WORD_LIMIT_SURPASSED' });
    }
  }


  
  export function getWordCount(argument: string): number {
    return argument.split(RegExp('[\t_\n ]'))
      .filter(word => word !== null && word.length > 0)
      .length;
  }

  export function getWordLimit(size: DiscussionSize) {
    switch (size) {
      case DiscussionSize.short:
        return 150;
      case DiscussionSize.medium:
        return 200;
      case DiscussionSize.long:
        return 250;
    }
  }
}