import { DiscussionCategory, DiscussionSize } from "@/models";

export interface DiscussionIndex {
  topic: string;
  category: string;
  created: Date;
  size: DiscussionSize;
  tags: string[];
}


export const DiscussionSchema = {
  mappings: {
    properties: {
      topic: { type: 'text' },
      category: { type: 'text' },
      created: { type: 'date' },
      size: { type: 'keyword' },
      tags: { type: 'keyword'}
    }
  }
};

export const UserSchema = {
  mappings: {
    properties: {
      username: { type: 'wildcard' },
      fullname: { type: 'text' },

      blocked: { type: 'boolean' },

      info: {
        properties: {
          about: { type: 'text'},

          level: { type: 'keyword'},
          country: { type: 'keyword' },
          
          education: { type: 'text' },
          occupation: { type: 'text' },

          religion: { type: 'text' },
          politics: { type: 'text' }
        }
      },

      stats: {
        properties: {
          points: { type: 'integer' },
          wins: { type: 'integer' },
          followers: { type: 'integer' },
          bestCategory: { type: 'text' }
        }
      }
    }
  }
};
