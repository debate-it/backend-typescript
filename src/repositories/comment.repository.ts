import { Inject, Service } from "typedi";
import { EnvelopError } from "@envelop/core";

import { ChallengeRepository } from "./challenge.repository";
import { DebateRepository } from "./debate.repository";
import { PanelRepository } from "./panel.repository";
import { EventRepository } from "./event.repository";
import { PostRepository } from "./post.repository";
import { VoteRepository } from "./vote.repository";

import { CommentOrder, CommentSearchArgs, CreateCommentArgs } from "@/graphql/types/comment.types";

import { PostVoteModel, VoteTargetModel, DiscussionVoteModel } from "@/models/vote";
import { Comment, CommentModel, CommentTargetType } from "@/models/comment";
import { Debate, DebateParticipant } from "@/models/debate";
import { Post, PostAlignmentSide } from "@/models/post";
import { EventTrigger, EventType } from "@/models/event";
import { Panel, PanelParticipant } from "@/models/panel";
import { Discussion } from '@/models/dicsussion';
import { Challenge } from "@/models/challenge";
import { User } from "@/models/user";

import { WordLimit } from "@/utils/word.limit";
import { PaginatedRepository } from "./paginated.repository";


interface CreateCommentOptions {
  skipEvent: boolean
}

@Service()
export class CommentRepository extends PaginatedRepository<Comment, typeof Comment> {
  protected readonly model = CommentModel;

  @Inject(() => ChallengeRepository)
  private readonly challenges: ChallengeRepository;

  @Inject() private readonly debates: DebateRepository;
  @Inject() private readonly panels: PanelRepository;
  @Inject() private readonly posts: PostRepository;

  @Inject() private readonly events: EventRepository;
  @Inject() private readonly votes: VoteRepository;

  public async findFor(user: User, args: CommentSearchArgs) {
    return this.paginate(
      { target: args.target, targetModel: args.type, author: { $ne: user } },
      { ...args.page, ...this.getSortOrder(args.order) }
    );
  }

  private getSortOrder(order: CommentOrder): { sortBy: string, sortDir: 'asc' | 'dsc' } {
    switch (order) {
      case CommentOrder.new:
        return { sortBy: 'created', sortDir: 'dsc' };

      case CommentOrder.old:
        return { sortBy: 'created', sortDir: 'asc' };

      case CommentOrder.top:
        return { sortBy: 'votes', sortDir: 'dsc' };
    }
  }

  public async initiate(author: User, args: CreateCommentArgs, options?: CreateCommentOptions): Promise<Comment> {
    WordLimit.validateComment(args.content);
    
    const target = await this.getTarget(args.target, args.type);

    await this.validate(author, target, args.type);

    const alignment = await this.getAlignment(author, target, args.type);
    const comment = await this.model.create({
      author,
      alignment,
      target,
      targetModel: args.type,
      content: args.content,
    });

    if (!options?.skipEvent) {
      const eventPayload = {
        source: author,
        type: EventType.CommentCreated,
        trigger: EventTrigger.Comment,
        entity: comment
      };

      await Promise.all(
        this.getEventTarget(target, args.type)
          .map(target => this.events.add({ target, ...eventPayload }))
      );
    }

    return comment;
  }

  public async vote(id: string, user: User) {
    const comment = await this.get(id);

    if (!comment) {
      throw new EnvelopError('Comment not found', { code: 'NOT_FOUND' });
    }

    if ((comment.author as User).id === user.id) {
      throw new EnvelopError('Author cannot vote on their own comment', { code: 'CANNOT_VOTE_SELF' });
    }

    return this.votes.comment(comment, user);
  }

  private getEventTarget(target: Discussion, type: CommentTargetType): User[] {
    switch (type) {
      case CommentTargetType.Challenge:
        return [(target as Challenge).author as User];
      case CommentTargetType.Panel:
      case CommentTargetType.Debate:
        return (target as Panel | Debate).participants
          .map((p: DebateParticipant | PanelParticipant) => p.user as User);
      case CommentTargetType.Post:
        return [(target as Post).author as User];
    }
  }

  private async getTarget(id: string, type: CommentTargetType): Promise<Discussion> {
    switch (type) {
      case CommentTargetType.Challenge:
        return (await this.challenges.get(id)) as Challenge;
      case CommentTargetType.Debate:
        return (await this.debates.get(id)) as Debate;
      case CommentTargetType.Panel:
        return (await this.panels.get(id)) as Panel;
      case CommentTargetType.Post:
        return (await this.posts.get(id)) as Post;
    }
  }

  private async getAlignment(user: User, target: Discussion, type: CommentTargetType) {
    switch (type) {
      case CommentTargetType.Challenge:
        return null;

      case CommentTargetType.Debate:
      case CommentTargetType.Panel:
        const discussionVote = await DiscussionVoteModel.findOne({
          targetModel: VoteTargetModel.Debate,
          target: target as Debate,
          voter: user
        });

        if (!discussionVote) {
          return null;
        }

        return discussionVote.participant;
        
      case CommentTargetType.Post:
        const postVote = await PostVoteModel.findOne({
          targetModel: VoteTargetModel.Post,
          target: target as Post,
          voter: user
        });

        if (!postVote) {
          return null;
        }

        switch (postVote.alignment) {
          case PostAlignmentSide.agree:
            return 0;

          case PostAlignmentSide.disagree:
            return 1;
        }
    }
  }

  private async validate(user: User, target: Discussion, type: CommentTargetType) {
    const record = await this.one({ author: user, target, targetModel: type });

    if (record) {
      throw new EnvelopError('Cannot comment twice', { code: 'DUPLICATE_COMMENT' });
    }

    switch (type) {
      case CommentTargetType.Debate:
        const debate = target as Debate;

        const isPartitipant = debate.participants
          .map(p => (p.user as User).id)
          .includes(user.id);

        if (isPartitipant) {
          throw new EnvelopError('Participants cannot comment on their own debate', { code: 'CANNOT_COMMENT' });
        }

        break;

      case CommentTargetType.Challenge:
        const challenge = target as Challenge;

        if ((challenge.author as User).id === user.id) {
          throw new EnvelopError('Challenge creator cannot respond to their own challenge', { code: 'CANNOT_COMMENT' });
        }

        if (challenge.deadline.valueOf() < Date.now()) {
          throw new EnvelopError('Cannot respond to finished challenge', { code: 'CANNOT_COMMENT' });
        }

        break;

      case CommentTargetType.Panel:
        const panel = target as Panel;

        const participant = panel.participants
          .find(p => (p.user as User).id === user.id);

        if (participant) {
          throw new EnvelopError('Panel participants cannot comment on their panel', { code: 'CANNOT_COMMENT' });
        }

        break;

      case CommentTargetType.Post:
        const post = target as Post;

        if ((post.author as User).id === user.id) {
          throw new EnvelopError('Post author cannot comment on their own post', { code: 'CANNOT_COMMENT' });
        }

        break;

    }
  }
}