import { Inject, Service } from 'typedi';
import { EnvelopError } from '@envelop/core';

import { Repository } from './base.repository';
import { PaginatedRepository } from './paginated.repository';

import { DiscussionInput } from '@/graphql/types/discussion.types';

import {
  Discussion,
  DiscussionTag,
  DiscussionModel,
  TagModel,
  DiscussionType,
} from '@/models/dicsussion';

import { WordLimit } from '@/utils/word.limit';
import { CategoryRepository } from './category.repository';


@Service()
export class TagRepository extends Repository<DiscussionTag, typeof DiscussionTag> {
  protected readonly model = TagModel;

  public async findOrCreate(tags: string[]) {
    const foundTags = await TagModel.find({ name: { $in: tags } });
    const foundTagsNames = new Set(foundTags.map(tag => tag.name));

    const createdTags = await Promise.all(
      tags
        .filter(tag => !foundTagsNames.has(tag))
        .map(tag => this.model.create({ name: tag }))
    );

    return foundTags.concat(createdTags);
  }
}
@Service()
export class DiscussionRepository extends PaginatedRepository<Discussion, typeof Discussion> {
  protected readonly model = DiscussionModel;

  @Inject() private readonly categories: CategoryRepository;
  @Inject() private readonly tags: TagRepository;


  public async start(args: DiscussionInput, type?: DiscussionType) {
    // Validate topic as long argument, if current discussion is a Post
    if (type && type === DiscussionType.Post) {
      WordLimit.validatePostTopic(args.topic);
    } else {
      WordLimit.validateTopic(args.topic);
    }

    const [category, tags] = await Promise.all([
      this.categories.findByName(args.category),
      this.tags.findOrCreate(args.tags)
    ]);

    if (!category) {
      throw new EnvelopError('Category not found', { code: 'NOT_FOUND' });
    }

    return {
      topic: args.topic,
      size: args.size,
      tags, category
    };
  }

  public async updateDiscussionStat(id: string) {
    return await this.categories.updateDiscussionStat(id, 1);
  }
}
