import { Types } from 'mongoose';
import { Inject, Service } from 'typedi';
import { DocumentType, isDocument } from '@typegoose/typegoose';
import { EnvelopError } from '@envelop/core';

import { config } from '@/config';

import { CreateDebateRequestArgs, CreateOpenRequestArgs, CreatePanelRequestArgs } from '@/graphql/types';

import { ScheduleService } from '@/services/schedule.service';

import { WordLimit } from '@/utils/word.limit';

import { User } from '@/models/user';
import { Debate } from '@/models/debate';
import { SchedulableType } from '@/models/schedule';
import { PanelParticipant, PANEL_MAX_PARTICIPANTS } from '@/models/panel';
import { EventTrigger, EventType } from '@/models/event';

import {
  Request,
  RequestStatus,
  RequestModel,

  OpenRequest,
  PanelRequest,
  DebateRequest,

  OpenRequestModel,
  PanelRequestModel,
  DebateRequestModel,
} from '@/models/request';

import { UserRepository } from './user.repository';
import { EventRepository } from './event.repository';

import { PanelRepository } from './panel.repository';
import { DebateRepository } from './debate.repository';
import { DiscussionRepository } from './discussion.repository';
import { RequestCandidacyRepository } from './request.candidacy.repository';

import { PaginatedRepository } from './paginated.repository';


@Service()
export class RequestRepository extends PaginatedRepository<Request, typeof Request> {
  protected readonly model = RequestModel;

  @Inject() private readonly candidacies: RequestCandidacyRepository;
  @Inject() private readonly discussions: DiscussionRepository;
  @Inject() private readonly debates: DebateRepository;
  @Inject() private readonly panels: PanelRepository;

  @Inject() private readonly events: EventRepository;
  @Inject() private readonly users: UserRepository;

  @Inject(() => ScheduleService)
  private readonly schedule: ScheduleService;

  public async initiateDebate(initiator: User, args: CreateDebateRequestArgs) {
    const [recipient, discussion] = await Promise.all([
      this.users.get(args.recipient),
      this.discussions.start(args.discussion)
    ]);

    if (!recipient) {
      throw new EnvelopError('Request recipient not found', { code: 'NOT_FOUND' });
    }

    const request = await DebateRequestModel.create({
      initiator,
      recipient,
      info: discussion,
    });

    await this.events.add({
      source: initiator,
      target: recipient,
      type: EventType.DebateRequestSent,
      trigger: EventTrigger.Request,
      entity: request
    });

    return request as DebateRequest;
  }

  public async initiatePanel(initiator: User, args: CreatePanelRequestArgs) {
    if (args.recipients.length > 9) {
      throw new EnvelopError('Can only add up to 9 participants for a panel', { code: 'INVALID_REQUEST' });
    }

    const [recipients, panel] = await Promise.all([
      this.users.find({ _id: { $in: args.recipients } }),
      this.panels.initiate(args.discussion, { user: initiator, argument: args.position })
    ]);

    const request = await PanelRequestModel.create({
      panel,
      initiator,
      recipients: recipients.map(user => ({ user })),
      info: {
        topic: panel.topic,
        category: panel.category,
        size: panel.size,
        tags: panel.tags
      }
    });

    const event = {
      source: initiator,
      type: EventType.PanelRequestSent,
      trigger: EventTrigger.Request,
      entity: request
    };

    await Promise.all(recipients.map(target => this.events.add({ target, ...event })));

    return request;
  }

  public async resolveDebateRequest(id: string, status: RequestStatus, user: User) {
    const request = await DebateRequestModel.findById(id);

    if (request.status !== RequestStatus.submitted) {
      throw new EnvelopError('Request already resolved', { code: 'REQUEST_ALREADY_RESOLVED'});
    }

    if (user.id !== (request.recipient as User).id) {
      throw new EnvelopError('Request can only be resolved by the recipient', { code: 'CANNOT_RESOLVE_REQUEST' });
    }

    request.status = status;

    await request.save();

    let debate: DocumentType<Debate> = null;

    let eventType: EventType;
    let eventTrigger: EventTrigger;

    switch (status) {
      case RequestStatus.accepted:
        debate = await this.debates.createFromRequest(request);

        eventType = EventType.DebateCreated;
        eventTrigger = EventTrigger.Debate;
        break;

      case RequestStatus.rejected:
        eventType = EventType.RequestRejected;
        eventTrigger = EventTrigger.Request;
        break;
    }

    await this.events.add({
      source: user,
      target: request.initiator as User,

      type: eventType,
      trigger: eventTrigger,

      entity: debate ? debate : request
    })

    return debate;
  }

  public async addPanelParticipant(id: string, user: User, participantId: string) {
    const request = await PanelRequestModel.findById(id);

    if (!request) {
      throw new EnvelopError('Panel request not found', { code: 'NOT_FOUND' });
    }

    if (user.id !== (request.initiator as User).id) {
      throw new EnvelopError('Only request initiator can add new participants', { code: 'NOT_INITATOR' });
    }

    const existing = request.recipients
      .find(recipient => (recipient.user as User).id === participantId);

    if (existing) {
      throw new EnvelopError('User has already received a request to this panel', { code: 'DUPLICATE' });
    }

    const participant = await this.users.get(participantId);
    if (!participant) {
      throw new EnvelopError('Participant not found', { code: 'NOT_FOUND' });
    }

    const accepted = request.recipients
      .filter(recipient => recipient.status === RequestStatus.accepted)
      .length;

    const waiting = request.recipients
      .filter(recipient => recipient.status === RequestStatus.submitted)
      .length;

    if (PANEL_MAX_PARTICIPANTS - (accepted + waiting) <= 0) {
      throw new EnvelopError('Panel is currently at max capacity', { code: 'CANNOT_ADD_MORE_PARTICIPANTS' });
    }

    request.recipients.push({ user: participant });

    await this.events.add({
      source: request.initiator as User,
      target: participant as User,
      type: EventType.PanelRequestSent,
      trigger: EventTrigger.Request,
      entity: request
    });

    return await request.save() as DocumentType<PanelRequest>;
  }

  public async resolvePanelRequest(id: string, status: RequestStatus, user: User, argument?: string) {
    const request = await PanelRequestModel.findById(id);

    if (Date.now() > request.expiration?.getTime()) {
      throw new EnvelopError('Panel request has expired', { code: 'EXPIRED' });
    }

    const recipient = request.recipients
      .find(r => (r.user as User).id === user.id);

    if (!recipient) {
      throw new EnvelopError('Request can only be resolved by the recipient', { code: 'NOT_RECIPIENT' });
    }

    if (recipient.status !== RequestStatus.submitted) {
      throw new EnvelopError('Request is already resolved', { code: 'REQUEST_ALREADY_RESOLVED' });
    }

    if (status === RequestStatus.submitted) {
      throw new Error('Invalid status');
    }

    recipient.status = status;

    if (status === RequestStatus.accepted) {
      if (!argument) {
        throw new EnvelopError('Argument is required for accepting the panel request', { code: 'ARUMENT_REQUIRED' });
      }

      WordLimit.validateArgument(argument, request.info.size);

      request.accepted += 1;

      await this.panels.addArgument(request.panel, user, argument);
    }


    await this.events.add({
      source: user,
      target: request.initiator as User,

      type: status === RequestStatus.accepted
        ? EventType.PanelRequestAccepted
        : EventType.RequestRejected,

      trigger: EventTrigger.Request,

      entity: request
    });


    if (request.accepted === 2) {
      request.expiration = new Date(Date.now() + config.constants.dayInSeconds * 3);
      this.schedule.create(
        { entity: request, type: SchedulableType.PanelRequest, updateAt: request.expiration },
        () => this.startPanelOnExpiration(request.panel as Types.ObjectId, request.id)
      );
    }

    if (request.accepted === request.recipients.length) {
      request.closed = true;
      await this.schedule.cancel({ entity: request.panel, type: SchedulableType.PanelRequest });
      await this.panels.start(request.panel as Types.ObjectId);
    }

    return await request.save() as DocumentType<PanelRequest>;
  }

  public async updatePanelExpiration(panelId: string, requestId: string) {
    const panel = await this.panels.get(panelId);

    if (!panel || !panel.deadline) {
      return;
    }

    if (panel.deadline && panel.deadline.getMilliseconds() < Date.now()) {
      return;
    }

    await this.startPanelOnExpiration(panel._id, requestId);
  }

  public async startPanelOnExpiration(panelId: Types.ObjectId, requestId: string) {
    await Promise.all([
      this.panels.start(panelId),
      this.schedule.done({ entity: panelId, type: SchedulableType.PanelRequest }),
      PanelRequestModel.findByIdAndUpdate(requestId, { $set: { closed: true } })
    ]);
  }

  /* ------------- Open Requests ------------- */
  
  public async findOpenRequests(user: User) {
    const sample = await this.aggregate<{ _id: Types.ObjectId }>()
      // Match unresolved open request for the last 10 days
      .match({ 
        kind: 'OpenRequest',
        resolved: false,
        initiator: { $ne: user._id },
        created: { $gte: new Date(Date.now() - 10 * 24 * 36e5) } 
      })
      // Leave only id of the request
      .project({ _id: 1 })
      // Look up user responses for requests
      .lookup({
        from: 'requestcandidacies',
        let: { target: "$_id" },
        as: 'responses',
        pipeline: [
          { $match: { candidate: user._id, $expr: { $eq: ["$request", "$$target"] } } },
          { $project: { _id: 1 } }
        ]
      })
      // Filter requests that don't have a 
      .match({ responses: { $size: 0 } })
      .sample(10)

    return await this.find({ _id: { $in: sample.map(s => s._id) } });
  }

  public async initiateOpenRequest(initiator: User, args: CreateOpenRequestArgs) {
    const info = await this.discussions.start(args.discussion);

    return OpenRequestModel.create({
      initiator, info,
      type: args.type,
    });
  }

  private async getOpenRequest(id: string, candidate: User) {
    const request = await OpenRequestModel.findById(id);

    if (!request) {
      throw new EnvelopError('Request not found', { code: 'NOT_FOUND' });
    }

    if (isDocument(request.initiator) && request.initiator.id === candidate.id) {
      throw new EnvelopError('Request creator cannot respond', { code: 'NOT_ALLOWED' });
    }

    return request;
  }

  public async submitDebateCandidacy(id: string, candidate: User) {
    const request = await this.getOpenRequest(id, candidate);
    const candidacy = await this.candidacies.submitDebateCandidacy(request, candidate);

    request.candidacies += 1;

    await Promise.all([
      request.save(),

      this.events.add({
        source: candidate,
        target: request.initiator as User,
        type: EventType.RequestCandidacySubmitted,
        trigger: EventTrigger.Request,
        entity: request,
      })
    ])

    return candidacy;
  }

  public async submitPanelCandidacy(id: string, candidate: User, argument: string) {
    const request = await this.getOpenRequest(id, candidate);
    const candidacy = await this.candidacies.submitPanelCandidacy(request, candidate, argument);

    request.candidacies += 1;

    await Promise.all([
      request.save(),

      this.events.add({
        source: candidate,
        target: request.initiator as User,
        type: EventType.RequestCandidacySubmitted,
        trigger: EventTrigger.Request,
        entity: request,
      })
    ])

    return candidacy;
  }

  public async resolveOpenDebateRequest(id: string, user: User, candidateIds: string[]) {
    const [request, candidates] = await Promise.all([
      OpenRequestModel.findById(id),
      this.candidacies.findDebateCandidacies(candidateIds)
    ]);

    if (!request) {
      throw new EnvelopError('Request not found', { code: 'NOT_FOUND' });
    }

    if (request.resolved) {
      throw new EnvelopError('Request already resolved', { code: 'ALREADY_RESOLVED' });
    }

    if ((request.initiator as User).id !== user.id) {
      throw new EnvelopError('Only request initiator can resolve a request', { code: 'NOT_INITIATOR' })
    }

    if (!candidates.reduce((acc: boolean, e) => acc && !!e, true)) {
      throw new EnvelopError('Invalid candidate id', { code: 'INVALID_CANDIDATE' });
    }

    if (candidates.reduce((acc: boolean, e) => acc && (e.request as OpenRequest).id === request.id, true)) {
      throw new EnvelopError('Not all candidacies are for this request', { code: 'INVALID_CANIDADATE' });
    }

    const debates = await Promise.all(
      candidates.map(candidate => this.debates.createFromOpenRequest(request, candidate))
    );

    request.resolved = true;

    await OpenRequestModel.updateOne({ _id: request.id }, { $set: { resolved: true } });

    return debates;
  }

  public async resolveOpenPanelRequest(id: string, user: User, candidateIds: string[], argument: string) {
    const [request, candidates] = await Promise.all([
      OpenRequestModel.findById(id),
      this.candidacies.findPanelCandidacies(candidateIds)
    ]);

    if (!request) {
      throw new EnvelopError('Request not found', { code: 'NOT_FOUND' });
    }

    if (request.resolved) {
      throw new EnvelopError('Request already resolved', { code: 'ALREADY_RESOLVED' });
    }

    if ((request.initiator as User).id !== user.id) {
      throw new EnvelopError('Only request initiator can resolve a request', { code: 'NOT_INITIATOR' })
    }

    if (!candidates.reduce((acc, e) => acc && !!e, true)) {
      throw new EnvelopError('Ivalid candidate id', { code: 'INVALID_CANIDATE' });
    }

    if (candidates.reduce((acc, e) => acc && (e.request as OpenRequest).id === request.id, true)) {
      throw new EnvelopError('Not all candidacies are for this request', { code: 'INVALID_CANDIDATE' });
    }

    const participants: PanelParticipant[] = [
      { user, argument },
      ...candidates
        .map(c => ({ user: c.candidate, argument: c.argument }))
    ];

    const panel = await this.panels.createFromOpenRequest(request, participants);

    await OpenRequestModel.updateOne({ _id: request.id }, { $set: { resolved: true } });

    return panel;
  }
}