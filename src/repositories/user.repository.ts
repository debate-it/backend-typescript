import { Inject, Service } from 'typedi';
import { PipelineStage } from 'mongoose';
import { FileUpload } from 'graphql-upload';
import { DocumentType } from '@typegoose/typegoose';

import { DebateModel, DebateStatus } from '@/models/debate';
import { User, UserInfo, UserModel } from '@/models/user';
import { DiscussionCategory, } from '@/models/dicsussion';
import { EventTrigger, EventType } from '@/models/event';
import { Subscription } from '@/models/subscription';
import { PanelStatus } from '@/models/panel';

import { CreateArgs } from './base.repository';
import { EventRepository } from './event.repository';
import { PaginatedRepository } from './paginated.repository';

import { CDNService } from '@/services/cdn.service';
import { SearchService } from '@/services/search.service';

import { UserInfoUpdateInput } from '@/graphql/types/profile.types';

import { levels } from '@/utils/levels';


export type UserInfoUpdate = Partial<Omit<UserInfo, 'level' | 'firstName' | 'lastName' | 'avatar'>>;

@Service()
export class UserRepository extends PaginatedRepository<User, typeof User> {
  protected readonly model = UserModel;

  @Inject() private readonly cdn: CDNService;
  @Inject() private readonly search: SearchService;
  @Inject() private readonly events: EventRepository;

  public async register(args: CreateArgs<User, typeof User>) {
    return await this.create(args);
  }

  public async updateInfo(user: DocumentType<User>, info: UserInfoUpdateInput) {
    if (info.fullname) {
      const [first, ...last] = info.fullname.split(' ');

      user.info.firstName = first;
      user.info.lastName = last.join(' ');
    }

    user.info.about = info.about;
    user.info.country = info.country;
    user.info.education = info.education;
    user.info.occupation = info.occupation;
    user.info.politics = info.politics;
    user.info.religion = info.religion;

    await this.search.addUsers([ user ]);

    return await user.save();
  }

  public async updateAfterFollowing(user: DocumentType<User>, subscription?: Subscription) {
    user.stat.followers += subscription && subscription.active ? -1 : 1;
    user.stat.points += subscription && subscription.active ? -1 : 1

    const currentLevel = user.progress.level;
    const updatedLevel = levels.find(l => l.start <= user.stat.points);

    user.progress.level = updatedLevel.level;
    user.progress.earned = user.stat.points - updatedLevel.start;
    user.progress.needed = updatedLevel.end - updatedLevel.start;

    await Promise.all([
      user.save(),
      this.search.addUsers([ user ]),
      
      updatedLevel.level > currentLevel &&
        this.events.add({
          entity: user,

          source: user,
          target: user,

          trigger: EventTrigger.User,
          type: EventType.UserLevelUp
        })
    ])
  }

  public async updateAfterWinning(user: DocumentType<User>, points?: number) {
    user.stat.wins += 1;
    user.stat.points += points ?? this.getReward(user);
    user.stat.bestCategory = await this.findBestCategory(user);

    const currentLevel = user.progress.level;
    const updatedLevel = levels.find(l => user.stat.points < l.end);

    user.progress.level = updatedLevel.level;
    user.progress.earned = user.stat.points - updatedLevel.start;
    user.progress.needed = updatedLevel.end - updatedLevel.start;

    await Promise.all([
      user.save(),
      this.search.addUsers([ user ]),

      updatedLevel.level > currentLevel &&
        this.events.add({
          entity: user,

          source: user,
          target: user,

          trigger: EventTrigger.User,
          type: EventType.UserLevelUp
        })
    ]);
  }

  private getReward(user: User) {
    return 25 * Math.pow(2, user.progress.level - 1)
  }

  public async findBestCategory(user: DocumentType<User>): Promise<DiscussionCategory> {
    const bestCategory = await DebateModel.aggregate()
      .match({
        $or: [
          { kind: 'Debate', status: DebateStatus.finished },
          { kind: 'Panel', status: PanelStatus.finished }
        ],
        'participants.user': user._id
      })
      .project({
        category: 1,
        participants: { user: 1, score: 1 },
        winningScore: { $max: '$participants.score' }
      })
      .addFields({
        isWinner: {
          $let: {
            vars: {
              winners: {
                $map: {
                  input: {
                    $filter: {
                      input: '$participants', as: 'participant',
                      cond: { $eq: ['$$participant.score', '$winningScore'] }
                    }
                  },
                  as: 'entry', in: '$$entry.user'
                }
              }
            },
            in: {
              $and: [
                { $ne: [{ $size: '$$winners' }, { $size: '$participants' }] },
                { $in: [user._id, '$$winners'] }
              ]
            }
          }
        }
      } as Record<string, any>)
      .match({ isWinner: true })
      .group({ _id: '$category', 'wins': { $sum: 1 } })
      .sort({ wins: 1 })
      .limit(1);
      
      // { $match: { 'isWinner': true } },
      // { $group: { _id: '$category', 'wins': { $sum: 1 } } } as PipelineStage.Group,
      // { $sort: { wins: -1 } },
      // { $limit: 1 },
      
      // [
      // {
      //   $match: {
      //     $or: [
      //       { kind: 'Debate', status: DebateStatus.finished },
      //       { kind: 'Panel', status: PanelStatus.finished }
      //     ],
      //     'participants.user': user._id
      //   }
      // },
      // {
      //   $project: {
      //     category: 1,
      //     participants: { user: 1, score: 1 },
      //     winningScore: { $max: '$participants.score' }
      //   }
      // },
      // {
      //   $set: {
      //     isWinner: {
      //       $let: {
      //         vars: {
      //           winners: {
      //             $map: {
      //               input: {
      //                 $filter: {
      //                   input: '$participants', as: 'participant',
      //                   cond: { $eq: ['$$participant.score', '$winningScore'] }
      //                 }
      //               },
      //               as: 'entry', in: '$$entry.user'
      //             }
      //           }
      //         },
      //         in: {
      //           $and: [
      //             { $ne: [{ $size: '$$winners' }, { $size: '$participants' }] },
      //             { $in: [user._id, '$$winners'] }
      //           ]
      //         }
      //       }
      //     }
      //   }
      // },
      // { $match: { 'isWinner': true } },
      // { $group: { _id: '$category', 'wins': { $sum: 1 } } } as PipelineStage.Group,
      // { $sort: { wins: -1 } },
      // { $limit: 1 },
    // ]);


    return bestCategory.pop();
  }

  async updateProfilePicture(user: DocumentType<User>, file: FileUpload) {
    const url = await this.cdn.upload(user, file);

    user.info.avatar = url;
    await user.save();

    return true;
  }
}