import { Inject, Service } from "typedi";
import { DocumentType } from "@typegoose/typegoose";
import { EnvelopError } from "@envelop/core";

import { FilterArgs, Repository } from "./base.repository";

import { Leaderboard, LeaderboardService } from "@/services/leaderboard.service";

import { Comment, EventTrigger, EventType, User } from "@/models";
import { Debate } from '@/models/debate'
import { Post, PostAlignmentSide } from '@/models/post';

import {
  Vote,
  VoteTarget,
  VoteTargetModel,
  VoteModel,
  PostVoteModel,
  DiscussionVoteModel,
  DiscussionVote,
  VoteType
} from "@/models/vote";

import { EventRepository } from "./event.repository";
import { Panel } from "@/models/panel";


export interface VoteInfo {
  voter: User;
  target: VoteTarget;
  targetModel: VoteTargetModel;

  participant?: number;
  alignment?: PostAlignmentSide
}

@Service()
export class VoteRepository extends Repository<Vote, typeof Vote> {
  protected readonly model = VoteModel;

  private readonly posts = PostVoteModel;
  private readonly discussions = DiscussionVoteModel;

  @Inject() private readonly events: EventRepository;
  @Inject() private readonly leaderboard: LeaderboardService;


  public async vote(info: VoteInfo) {
    const baseInfo = {
      voter: info.voter,
      target: info.target,
      targetModel: info.targetModel,
    };

    switch (info.targetModel) {
      case VoteTargetModel.Comment:
        return await this.model.create({ ...baseInfo, value: 1 });

      case VoteTargetModel.Debate:
        if (info.participant == null) {
          throw new EnvelopError('Debate side is not selected', { code: 'INVALID_VOTE' });
        }

        return await this.discussions.create({
          ...baseInfo, 
          value: info.voter.progress.level,
          participant: info.participant
        }); 

      case VoteTargetModel.Post:
        if (!info.alignment) {
          throw new EnvelopError('Post alignment is not selected', { code: 'INVALID_VOTE' });
        }

        return await this.posts.create({ ...baseInfo, value: 1, alignment: info.alignment });
    }
  }

  public findFor<T extends Vote>(type: VoteTargetModel, query: FilterArgs<typeof Vote>) {
    return this.find({ ...query, kind: this.getDiscriminator(type) })
      .map(doc => doc as DocumentType<T>[]);
  }

  public async oneFor<T extends Vote>(type: VoteTargetModel, query: FilterArgs<typeof Vote>) {
    return (await this.one({ ...query, kind: this.getDiscriminator(type) })) as DocumentType<T>;
  }

  private getDiscriminator(type: VoteTargetModel) {
    switch (type) {
      case VoteTargetModel.Comment:
        return null;

      case VoteTargetModel.Debate:
      case VoteTargetModel.Panel:
        return VoteType.Discussion;

      case VoteTargetModel.Post:
        return VoteType.Post;
    }
  }

  public async debate(debate: DocumentType<Debate>, voter: User, participant: number) {
    const target = debate.participants[participant];

    const vote = await this.oneFor<DiscussionVote>(VoteTargetModel.Debate, {
      voter,
      target: debate,
      targetModel: VoteTargetModel.Debate
    });

    // Vote exists
    if (vote) {
      // Remove points from participant
      debate.participants[vote.participant].score -= vote.value;

      this.leaderboard.add(Leaderboard.Users, debate.participants[vote.participant].user as User, -vote.value);

      // If the current vote has the same side, remove the vote;
      // Otherwise, change vote side and add points to other participant
      if (vote.active && vote.participant === participant) {
        await this.model.findByIdAndUpdate(vote.id, { $set: { active: false, value: 0 } });

        this.leaderboard.add(Leaderboard.Debates, debate, -vote.value);
      } else {
        vote.participant = participant;
        vote.value = voter.progress.level;
        vote.active = true;

        target.score += vote.value;

        await vote.save();

        this.leaderboard.add(Leaderboard.Users, target.user as User, voter.progress.level);
      }
    } else {
      // create new vote and add points
      target.score += voter.progress.level;

      await this.vote({
        voter, 
        participant,
        target: debate,
        targetModel: VoteTargetModel.Debate,
      });

      this.leaderboard.add(Leaderboard.Debates, debate, voter.progress.level);
      this.leaderboard.add(Leaderboard.Users, target.user as User, voter.progress.level);

      const event = {
        source: voter,
        type: EventType.Vote,
        trigger: EventTrigger.Debate,
        entity: debate
      };

      await Promise.all(
        debate.participants
          .map(p => this.events.add({ target: p.user as User, ...event }))
      );
    }


    return await debate.save() as DocumentType<Debate>;
  }

  public async comment(comment: DocumentType<Comment>, voter: User) {
    const voteInfo = { voter, target: comment, targetModel: VoteTargetModel.Comment };

    const vote = await this.model.findOne(voteInfo);

    comment.votes += vote && vote.active ? -1 : 1;

    if (vote && vote.active) {

      await this.model.findByIdAndUpdate(vote.id, { $set: { active: false, value: 0 } });

    } else if (vote && !vote.active) {

      vote.value = 1;
      vote.active = true;

      vote.save();

    } else {
      await Promise.all([
        this.model.create({ value: 1, ...voteInfo }),
        this.events.add({
          source: voter,
          target: comment.author as User,
          type: EventType.Vote,
          trigger: EventTrigger.Comment,
          entity: await comment
            .populate('target') as DocumentType<Comment>
        })
      ])

    }

    return await comment.save() as DocumentType<Comment>;
  }

  public async post(post: DocumentType<Post>, voter: User, alignment: PostAlignmentSide) {
    /* Post voting states:
     * 1. No vote exists - create vote record
     * 2. Vote exists, different alignment - move point, update post and vote
     * 3. Vote exists, same alignment - remove point, update post and vote
     * 4. Vote not active - add point to alignment, activate vote and update post
     */
    const vote = await this.posts.findOne({ 
      voter,
      target: post, 
      targetModel: VoteTargetModel.Post
    });

    // Vote record exists
    if (vote) {
      const alignmentMatches = vote.alignment === alignment;
      const isActive = vote.active;

      if (isActive) {

        post.alignment[vote.alignment] -= vote.value;

        vote.alignment = alignment;
        vote.active = !alignmentMatches;

        if (alignmentMatches) {
          this.leaderboard.add(Leaderboard.Posts, post, -vote.value);
          vote.value = 0;
        } else {
          post.alignment[alignment] += vote.value;
        }

      } else {
        vote.value = 1;
        vote.active = true;
        vote.alignment = alignment;

        post.alignment[vote.alignment] += vote.value;
        this.leaderboard.add(Leaderboard.Posts, post, vote.value);
      }

      await vote.save();
    } else {
      post.alignment[alignment] += 1;

      await this.posts.create({
        value: 1,
        voter,
        target: post,
        targetModel: VoteTargetModel.Post,
        alignment
      });

      this.leaderboard.add(Leaderboard.Posts, post, 1);

      await this.events.add({
        source: voter,
        target: post.author as User,
        type: EventType.Vote,
        trigger: EventTrigger.Post,
        entity: post
      });
    }

    return post.save();
  }

  public async panel(panel: DocumentType<Panel>, voter: User, participant: number) {
    const vote = await this.discussions.findOne({ targetModel: VoteTargetModel.Panel, target: panel, voter });
    const target = panel.participants[participant];

    // Vote exists
    if (vote) {
      // Remove points from participant
      panel.participants[vote.participant].score -= vote.value;

      this.leaderboard.add(Leaderboard.Users, panel.participants[vote.participant].user as User, -vote.value);

      // If the current vote has the same side, remove the vote;
      // Otherwise, change vote side and add points to other participant
      if (vote.active && vote.participant === participant) {
        await this.model.findByIdAndUpdate(vote.id, { $set: { active: false, value: 0 } });

        this.leaderboard.add(Leaderboard.Panels, panel, -vote.value);
      } else {
        vote.participant = participant;
        vote.value = voter.progress.level;
        vote.active = true;

        target.score += vote.value;

        await vote.save();

        this.leaderboard.add(Leaderboard.Users, target.user as User, voter.progress.level);
      }
    } else {
      // create new vote and add points
      target.score += voter.progress.level;

      const vote = await this.discussions.create({
        value: 1,
        voter,
        target: panel,
        targetModel: VoteTargetModel.Panel,
        participant,
      });

      this.leaderboard.add(Leaderboard.Panels, panel, voter.progress.level);
      this.leaderboard.add(Leaderboard.Users, target.user as User, voter.progress.level);

      const event = {
        source: voter,
        type: EventType.Vote,
        trigger: EventTrigger.Panel,
        entity: panel
      };

      await Promise.all(
        panel.participants
          .map(participant => participant.user)
          .map((target: User) => this.events.add({ target, ...event }))
      );
    }


    return await panel.save() as DocumentType<Panel>;
  }
}