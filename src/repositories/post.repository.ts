import { Inject, Service } from 'typedi';
import { DocumentType } from '@typegoose/typegoose';
import { EnvelopError } from '@envelop/core';

import { User } from '@/models/user';
import { Post, PostModel } from '@/models/post';
import { DiscussionCategory, DiscussionType } from '@/models/dicsussion';
import { PostVote, VoteTargetModel } from '@/models/vote';

import { AlignWithPostArgs, CreatePostArgs } from '@/graphql/types';

import { SearchService } from '@/services/search.service';

import { VoteRepository } from './vote.repository';
import { PaginatedRepository } from './paginated.repository';
import { DiscussionRepository } from './discussion.repository';


@Service()
export class PostRepository extends PaginatedRepository<Post, typeof Post> {
  protected readonly model = PostModel;

  @Inject() private readonly discussions: DiscussionRepository;
  @Inject() private readonly votes: VoteRepository;
  @Inject() private readonly search: SearchService;
  

  public async initiate(creator: User, args: CreatePostArgs) {
    const discussion = await this.discussions
      .start(args.discussion, DiscussionType.Post); 
    
    const post = await this.model.create({ ...discussion, author: creator });
    
    await Promise.all([
      this.search.addDiscussions([ post ]),
      this.discussions.updateDiscussionStat((post.category as DiscussionCategory).id),
    ]);

    return post;
  }

  public async align(user: User, args: AlignWithPostArgs) {
    const post = await this.get(args.id);

    if (!post) {
      throw new EnvelopError('Post not found', { code: 'NOT_FOUND' });
    }

    if (user.id === (post.author as User).id) {
      throw new EnvelopError('Author cannot align with their post', { code: 'CANNOT_VOTE' });
    }

    return this.votes.post(post, user, args.alignment);
  }

  public async findUserVote(post: Post, user: User) {
    const vote = await this.votes.one({
      voter: user.id,
      target: post.id,
      active: true,
      targetModel: VoteTargetModel.Post
    }) as DocumentType<PostVote>;

    return vote?.alignment;
  }
}