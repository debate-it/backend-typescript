import { Aggregate, FilterQuery, QueryOptions, UpdateQuery  } from 'mongoose';
import { ReturnModelType } from '@typegoose/typegoose';
import { AnyParamConstructor, BeAnObject, DocumentType } from '@typegoose/typegoose/lib/types';

export type UpdateArgs<T extends AnyParamConstructor<any>> = UpdateQuery<DocumentType<InstanceType<T>>>;
export type FilterArgs<T extends AnyParamConstructor<any>> = FilterQuery<DocumentType<InstanceType<T>>>;
export type CreateArgs<T, U extends AnyParamConstructor<any>> = (T | DocumentType<InstanceType<U>>)

export abstract class Repository<T, U extends AnyParamConstructor<any>> {
  protected abstract readonly model: ReturnModelType<U, BeAnObject>;

  public aggregate<TAgg = U>(): Aggregate<TAgg[]> {
    return this.model.aggregate<TAgg>();
  }

  public count(query: FilterArgs<U>) {
    return this.model.countDocuments(query);
  }

  public find(query: FilterArgs<U>, projection?: any, options?: QueryOptions) {
    return this.model.find(query, projection, options);
  }

  public one(query: FilterArgs<U>, projection?: any, options?: QueryOptions) {
    return this.model.findOne(query, projection, options);
  }

  public get(id: string, projection?: any, options?: QueryOptions) {
    return this.model.findById(id, projection, options);
  }

  // Weird way to solve the problem with arguments for create
  public async create(doc: CreateArgs<T, U>) {
    return this.model.create<T>(doc).then(r => r as DocumentType<InstanceType<U>>);
  }

  public async delete(id: string): Promise<void> {
    await this.model.findByIdAndDelete(id);
  }

  public async deleteMany(query: FilterArgs<U>): Promise<void> {
    await this.model.deleteMany(query);
  }

  public update(query: FilterArgs<U>, update: UpdateArgs<U>) {
    return this.model.updateMany(query, update);
  }
  
  public updateOne(query: FilterArgs<U>, update: UpdateArgs<U>) {
    this.model.updateOne()
    return this.model.updateOne(query, update);
  }

  public updateById(id: string, update: UpdateArgs<U>) {
    return this.model.findByIdAndUpdate(id, update);
  }

}
