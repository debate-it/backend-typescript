import { EJSON } from 'bson';
import * as base64 from 'base64-url';

import { DocumentDefinition, FilterQuery, HydratedDocument, PopulateOptions } from "mongoose";
import { AnyParamConstructor, DocumentType } from "@typegoose/typegoose/lib/types";

import { Repository } from "./base.repository"; 


export interface RepositoryPageOptions {
  /**
   * Pagiantion cursor for the last document of the previous page
   */
  cursor?: string;

  /**
   * Field that will be sorted by
   */
  sortBy?: string;

  /** 
   * Sort direction 
   */
  sortDir?: 'asc' | 'dsc'

  /**
   * Number of documents in a page
   */
  limit?: number;

  /**
   * Population options if any requried
   */
  populate?: PopulateOptions | PopulateOptions[];
}

export interface PaginatedResult<T> {
  page: T[];
  hasMore: boolean;
  next?: string;
}

export abstract class PaginatedRepository<T, U extends AnyParamConstructor<T>> extends Repository<T, U> {
  public async paginate(query: FilterQuery<DocumentType<T>>, page: RepositoryPageOptions) {

    const paginationConstraint = this.constructQuery(page);

    const paginationQuery: any = paginationConstraint
      ? { $and: [ query, paginationConstraint ] }
      : query;

    const sortDir = page.sortDir ?? 'dsc';
    const sortBy = page.sortBy ?? '_id';

    const recordQuery = this.model
      .find(paginationQuery)
      .sort({ [sortBy]: sortDir === 'dsc' ? -1 : 1 })
      .limit(1 + (page.limit ?? 10));

    if (page.populate) {
      recordQuery.populate(page.populate)
    }

    const records = await recordQuery.exec(); 

    return this.prepareResponse(records, page);
  }

  private prepareResponse(docs: HydratedDocument<DocumentType<InstanceType<U>>>[], options: RepositoryPageOptions) {
    // Check if there is a next page
    const hasMore = options.limit && docs.length > options.limit;

    if (hasMore) {
      docs.pop(); // Remove extra doc used to check for a next page
    }

    // Next/previous page data
    const next = hasMore 
      ? this.prepareCursor(docs[docs.length - 1], options) 
      : null;

    // Build result
    const result: PaginatedResult<T> = {
      page: docs as T[],
      hasMore,
      next,
    };

    return result;
  }

  private prepareCursor(doc: InstanceType<any>, options: RepositoryPageOptions): string {
    // Always save _id for secondary sorting.
    return options.sortBy && options.sortBy !== "_id" 
      ? this.encodeCursor([this.path(options.sortBy.split("."), doc), doc._id])
      : this.encodeCursor([doc._id]);
  }

  private path(fields: string[], obj: any) {
    return fields.reduce((acc, field) => acc?.[field] ?? acc, obj);
  }

  private constructQuery(options: RepositoryPageOptions) {
    const query: any = {};

    // Decode cursor string
    const decoded = this.decodeCursor(options.cursor);


    if (decoded.length == 1 && decoded[0] === null) {
      return null;
    }

    // Determine sort direction (reversed for previous page)
    const sortAscending = options.sortDir && options.sortDir === 'asc'; 
    const sortComparer = sortAscending ? "$gt" : "$lt";

    // Secondary sort on _id
    if (options.sortBy && options.sortBy !== "_id") {
      query.$or = [
        { [options.sortBy]: { [sortComparer]: decoded[0] } },
        { [options.sortBy]: decoded[0], _id: { [sortComparer]: decoded[1] } }
      ];
    } else {
      query._id = { [sortComparer]: decoded[0] };
    }
    return query;
  }

  private encodeCursor(obj: any) {
    return base64.encode(EJSON.stringify(obj));
  }

  private decodeCursor(cursor?: string) { 
    if (!cursor) {
      return [null];
    }

    return EJSON.parse(base64.decode(cursor)) as any[];
  }
}