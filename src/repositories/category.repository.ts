import { Service } from "typedi";
import { DocumentType } from '@typegoose/typegoose';

import { PaginatedRepository } from "./paginated.repository";

import { CategoryModel, DiscussionCategory } from "@/models/dicsussion";
import { Subscription } from "@/models/subscription";

@Service()
export class CategoryRepository extends PaginatedRepository<DiscussionCategory, typeof DiscussionCategory> {
  protected readonly model = CategoryModel;
  
  public findByName(name: string) {
    return this.model.findOne({ name });
  }

  public async updateDiscussionStat(id: string, update: number) {
    return await this.model.findByIdAndUpdate(id, { $inc: { [`stat.discussions`]: update } });
  }

  public async updateAfterFollowing(target: DocumentType<DiscussionCategory>, subscription?: Subscription) {
    target.stat.followers += subscription && subscription.active ? -1 : 1;

    await target.save();
  }
}