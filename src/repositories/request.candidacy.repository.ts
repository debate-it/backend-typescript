import { Service } from "typedi";
import { EnvelopError } from "@envelop/core";

import { PaginatedRepository } from "./paginated.repository";

import { User } from "@/models/user";

import {
  OpenRequest,
  RequestCandidacy,
  RequestCandidacyModel,
  PanelRequestCandidacyModel,
  DebateRequestCandidacyModel,
} from "@/models/request";

import { WordLimit } from "@/utils/word.limit";


@Service()
export class RequestCandidacyRepository extends PaginatedRepository<RequestCandidacy, typeof RequestCandidacy> {
  protected readonly model = RequestCandidacyModel;

  private readonly panelCandidacies = PanelRequestCandidacyModel;
  private readonly debateCandidacies = DebateRequestCandidacyModel;

  public async submitDebateCandidacy(request: OpenRequest, candidate: User) {
    const record = await this.debateCandidacies.findOne({ request, candidate });

    if (record) {
      throw new EnvelopError('Candidacy for this request already submitted', { code: 'DUPLICATE' });
    }

    return this.debateCandidacies.create({ request, candidate });
  }

  public async submitPanelCandidacy(request: OpenRequest, candidate: User, argument: string) {
    const record = await this.debateCandidacies.findOne({ request, candidate });

    if (record) {
      throw new EnvelopError('Candidacy for this request already submitted', { code: 'DUPLICATE' });
    }

    WordLimit.validateArgument(argument, request.info.size);

    return this.panelCandidacies.create({ request, candidate, argument });
  }
  
  public async findDebateCandidacies(ids: string[]) {
    return this.debateCandidacies.find({ _id: { $in: ids }});
  }

  public async findPanelCandidacies(ids: string[]) {
    return this.panelCandidacies.find({ _id: { $in: ids }});
  }
}