import { Inject, Service } from "typedi";
import { DocumentType } from "@typegoose/typegoose";

import {
  Challenge,
  Comment,
  Debate,
  Discussion,
  Post,
  Request, 
  User,
  Panel
} from "@/models";

import {
  Event,
  EventType,
  EventModel,
  EventRecord,
  EventTrigger,
  MAX_LENGTH_EXTRA,
  MAX_EVENT_RECORDS,
  getEventBucketEndDate,
  getEventBucketStartDate,
  EventExtra,
} from "@/models/event";

import { PaginatedRepository } from "./paginated.repository";
import { FirebaseService } from "@/services/firebase.service";

type EventTriggerEntity = Debate | Post | Panel | Challenge | Request | User | Comment;

interface EventPayload {
  source?: User;
  target: User;
  type: EventType;
  trigger: EventTrigger;
  entity: EventTriggerEntity;
}

@Service()
export class EventRepository extends PaginatedRepository<Event, typeof Event> {
  protected readonly model = EventModel;

  @Inject()
  private messaging: FirebaseService;

  public async add(payload: EventPayload): Promise<void> {
    const now = new Date();

    const event: EventRecord = new EventRecord();

    event.type = payload.type;
    event.link = this.createLink(payload.trigger, payload.entity);
    event.extra = this.getExtra(payload);

    const query = {
      target: payload.target,
      from: { $lte: now },
      to: { $gt: now }
    };

    const record = await this.model
      .findOne(query)
      .sort({ to: -1 })
      .limit(1);

    if (record && record.total < MAX_EVENT_RECORDS) {
      await this.updateById(record.id, { $inc: { total: 1, new: 1 }, $push: { events: event } })
    } else { 
      await this.createNewBucket(payload.target, event, record);
    }

    await this.messaging.send(payload.target, event);
  }

  private async createNewBucket(target: User, event: EventRecord, old?: DocumentType<Event>) {
    const from = old
      ? new Date()
      : getEventBucketStartDate();

    const to = old
      ? old.to
      : getEventBucketEndDate()

    if (old) {
      await this.updateById(old.id, { $set: { to: new Date() } });
    }

    return this.model.create({
      from, to,
      target: target,
      events: [event]
    });
  }

  private getExtra(payload: EventPayload): EventExtra {
    switch (payload.type) {
      case EventType.UserFollow:
        return { username: payload.source.username };

      case EventType.DebateRequestSent:
      case EventType.PanelRequestSent:
      case EventType.RequestRejected:
      case EventType.RequestCandidacySubmitted:
        return {
          username: payload.source.username,
          topic: this.trim((payload.entity as Request).info.topic)
        }

      case EventType.DebateCreated:
      case EventType.DebateFinished:
      case EventType.PanelStarted:
      case EventType.PanelFinished:
      case EventType.ChallengeFinished:
        return { topic: this.trim((payload.entity as Discussion).topic) };

      case EventType.PanelRequestAccepted:
        return {
          username: payload.source.username,
          topic: this.trim((payload.entity as Request).info.topic)
        };

      case EventType.Vote:
        const args = {
          username: payload.source.username,
          discussion: payload.trigger,
        };

        if (payload.trigger !== EventTrigger.Comment) {
          return { ...args, topic: this.trim((payload.entity as Discussion).topic), };
        } else {
          return { ...args, topic: this.trim((payload.entity as Comment).content) };
        }

      case EventType.CommentCreated:
        return {
          username: payload.source.username,
          topic: this.trim(((payload.entity as Comment).target as Discussion).topic),
          discussion: payload.trigger
        };

      case EventType.ChallengeResponse:
      case EventType.DebateArgumentAdded:
        return {
          username: payload.source.username,
          topic: this.trim((payload.entity as Discussion).topic)
        };
    }
  }

  private createLink(trigger: EventTrigger, entity: EventTriggerEntity) {
    return `${trigger}/${entity.id}`;
  }

  private trim(topic: string) {
    return topic.length > MAX_LENGTH_EXTRA
      ? topic.slice(0, MAX_LENGTH_EXTRA - 3) + '...'
      : topic;
  }
}