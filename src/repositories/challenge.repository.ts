import { Inject, Service } from "typedi";
import { DocumentType } from "@typegoose/typegoose";

import { User } from "@/models/user";
import { SchedulableType } from "@/models/schedule";
import { CommentTargetType } from "@/models/comment";
import { EventTrigger, EventType } from "@/models/event";
import { DiscussionCategory } from "@/models/dicsussion";
import { Challenge, ChallengeModel, ChallengeStatus } from "@/models/challenge";

import { CreateChallengeArgs } from "@/graphql/types/challenge.types";

import { EventRepository } from "./event.repository";
import { DebateRepository } from "./debate.repository";
import { CommentRepository } from "./comment.repository";
import { PaginatedRepository } from "./paginated.repository";
import { DiscussionRepository } from "./discussion.repository";

import { SearchService } from "@/services/search.service";
import { ScheduleService } from "@/services/schedule.service";
import { Leaderboard, LeaderboardService } from "@/services/leaderboard.service";
import { EnvelopError } from "@envelop/core";




@Service({ factory: () => new ChallengeRepository() })
export class ChallengeRepository extends PaginatedRepository<Challenge, typeof Challenge> {
  protected readonly model = ChallengeModel;

  @Inject() private readonly leaderboard: LeaderboardService;

  @Inject() private readonly discussions: DiscussionRepository;
  @Inject() private readonly debates: DebateRepository;
  @Inject() private readonly events: EventRepository;
  
  @Inject(() => ScheduleService) 
  private readonly scheduler: ScheduleService;

  @Inject(() => CommentRepository)
  private readonly comments: CommentRepository;

  @Inject()
  private readonly search: SearchService;

  public async initiate(author: User, args: CreateChallengeArgs): Promise<Challenge> {
    const discussion = await this.discussions.start(args.discussion);

    const challenge = await this.model.create({
      ...discussion,
      author: author,
      position: args.position,
      level: author.progress.level,
      deadline: this.getDeadline(args.hours),
    });

    await Promise.all([
      this.schedule(challenge),
      this.search.addDiscussions([ challenge ]),
      this.discussions.updateDiscussionStat((challenge.category as DiscussionCategory).id)
    ]);

    return challenge;
  }

  public async addReponse(id: string, user: User, response: string) {
    const challenge = await this.get(id);
    if (!challenge) {
      throw new EnvelopError('Chalenge not found', { code: 'NOT_FOUND' });
    }

    const payload = {
      target: id,
      content: response,
      type: CommentTargetType.Challenge
    };

    await this.comments.initiate(user, payload, { skipEvent: true });

    challenge.responses += 1;

    this.leaderboard.add(Leaderboard.Challenges, challenge, 1);

    await this.events.add({
      source: user,
      target: challenge.author as User,
      type: EventType.ChallengeResponse,
      trigger: EventTrigger.Challenge,
      entity: challenge
    });

    return await challenge.save() as DocumentType<Challenge>;
  }

  public async chooseOpponent(id: string, author: User, responseId: string) {
    const [challenge, response] = await Promise.all([
      this.get(id),
      this.comments.get(responseId)
    ]);

    if (!challenge) {
      throw new EnvelopError('Challenge not found', { code: 'NOT_FOUND', source: 'CHALLENGE' });
    }

    if (!response) {
      throw new EnvelopError('Response not found', { code: 'NOT_FOUND', source: 'RESPONSE' });
    }

    if (author.id !== (challenge.author as User).id) {
      throw new EnvelopError('Non-author cannot choose opponent for the challenge', { code: 'NOT_CHALLENGE_AUTHOR' });
    }

    if (challenge.status !== ChallengeStatus.waitingForOpponent) {
      throw new EnvelopError('Challenge is not in the stage of choosing the opponent', { code: 'CHALLENGE_NOT_FINISHED'});
    }

    if ((response.target as Challenge).id === (challenge.author as User).id) {
      throw new EnvelopError('Challenge author cannot choose themselves as opponent', { code: 'CANNOT_CHOOSE_SELF' });
    }

    challenge.status = ChallengeStatus.finished;
    await challenge.save();

    return this.debates.createFromChallenge(challenge, response);
  }

  private getDeadline(hours: number): Date {
    // return new Date(Date.now() + config.constants.dayInSeconds * hours);
    // TODO: Replace
    return new Date(Date.now() + 60 * 1e3 * hours);
  }

  private async schedule(challenge: Challenge) {
    const info = {
      entity: challenge,
      updateAt: challenge.deadline,
      type: SchedulableType.Challenge
    }

    await this.scheduler.create(info, () => this.updateChallenge(challenge.id));
  }

  public async updateChallenge(id: string) {
    const challenge = await this.get(id);

    const deadline = Math.floor(challenge.deadline.getTime() / 1000);
    const now = Math.floor(Date.now() / 1000);

    if (now >= deadline) {
      await this.finish(challenge);
    } else {
      await this.scheduler.reschedule({
        entity: challenge,
        updateAt: challenge.deadline,
        type: SchedulableType.Challenge,
      });
    }
  }

  private async finish(challenge: Challenge) {
    await Promise.all([
      this.model.findByIdAndUpdate(challenge.id, { status: ChallengeStatus.waitingForOpponent }),
      this.scheduler.done({ entity: challenge, type: SchedulableType.Challenge }),
      this.events.add({
        target: challenge.author as User,
        type: EventType.ChallengeFinished,
        trigger: EventTrigger.Challenge,
        entity: challenge
      })
    ]);
  }

}