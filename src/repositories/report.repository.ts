import { Types, Document } from "mongoose";
import { Inject, Service } from "typedi";
import { BeAnObject } from "@typegoose/typegoose/lib/types";
import { EnvelopError } from "@envelop/core";

import { ReportResolutionInput, ReportSubmissionInput } from "@/graphql/types/report.types";

import { PaginatedRepository } from "./paginated.repository";

import { User } from "@/models/user";
import { Post } from "@/models/post";
import { Panel } from "@/models/panel";
import { Debate } from "@/models/debate";
import { Challenge } from "@/models/challenge";
import { Discussion } from "@/models/dicsussion";
import { Report, ReportModel, ReportResolution, ReportResolutionDecision } from "@/models/report";

import { UserRepository } from "./user.repository";
import { DiscussionRepository } from "./discussion.repository";


@Service()
export class ReportRepository extends PaginatedRepository<Report, typeof Report> {
  protected model = ReportModel;

  @Inject() private readonly discussions: DiscussionRepository;
  @Inject() private readonly users: UserRepository;

  public async submit(author: User, input: ReportSubmissionInput) {
    const existing = await this.one({ target: input.discussionId, "log.author": author });

    if (existing) {
      throw new EnvelopError('Users cannot report a discussion twice', { code: 'DUPLICATE' });
    }

    const discussion = await this.discussions
      .one({ _id: new Types.ObjectId(input.discussionId) }).exec();

    if (!discussion) {
      throw new EnvelopError('Discussion was not found', { code: 'NOT_FOUND' });
    }

    let report = await this.one({ target: input.discussionId });

    if (!report) {
      report = await this.create({ target: discussion._id } as Report);
    }

    this.validateReport(discussion, discussion.get('kind'), input.offenders);


    const offenders = await this.users.find({ _id: { $in: input.offenders } });

    report.log.push({
      author,
      offenders,
      reasons: input.reasons,
      description: input.description
    });

    await report.save();

    return true;
  }
  
  public async resolve(id: string, decisions: ReportResolutionInput[]) {
    const report = await this.get(id);

    const resolution = await Promise.all(
      decisions.map(async d => ({ ...d, user: await this.users.get(d.user) }))
    );

    await this.punish(report, resolution);
    
    report.resolution = resolution;
    report.resolved = true;

    return await report.save();
  }

  private async punish(report: Document<Types.ObjectId, BeAnObject, any> & Report, resolution: ReportResolution[]) {
    const offences = await Promise.all(
      resolution
        .filter(r => r.decision === ReportResolutionDecision.warning)
        .map(r => r.user)
        .map(async u => ({
          user: u,
          warnings: await this.count({
            'resolution.user': u, 
            'resolution.decision': ReportResolutionDecision.warning 
          })
        }))
    );

    const usersToBan = [
      ...offences
        .filter(offence => offence.warnings >= 2)
        .map(offence => offence.user.id),

      ...resolution
        .filter(r => r.decision === ReportResolutionDecision.ban)
        .map(r => r.user.id)
    ];

    // TODO: Add notification for user ban
    await Promise.all([
      // Block users
      this.users.update({ _id: { $in: usersToBan } }, { $set: { blocked: true } }),

      // Block discussion
      (resolution.some(r => r.decision !== ReportResolutionDecision.none)) &&
        this.discussions.updateById(report.target._id.toHexString(), { $set: { blocked: true } })
    ])
  }

  private validateReport(discussion: Discussion, type: string, offenders: string[]): void {
    let participants: string[];
    let offendersAreParticipants: boolean;

    switch (type) {
      case 'Debate':
        if (offenders.length > 2) {
          throw new EnvelopError('Too many users reported', { code: 'INVALID_REPORT'})
        }

        participants = ((discussion as unknown) as Debate).participants
          .map(p => p.user._id.toHexString());

        offendersAreParticipants = offenders
          .reduce((acc, offender) => acc && participants.includes(offender), true);

        if (!offendersAreParticipants) {
          throw new EnvelopError('Reported users are not participants of the debate', { code: 'INVALID_REPORT' });
        }
        break;
        
      case 'Panel':
        if (offenders.length > 10) {
          throw new EnvelopError('Too many users reported', { code: 'INVALID_REPORT' })
        }

        participants = ((discussion as unknown) as Panel).participants
          .map(p => p.user._id.toHexString());

        offendersAreParticipants = offenders
          .reduce((acc, offender) => acc && participants.includes(offender), true);

        if (!offendersAreParticipants) {
          throw new EnvelopError('Reported users are not participants of the debate', { code: 'INVALID_REPORT' });
        }
        break;
        
      case 'Post':
      case 'Challenge':
        if (offenders.length !== 1) {
          throw new EnvelopError('Incorrect number of reported users', { code: 'INVALID_REPORT' });
        }

        if (offenders[0] !== ((discussion as unknown) as Post | Challenge).author._id.toHexString()) {
          throw new EnvelopError('Reported user is not the author', { code: 'INVALID_REPORT' });
        }
        break;
    }
  }
}