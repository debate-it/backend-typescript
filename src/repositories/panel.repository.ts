import { Types } from "mongoose";
import Container, { Inject, Service } from "typedi";
import { DocumentType, isRefType, mongoose, Ref } from "@typegoose/typegoose";
import { EnvelopError } from "@envelop/core";

import { config } from "@/config";

import { User } from "@/models/user";
import { OpenRequest } from "@/models/request";
import { EventTrigger, EventType } from "@/models/event";
import { SchedulableType, Schedule } from "@/models/schedule";
import { DiscussionVote, VoteTargetModel} from '@/models/vote';
import { Panel, PanelModel, PanelStatus } from "@/models/panel";
import { DiscussionCategory  } from "@/models/dicsussion/discussion.category";

import { DiscussionInput } from "@/graphql/types";

import { DiscussionRepository } from "./discussion.repository";
import { PaginatedRepository } from "./paginated.repository";
import { EventRepository } from "./event.repository";
import { VoteRepository } from "./vote.repository";
import { UserRepository } from "./user.repository";

import { SearchService } from "@/services/search.service";
import { ScheduleService } from "@/services/schedule.service";

interface PanelParticipant {
  user: Ref<User>;
  argument: string;
}

@Service()
export class PanelRepository extends PaginatedRepository<Panel, typeof Panel> {
  protected readonly model = PanelModel;

  @Inject() private readonly discussions: DiscussionRepository;
  @Inject() private readonly events: EventRepository;
  @Inject() private readonly votes: VoteRepository;

  @Inject(() => ScheduleService)
  private readonly scheduler: ScheduleService;

  @Inject() 
  private readonly search: SearchService;

  public async initiate(input: DiscussionInput, creator: PanelParticipant) {
    return await this.model.create({
      ...(await this.discussions.start(input)),
      participants: [ creator ]
    });
  }

  public async createFromOpenRequest(request: OpenRequest, participants: PanelParticipant[]) {
    const panel = await this.model.create({
      topic: request.info.topic,
      size: request.info.size,
      category: request.info.category,
      tags: request.info.tags,

      status: PanelStatus.active,
      deadline: this.getDeadline(),

      participants: participants
    });

    await this.bootstrap(panel);

    return panel;
  }

  public async addArgument(panel: Ref<Panel>, user: User, argument: string) {
    await this.model.findByIdAndUpdate(
      isRefType(panel, mongoose.Types.ObjectId) ? panel : panel.id,
      { $push: { participants: { user, argument } } }
    );
  }

  public async vote(id: string, voter: User, participant: number) {
    const panel = await this.get(id);

    if (!panel) {
      throw new EnvelopError('Panel not found', { code: 'NOT_FOUND' });
    }

    if (panel.status !== PanelStatus.active) {
      throw new EnvelopError('Panel is not active and cannot be votes on', { code: 'CANNOT_VOTE'});
    }

    const isParticipant = panel.participants
      .reduce((acc, e) => acc || (e.user as User).id === voter.id, false);

    if (isParticipant) {
      throw new EnvelopError('Panel participants cannot vote on their panel', { code: 'CANNOT_VOTE' });
    }

    if (participant >= panel.participants.length) {
      throw new EnvelopError('User you are voting for is not participant of this panel', { code: 'CANNOT_VOTE' });
    }

    return this.votes.panel(panel, voter, participant);
  }

  public async start(id: Types.ObjectId) {
    const panel = await this.model.findByIdAndUpdate(
      id, 
      { 
        $set: { 
          status: PanelStatus.active, 
          created: new Date(),
          deadline: this.getDeadline()
        }
      },
      { new: true });

    await this.bootstrap(panel);
  }

  public async findUserVote(panel: Panel, user: User) {
    const vote = await this.votes.one({
      voter: user.id,
      target: panel.id,
      targetModel: VoteTargetModel.Panel,
    }) as DocumentType<DiscussionVote>;

    return vote?.participant;
  }

  private async bootstrap(panel: DocumentType<Panel>) {
    const event = {
      type: EventType.PanelStarted,
      trigger: EventTrigger.Panel,
      entity: panel
    };

    await Promise.all(
      panel.participants
        .map(participant => participant.user)
        .map((target: User) => this.events.add({ target, ...event }))
    );

    const schedule: Schedule = {
      entity: panel,
      type: SchedulableType.Panel,
      updateAt: panel.deadline
    };

    await Promise.all([
      this.search.addDiscussions([ panel ]),
      this.scheduler.create(schedule, () => this.finish(panel.id)),
      this.discussions.updateDiscussionStat((panel.category as DiscussionCategory).id)
    ]);
  }

  public async finish(id: string) {
    const panel = await this.get(id);

    // Copy participant list to avoid sort in-place
    const topParticipants = [...panel.participants]
      .filter(participant => participant.score > 0)
      .sort((a, b) => b.score - a.score);

    // Determine the winner only when there's at least one vote
    if (topParticipants.length > 0) {
      const winningScore = topParticipants[0].score;
      const winners = topParticipants
        .filter(participant => participant.score === winningScore);

      const points = (panel.participants.length * 10) / winners.length;

      for (const winner of winners) {
        await Container.get(UserRepository)
          .updateAfterWinning(winner.user as DocumentType<User>, points);
      }
    }

    const event = {
      type: EventType.PanelFinished,
      trigger: EventTrigger.Panel,
      entity: panel
    };

    await Promise.all(
      panel.participants
        .map(p => this.events.add({ target: p.user as User, ...event }))
        .concat(this.scheduler.cancel({ entity: panel, type: SchedulableType.Panel }))
    );

    panel.status = PanelStatus.finished;

    await panel.save();
  }

  private getDeadline(): Date {
    return new Date(Date.now() + config.constants.dayInSeconds * 3);
  }
}