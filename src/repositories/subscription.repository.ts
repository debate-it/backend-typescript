import { Inject, Service } from "typedi";
import { AnyParamConstructor, DocumentType } from "@typegoose/typegoose/lib/types";
import { EnvelopError } from "@envelop/core";

import { User } from "@/models/user";
import { DiscussionCategory } from "@/models/dicsussion";

import { SubscriptionCacheService } from '@/services/subscription.cache.service';

import {
  Subscription,
  SubscriptionModel,
  SubscriptionTargetModel
} from "@/models/subscription";

import { Repository } from "./base.repository";
import { UserRepository } from "./user.repository";
import { CategoryRepository } from "./category.repository";
import { PaginatedRepository } from "./paginated.repository";
import { EventRepository } from "./event.repository";
import { EventTrigger, EventType } from "@/models";

type RepoType<T> = AnyParamConstructor<T>;

interface SubscriptionPayload<T, U extends RepoType<T>> {
  id: string;
  type: SubscriptionTargetModel;

  follower: User;
  repository: Repository<T, U>;

  update?: (target: DocumentType<InstanceType<U>>, record?: Subscription) => Promise<void>
  validate?: (follower: User, record?: Subscription) => void
}

@Service()
export class SubscriptionRepository extends PaginatedRepository<Subscription, typeof Subscription> {
  protected readonly model = SubscriptionModel;

  @Inject() private readonly users: UserRepository;
  @Inject() private readonly events: EventRepository;
  @Inject() private readonly categories: CategoryRepository;

  @Inject() private readonly cache: SubscriptionCacheService;

  public async followUser(user: User, targetId: string) {
    const payload: SubscriptionPayload<User, typeof User> = {
      id: targetId,
      type: SubscriptionTargetModel.User,

      follower: user,
      repository: this.users,

      update: (target, record) => this.users.updateAfterFollowing(target, record),
      validate: (follower, record?) => {
        if (record && record.subscription === follower._id) {
          throw new EnvelopError('Users cannot follow themselves', { code: 'CANNOT_FOLLOW' });
        }
      }
    }


    return this.follow(payload);
  }

  public async followCategory(user: User, targetId: string) {
    const payload: SubscriptionPayload<DiscussionCategory, typeof DiscussionCategory> = {
      id: targetId,
      type: SubscriptionTargetModel.Category,

      follower: user,
      repository: this.categories,
      update: (target, record) => this.categories.updateAfterFollowing(target, record)
    }

    return this.follow(payload);

  }

  private async follow<T, U extends RepoType<T>>(payload: SubscriptionPayload<T, U>): Promise<U> {
    const target = await payload.repository.get(payload.id) as DocumentType<InstanceType<U>>;

    if (!target) {
      switch (payload.type) {
        case SubscriptionTargetModel.User:
          throw new EnvelopError('Subscription user not found', { code: 'NOT_FOUND' });
        case SubscriptionTargetModel.Category:
          throw new EnvelopError('Subscription category not found', { code: 'NOT_FOUND' });
      }
    }

    const info = {
      subscription: target.id,
      subscriptionModel: payload.type,
      follower: payload.follower
    };

    const record = await this.model.findOne(info);

    payload.validate?.(payload.follower, record);


    if (record && record.active) {
      // Remove subscription and delete from Redis cache
      await Promise.all([
        this.model.findByIdAndUpdate(record.id, { $set: { active: false } }),
        this.cache.deleteSubscription(payload.follower, record),
      ]);
    } else if (record) {
      await Promise.all([
        this.model.findByIdAndUpdate(record.id, { $set: { active: true } }),
        this.cache.updateSubscriptions(payload.follower, record)
      ]);
    } else {
      // Create subscription record and add to Redis cache
      await this.model
        .create(info)
        .then(subscription => this.cache.updateSubscriptions(payload.follower, subscription));
    }

    await Promise.all([
      payload.update?.(target, record),
      payload.type === SubscriptionTargetModel.User && !record &&
        this.events.add({
          source: payload.follower,
          target: target.toObject() as DocumentType<User>,
          type: EventType.UserFollow,
          trigger: EventTrigger.User,
          entity: payload.follower
        })
    ]);


    return target as U;
  }
}