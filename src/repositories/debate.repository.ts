import { Publisher } from 'type-graphql';
import Container, { Inject, Service } from 'typedi';
import { DocumentType } from '@typegoose/typegoose';
import { EnvelopError } from '@envelop/core';

import { config } from '@/config';

import { DebateRealtimeArgumentResponse } from '@/graphql/types';

import { User } from '@/models/user';
import { Comment } from '@/models/comment';
import { Challenge } from '@/models/challenge';
import { DebateRequest } from '@/models/request';
import { SchedulableType } from '@/models/schedule';
import { EventTrigger, EventType } from '@/models/event';
import { DiscussionCategory } from '@/models/dicsussion';
import { DiscussionVote, VoteTargetModel } from '@/models/vote';
import { Debate, DebateModel, DebateStatus } from '@/models/debate';
import { OpenRequest, RequestBase, RequestCandidacy } from '@/models/request';

import { SearchService } from '@/services/search.service';
import { ScheduleService } from '@/services/schedule.service';

import { UserRepository } from './user.repository';
import { VoteRepository } from './vote.repository';
import { EventRepository } from './event.repository';
import { PaginatedRepository } from './paginated.repository';
import { DiscussionRepository } from './discussion.repository';

import { WordLimit } from '@/utils/word.limit';


@Service()
export class DebateRepository extends PaginatedRepository<Debate, typeof Debate> {
  protected readonly model = DebateModel;

  @Inject() private readonly votes: VoteRepository;
  @Inject() private readonly events: EventRepository;
  @Inject() private readonly discussions: DiscussionRepository;

  @Inject() private readonly search: SearchService;
  
  @Inject(() => ScheduleService) 
  private readonly scheduler: ScheduleService;

  public async createFromRequest(request: DebateRequest) {
    return await this.createDebate(request.info, [request.initiator as User, request.recipient as User] as const);
  }

  public async createFromOpenRequest(request: OpenRequest, candidacy: RequestCandidacy) {
    return await this.createDebate(request.info, [request.initiator as User, candidacy.candidate as User] as const);
  }

  public async createFromChallenge(challenge: Challenge, response: Comment) {
    return await this.createDebate(challenge, [challenge.author as User, response.author as User] as const);
  }

  private async createDebate(discussion: RequestBase, participants: readonly [User, User]) {
    const debate = await this.create({
      topic: discussion.topic,
      category: discussion.category,
      size: discussion.size,
      tags: discussion.tags,

      status: DebateStatus.active,

      participants: participants.map((user) => ({ user }))
    } as Debate);

    const scheduleInfo = {
      entity: debate,
      updateAt: new Date(debate.deadline),
      type: SchedulableType.Debate,
    };

    await Promise.all([
      this.search.addDiscussions([debate]),
      this.discussions.updateDiscussionStat((debate.category as DiscussionCategory).id),
    ]);

    return debate;
  }

  public async addArgument(id: string, author: User, argument: string, publish?: Publisher<DebateRealtimeArgumentResponse>) {
    const debate = await this.get(id);

    if (!debate) {
      throw new EnvelopError('Debate not found', { code: 'NOT_FOUND' });
    }

    const participant = debate.participants
      .map(p => p.user as User)
      .findIndex(user => user.id === author.id);

    if (participant < 0) {
      throw new EnvelopError('Non-participants can\'t submit arguments', { code: 'CANNOT_SUBMIT_ARGUMENT' });
    }

    if (participant != debate.currentTurn) {
      throw new EnvelopError('Wait for your turn to submit an argument', { code: 'CANNOT_SUBMIT_ARGUMENT' });
    }

    if (debate.status !== DebateStatus.active) {
      throw new EnvelopError('Debate is not active, arguments cannot be submitted', { code: 'CANNOT_SUBMIT_ARGUMENT'});
    }

    WordLimit.validateArgument(argument, debate.size);

    debate.arguments.push({ participant, argument });

    if (debate.arguments.length / 2 === debate.numberOfRounds) {
      debate.status = DebateStatus.voting;
      debate.deadline = new Date(Date.now() + config.constants.dayInSeconds);

      const scheduleInfo = {
        entity: debate,
        updateAt: debate.deadline,
        type: SchedulableType.Debate
      };

      await this.scheduler.create(scheduleInfo, () => this.finishDebate(debate));
    }

    await Promise.all([
      debate.save(),
      this.events.add({ 
        source: author,
        target: debate.participants[debate.currentTurn].user as User,
        type: EventType.DebateArgumentAdded,
        trigger: EventTrigger.Debate,
        entity: debate
      }),
    ])
    
    if (publish) {
      publish({ 
        index: debate.arguments.length - 1,
        debate: debate.id,
        argument: { participant, argument, time: new Date(Date.now()) },
      });
    }

    return debate;
  }

  public async vote(id: string, participant: number, voter: User): Promise<Debate> {
    const debate = await this.get(id);

    if (!debate) {
      throw new EnvelopError('Debate not found', { code: 'NOT_FOUND' });
    }

    const isParticipant = !!debate.participants
      .find(p => (p.user as User).id === voter.id);

    if (isParticipant) {
      throw new EnvelopError('Participants can\'t vote on their debate', { code: 'CANNOT_VOTE' });
    }

    if (debate.status === DebateStatus.finished) {
      throw new EnvelopError('Can\'t vote on finished debate', { code: 'CANNOT_VOTE' });
    }

    return this.votes.debate(debate, voter, participant);
  }

  public async findUserVote(debate: Debate, user: User) {
    const vote = await this.votes.one({
      voter: user.id,
      target: debate.id,
      targetModel: VoteTargetModel.Debate
    }) as DocumentType<DiscussionVote>;

    return vote?.participant;
  }

  public async updateDebate(id: string) {
    const debate = await this.get(id);

    await this.finishDebate(debate);
  }

  private async finishDebate(debate: DocumentType<Debate>) {
    // Copy the participant list to avoid sort in-place
    const [winner, loser] = [...debate.participants]
      .sort((a, b) => b.score - a.score);

    if (winner.score !== loser.score) {
      await Container.get(UserRepository)
        .updateAfterWinning(winner.user as DocumentType<User>);
    }

    const event = { 
      type: EventType.DebateFinished,
      trigger: EventTrigger.Debate,
      entity: debate 
    };

    await Promise.all([ 
      this.events.add({ target: winner.user as User, ...event }),
      this.events.add({ target: loser.user as User, ...event }),
      this.scheduler.done({ entity: debate, type: SchedulableType.Debate })
    ])

    debate.status = DebateStatus.finished;
    debate.save();
  }
}