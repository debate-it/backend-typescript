import * as scheduler from 'node-schedule';

import { Types } from 'mongoose';
import Container, { Service } from 'typedi';
import { DocumentType, isDocument, Ref } from '@typegoose/typegoose';

import { PanelRequest } from '@/models/request';
import { Discussion } from '@/models/dicsussion';
import { SchedulableType, Schedule, ScheduleModel } from '@/models/schedule';

import { PanelRepository } from '@/repositories/panel.repository';
import { DebateRepository } from '@/repositories/debate.repository';
import { RequestRepository } from '@/repositories/request.repository';
import { ChallengeRepository } from '@/repositories/challenge.repository';


@Service()
export class ScheduleService {
  private readonly model = ScheduleModel;

  public async initialize() {
    // find all scheduled/late jobs
    const records = await this.model.find();

    // convert related object to id
    const convert = (entity: Ref<Discussion | PanelRequest>) =>
      isDocument(entity) ? entity._id.toHexString() : (entity as Types.ObjectId).toHexString();

    for (const record of records) {
      switch (record.type) {
        case SchedulableType.Challenge:
          this.schedule(record, () => 
            Container
              .get(ChallengeRepository)
              .updateChallenge(convert(record.entity)));
          break;

        case SchedulableType.Debate:
          this.schedule(record, () => 
            Container
              .get(DebateRepository)
              .updateDebate(convert(record.entity)));
          break;

        case SchedulableType.Panel:
          this.schedule(record, () =>
            Container
              .get(PanelRepository)
              .finish(convert(record.entity)));
          break;

        case SchedulableType.PanelRequest:
          const repository = Container.get(RequestRepository);

          const request = await repository
            .get(convert(record.entity), {}, { populate: 'panel' }) as DocumentType<PanelRequest>;

          this.schedule(record, () => repository
            .updatePanelExpiration(convert(request.panel), convert(request)));

          break;
      }
    }
  }


  public async create(info: Schedule, callback: scheduler.JobCallback) {
    const record = await this.model.create(info);

    this.schedule(record, callback);
  }

  private schedule(entry: Schedule, callback: scheduler.JobCallback): void | Promise<any> {
    const now = new Date(Date.now());

    if (entry.updateAt < now) {
      return callback(now);
    }

    scheduler.scheduleJob(this.getName(entry), entry.updateAt, callback);
  }

  public async reschedule(info: Schedule) {
    const record = await this.model.findOne({
      entity: info.entity,
      type: info.type
    });

    if (!record) {
      throw new Error('Job not found');
    }

    const job = scheduler.scheduledJobs[this.getName(record)];

    scheduler.rescheduleJob(job, info.updateAt);
  }

  public async done(info: Pick<Schedule, 'entity' | 'type'>) {
    await this.model.findOneAndDelete(info);
  }

  public async cancel(info: Pick<Schedule, 'entity' | 'type'>) {
    scheduler.cancelJob(this.getName(info));

    await this.done(info);
  }

  private getName(entry: Pick<Schedule, 'entity' | 'type'>) {
    return `${entry.type}-${(entry.entity as Discussion)?.id ?? entry.entity}`;
  }

}