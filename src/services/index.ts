export * from './auth.service';
export * from './schedule.service';

import Container from 'typedi';

import { SearchService } from './search.service';
import { LeaderboardService } from './leaderboard.service';
import { ScheduleService } from './schedule.service';
import { logger } from './log.service';


export namespace Services {
  export async function initialize() {
    const leaderboards = Container.get(LeaderboardService);
    const scheduler = Container.get(ScheduleService);
    const search = Container.get(SearchService);

    await Promise.all([
      search.initialize(),
      scheduler.initialize(),
      leaderboards.initialize(),
    ]);

    logger.info({ message: 'Services are initialized' })
  }
}