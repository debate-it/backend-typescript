import { Redis } from "ioredis";
import Container, { Service } from "typedi";
import { Types } from "mongoose";

import { RedisService } from "./redis.service";

import { User } from "@/models/user";
import { DiscussionCategory } from "@/models/dicsussion";
import { Subscription, SubscriptionTargetModel } from "@/models/subscription";

@Service()
export class SubscriptionCacheService {

  private readonly redis: Redis = Container.get<Redis>(RedisService.token);
  
  public async createSubscriptionCache(user: User, subscriptions: Subscription[]) {
    const keys = this.getSubscriptionKeys(user);

    await this.redis.pipeline()
      .sadd(keys.categories, subscriptions
        .filter(s => s.subscriptionModel === SubscriptionTargetModel.Category)
        .map(s => s.subscription._id.toHexString()))

      .sadd(keys.users, subscriptions
        .filter(s => s.subscriptionModel === SubscriptionTargetModel.User)
        .map(s => s.subscription._id.toHexString())) 

      .expire(keys.categories, 15 * 60)
      .expire(keys.users, 15 * 60)
      .exec();
  }

  public async updateSubscriptions(user: User, subscription: Subscription) {
    const keys = this.getSubscriptionKeys(user);

    const id = subscription.subscription instanceof Types.ObjectId 
      ? subscription.subscription 
      : subscription.subscription._id as Types.ObjectId;

    switch (subscription.subscriptionModel) {
      case SubscriptionTargetModel.User:
        await this.redis.sadd(keys.users, id.toHexString())
        break
        
      case SubscriptionTargetModel.Category:
        await this.redis.sadd(keys.categories, id.toHexString())
        break;
    }
  }

  public async deleteSubscription(user: User, subscription: Subscription) {
    const keys = this.getSubscriptionKeys(user);

    const id = subscription.subscription instanceof Types.ObjectId 
      ? subscription.subscription 
      : subscription.subscription._id as Types.ObjectId;

    switch (subscription.subscriptionModel) {
      case SubscriptionTargetModel.User:
        await this.redis.srem(keys.users, id.toHexString())
        break
        
      case SubscriptionTargetModel.Category:
        await this.redis.srem(keys.categories, id.toHexString())
        break;
    }
  }

  public async getSubscriptions(user: User) {
    const keys = this.getSubscriptionKeys(user);

    const results = await this.redis.pipeline()
      .exists(keys.categories)
      .exists(keys.users)
      .smembers(keys.categories)
      .smembers(keys.users)
      .expire(keys.categories, 15 * 60)
      .expire(keys.users, 15 * 60)
      .exec();

    const [categoriesExist, usersExist] = [
      this.resolveErrors<number>(results[0]),
      this.resolveErrors<number>(results[1]) 
    ]

    const [categories, users] = [
      this.resolveErrors<string[]>(results[2]),
      this.resolveErrors<string[]>(results[3]) 
    ]; 

    if (!categoriesExist && !usersExist) {
      return null;
    }
 
    return { 
      categories: categories.map(x => new Types.ObjectId(x)),
      users: categories.map(x => new Types.ObjectId(x))
    };
  }

  private resolveErrors<T>(results: [Error, any]) {
    return results[1] as T;
  }

  private getSubscriptionKeys(user: User) {
    const prefix = `subscriptions:${user.id}`;

    return {
      categories: `${prefix}:categories`,
      users: `${prefix}:users`
    }
  }
}