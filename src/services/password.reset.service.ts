import { v4 as uuid } from 'uuid';
import { createHash } from 'crypto';
import { Inject, Service } from "typedi";
import { EnvelopError } from '@envelop/core';

import { User } from "@/models/user";
import { PasswordResetModel } from "@/models/util/password.reset";

import { config } from "@/config";

import { MailService } from "./mail.service";

@Service()
export class PasswordResetService {
  private readonly model = PasswordResetModel;

  @Inject()
  private readonly mail: MailService;

  async create(user: User) {
    const id = uuid();
    const hash = this.hash(id);

    await this.model.create({ user, hash });
    await this.mail.sendPasswordReset(user, id);

    return true;
  }

  async get(id: string) {
    await this.verify(id);

    return await this.model.findOne({ hash: this.hash(id) }).populate('user');
  }

  async verify(id: string) {
    const hash = this.hash(id);
    const record = await this.model.findOne({ hash });

    if (!record) {
      throw new EnvelopError('Could not find password reset request', { code: 'NOT_FOUND' });
    }

    if (record.activated) {
      throw new EnvelopError('Password reset request already activated', { code: 'ALREADY_ACTIVATED' });
    }

    const expired = Math.abs(Date.now() - record.date.getTime()) > config.constants.passwordResetTimeout;

    if (expired) {
      throw new EnvelopError('Password reset request already expired', { code: 'EXPIRED' });
    }

    return true;
  }

  private hash(id: string) {
    return createHash('sha256').update(id).digest('base64');
  }
}

