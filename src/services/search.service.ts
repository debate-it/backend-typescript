import * as base64 from 'base64-url';

import { DocumentType } from '@typegoose/typegoose'

import { Service } from 'typedi';
import { MeiliSearch, SearchParams } from 'meilisearch';

import { config } from '@/config';

import { PaginationInfo } from "@/graphql/types/pagination.types";

import { User } from '@/models/user';
import { Discussion, DiscussionCategory, DiscussionTag } from '@/models/dicsussion';
import { UserFilters } from '@/graphql/types/user.types';


export enum SearchIndex {
  Discussions = 'search-discussions',
  Users = 'search-users'
}

export const client = new MeiliSearch({ host: config.search.host, apiKey: config.search.key });

interface SearchSettings {
  limit: number;
  offset: number;
}

@Service()
export class SearchService {
  async initialize() {
    await client
      .index(SearchIndex.Discussions)
      .updateSettings({
        // displayedAttributes: ['id'],
        searchableAttributes: [ 'topic', 'tags', 'category', 'size' ],
        filterableAttributes: ['blocked', 'category', 'tags', 'size'],
        sortableAttributes: ['created']
      });

    await client
      .index(SearchIndex.Users)
      .updateSettings({
        distinctAttribute: 'username',
        // displayedAttributes: ['id'],
        searchableAttributes: ['username', 'fullname' ],
        filterableAttributes: [
          'blocked',
          'username',
          'fullname',

          'info.country',
          'info.education',
          'info.occupation',
          'info.politics',
          'info.religion',
          'info.level',
          'info.category'
        ]
      });
  }

  async clean() {
    if (process.env.NODE_ENV !== 'production') {
      await Promise.all([
        client.deleteIndex(SearchIndex.Discussions),
        client.deleteIndex(SearchIndex.Users)
      ]);
    }
  }

  async addUsers(users: DocumentType<User>[]) {
    const documents = users
      .map(user => user.toObject<User>({ virtuals: true }))
      .map(user => {
        const fullname = `${user.info?.firstName ?? ''} ${user.info?.lastName ?? ''}`.trim() || null;
        const bestCategory = (user.stat.bestCategory as DiscussionCategory)?.name;

        const result = {
          id: user.id,
          blocked: user.blocked,

          username: user.username,
          fullname: fullname,

          info: user.info,

          stat: { ...user.stat, bestCategory }
        }

        return result;
      })

    await client
      .index(SearchIndex.Users)
      .addDocuments(documents);
  }

  async addDiscussions(discussions: DocumentType<Discussion>[]) {
    const documents = discussions
      .map(discussion => discussion.toObject<Discussion>({ virtuals: true }))
      .map(discussion => {
        const result = {
          id: discussion.id,
          topic: discussion.topic,
          size: discussion.size,
          category: (discussion.category as DiscussionCategory).name,
          tags: discussion.tags.map(tag => (tag as DiscussionTag)?.name)
        };

        return result;
      });

    await client
      .index(SearchIndex.Discussions)
      .addDocuments(documents);
  }


  async discussion(query: string, pagination: PaginationInfo, options: SearchParams = {}) {
    return this.search(SearchIndex.Discussions, query, pagination, options);
  }
  
  async users(query: string, pagination: PaginationInfo, options: SearchParams = {}) {
    return this.search(SearchIndex.Users, query, pagination, options);
  }

  async filterOpponents(user: User, filters: UserFilters, pagination: PaginationInfo) {
    const defaultFilters = [
      `blocked != true`,
      `(NOT username = ${user.username})`,
      `(NOT info.level < ${user.progress.level})`
    ];

    const propertyFilters = Object.keys(filters)
      .filter(f => !!f)
      .map(f => `info.${f} = '${filters[f]}'`);

    const settings = this.getPaginationSettings(pagination);

    const filter = defaultFilters
      .concat(propertyFilters)
      .join(' AND ');

    const result = await client
      .index(SearchIndex.Users)
      .search(null, { ...settings, filter });

    return {
      hasMore: result.hits.length > pagination.limit,
      next: this.createCursor(settings),
      page: result.hits.slice(0, -2)
    };
  }

  public async search(index: SearchIndex, query: string, pagination: PaginationInfo, options: SearchParams = {}) {
    const settings = this.getPaginationSettings(pagination);

    const result = await client
      .index(index)
      .search(query, { ...settings, ...options });


    return {
      hasMore: result.hits.length > pagination.limit,
      next: this.createCursor(settings),
      page: result.hits
    };
  }

  private createCursor(settings: SearchSettings) {
    return base64.encode(JSON.stringify({ offset: settings.offset + settings.limit }));
  }

  private getPaginationSettings(pagination: PaginationInfo): SearchSettings {
    const limit = pagination.limit + 1;

    if (!pagination.cursor) {
      return { limit, offset: 0 };
    }

    const { offset } = JSON.parse(base64.decode(pagination.cursor))

    return { offset, limit };
  }
}