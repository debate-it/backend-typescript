import Redis, { RedisOptions } from 'ioredis';
import Container, { Token } from 'typedi';

import { config } from '@/config';

const RedisOptions: RedisOptions = {
  retryStrategy: times => Math.max(times * 100, 3000),
}

export namespace RedisService {
  export const token = new Token('REDIS_CLIENT_TOKEN');

  export function create() {
    return new Redis(config.redis.url, RedisOptions)
  }

  export function initialize() {
    Container.set({ id: token, value: create() });
  }
}

RedisService.initialize();

