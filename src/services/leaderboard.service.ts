import base64 from 'base64-url';
import * as scheduler from 'node-schedule';

import { Redis } from "ioredis";
import Container, { Inject, Service } from "typedi";

import { User } from "@/models/user";
import { Discussion } from "@/models/dicsussion";

import { UserRepository } from "@/repositories/user.repository";
import { DiscussionRepository } from "@/repositories/discussion.repository";

import { RedisService } from "./redis.service";
import { LeaderboardPagination } from '@/graphql/types/leaderboard.types';


export enum Leaderboard {
  Users,
  Posts,
  Panels,
  Debates,
  Challenges
}

export type LeaderboardEntry = Discussion | User;

interface RecordMap {
  [id: string]: Discussion | User
}

@Service()
export class LeaderboardService {
  private readonly redis: Redis = Container.get<Redis>(RedisService.token);

  @Inject() private readonly discussions: DiscussionRepository;
  @Inject() private readonly users: UserRepository;

  private readonly PREFIX = 'leaderboard';
  private readonly KEYS = Object.freeze({
    [Leaderboard.Users]: `${this.PREFIX}:users`,
    [Leaderboard.Posts]: `${this.PREFIX}:posts`,
    [Leaderboard.Panels]: `${this.PREFIX}:panels`,
    [Leaderboard.Debates]: `${this.PREFIX}:debates`,
    [Leaderboard.Challenges]: `${this.PREFIX}:challenges`
  });

  async initialize() {
    scheduler.scheduleJob({ hour: 0, minute: 0 }, () => this.clear());
  }

  public async clear() {
    await this.redis.pipeline()
      .del(this.KEYS[Leaderboard.Users])
      .del(this.KEYS[Leaderboard.Posts])
      .del(this.KEYS[Leaderboard.Panels])
      .del(this.KEYS[Leaderboard.Debates])
      .del(this.KEYS[Leaderboard.Challenges])
      .exec();
  }

  public async get(leaderboard: Leaderboard, cursor?: string, size: number = 20): Promise<LeaderboardPagination> {
    const { start } = cursor ? JSON.parse(base64.decode(cursor)) : { start: 0 };

    const [entries, cardinality] = await this.redis.pipeline()
      .zrevrange(this.KEYS[leaderboard], start, start + size, 'WITHSCORES')
      .zcard(this.KEYS[leaderboard])
      .exec()
      .then(values => values.map(v => v[1]) as [string[], number]);

    const [ids, scores] = this.separateResult(entries);

    const records: LeaderboardEntry[] = leaderboard == Leaderboard.Users
      ? await this.users.find({ _id: { $in: ids } })
      : await this.discussions.find({ _id: { $in: ids } });

    const recordMap = records
      .reduce<RecordMap>((acc: RecordMap, e) => ({ ...acc, [e.id]: e }), {} as RecordMap);

    const hasMore = cardinality > start + size;
    const next = hasMore ? base64.encode(JSON.stringify({ start: start + size + 1 })) : null;

    const page = ids.map((entry, idx) => ({ entry: recordMap[entry], score: Number.parseInt(scores[idx]) }));

    return { hasMore, next, page };
  }

  public add(leaderboard: Leaderboard, entry: LeaderboardEntry, increment: number) {
    this.redis.zincrby(this.KEYS[leaderboard], increment, entry.id);
  }

  private separateResult(entries: string[]) {
    const predicate = (n: 0 | 1) => (acc: string[], e: string, idx: number) => {
      if (idx % 2 === n) {
        acc.push(e);
      }

      return acc;
    }

    const ids = entries.reduce(predicate(0), []);
    const scores = entries.reduce(predicate(1), []);

    return [ids, scores] as const;
  }

}
