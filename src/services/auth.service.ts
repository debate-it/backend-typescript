import * as argon from 'argon2';
import * as jwt from 'jsonwebtoken';

import { EnvelopError } from '@envelop/core';
import { DocumentType } from '@typegoose/typegoose';
import { Inject, Service } from 'typedi';
import { randomBytes } from 'crypto';

import { config } from '@/config';

import { User } from '@/models/user';
import { UserRole } from '@/models/user/user.role';

import { UserRepository } from '@/repositories/user.repository';
import { AuthResponse, AuthTokens, LoginInfo, SignUpInfo } from '@/graphql/types/auth.types';

import { MailService } from './mail.service';
import { SearchService } from './search.service';
import { PasswordResetService } from './password.reset.service';


interface TokenPayload {
  id: string;
  roles?: string[];
}

@Service()
export class AuthService {
  @Inject() private readonly users: UserRepository;

  @Inject() private readonly passwordReset: PasswordResetService;
  @Inject() private readonly search: SearchService;
  @Inject() private readonly mail: MailService;

  public async login(payload: LoginInfo): Promise<AuthResponse> {
    const user = await this.users.one({ username: payload.username });

    if (!user) {
      throw new EnvelopError('User not found', { code: 'NOT_FOUND' });
    }

    const valid = await this.verify(payload.password, user.auth.password);

    if (!valid) {
      throw new EnvelopError('Invalid password', { code: 'INVALID_CREDENTIALS' });
    }

    return {
      active: user.auth.active,
      tokens: this.generateTokenPair(user.id, user.auth.roles),
      user
    };
  }

  public async authenticate(token: string): Promise<AuthResponse> {
    try {
      const { id } = this.decode(token);
      const user = await this.users.get(id);

      return {
        active: user.auth.active,
        tokens: {
          session: this.createSessionToken(user.id, user.auth.roles),
          refresh: token
        },
        user
      }
    } catch (err) {
      throw new EnvelopError('Authorization error', { code: 'AUTH_TOKEN_VERIFICATION' });
    }
  }

  public async signup(payload: SignUpInfo): Promise<AuthResponse> {
    await this.findDuplicates(payload.username, payload.email);

    const password = await this.hash(payload.password);
    const activationCode = this.generateActivationCode();

    const [firstName, ...rest] = payload.extra.fullname
      ? payload.extra.fullname.split(' ')
      : [null, null];

    const lastName = rest.join(' ');

    const user = await this.users.register({
      email: payload.email,
      username: payload.username,
      info: {
        firstName: firstName, lastName: lastName,
        about: payload.extra.about,
        country: payload.extra.country,
        education: payload.extra.education,
        occupation: payload.extra.occupation,
        politics: payload.extra.politics,
        religion: payload.extra.religion,
      },
      auth: {
        password,
        activationCode
      }
    } as User);

    await this.mail.sendActivationCode(user.email, activationCode);

    return {
      active: user.auth.active,
      tokens: this.generateTokenPair(user.id, user.auth.roles),
      user
    };
  }

  public async activate(user: DocumentType<User>, code: string) {
    if (user.auth.active) {
      throw new EnvelopError('User is already activated', { code: 'USER_ALREADY_ACTIVATED' });
    }

    if (user.auth.activationCode !== code) {
      user.auth.activationAttempts -= 1;

      if (user.auth.activationAttempts <= 0) {
        throw new EnvelopError('Activation attemtps exceeded', { code: 'ACTIVATION_ERROR'});
      }

      throw new EnvelopError('Invalid activation code', { code: 'ACTIVATION_ERROR' });
    }

    user.auth.active = true;

    await Promise.all([ user.save(), this.search.addUsers([ user ]) ]);

    return true;
  }

  public async resendActivationCode(user: DocumentType<User>) {
    if (user.auth.activationResends == 0) {
      await this.users.delete(user.id);

      throw new EnvelopError('Activation code cannot be resent, user will be deleted', { code: 'ACTIVATION_ERROR' });
    }

    user.auth.activationCode = this.generateActivationCode();
    user.auth.activationResends -= 1;

    await user.save();
    await this.mail.sendActivationCode(user.email, user.auth.activationCode);

    return true;
  }

  public async changeEmail(user: User, email: string) {
    const duplicate = await this.users.one({ email });

    if (duplicate) {
      throw new EnvelopError('Email is already used', { code: 'CREDENTIAL_ALREADY_USED' });
    }

    await this.users.updateById(user.id, { email });
  }

  public async createPasswordChangeRequest(email: string) {
    const user = await this.users.one({ email });

    if (!user) {
      throw new EnvelopError('No user registered under this email address', { code: 'NOT_FOUND' });
    }

    return await this.passwordReset.create(user);
  }

  public async verifyPasswordChangeRequest(id: string) {
    return this.passwordReset.verify(id);
  }

  public async resetPassword(id: string, password: string) {
    const record = await this.passwordReset.get(id);

    const hash = await this.hash(password);
    const user = record.user as DocumentType<User>;
    
    if (!hash) {
      return false;
    }

    user.auth.password = hash;
    await user.save();

    return true;
  }

  public async changePassword(user: DocumentType<User>, password: string) {
    user.auth.password = await this.hash(password);

    await user.save();

    return true;
  }

  public async verifyUsername(username: string) {
    const record = await this.users.one({ username });

    if (record) {
      throw new EnvelopError(`Username '${username}' is already taken`, { code: 'CREDENTIAL_ALREADY_USED' });
    }

    return true;
  }

  public async verifyEmail(email: string) {
    const record = await this.users.one({ email });

    if (record) {
      throw new EnvelopError(`Email ${email} is already registered`, { code: 'CREDENTIAL_ALREADY_USED' });
    }

    return true;
  }

  public async findDuplicates(username: string, email: string) {
    const [usernameDuplicate, emailDuplicate] = await Promise.all([
      this.users.one({ username }),
      this.users.one({ email })
    ]);

    if (emailDuplicate) {
      throw new EnvelopError(`Email ${email} is already registered`, { code: 'CREDENTIAL_ALREADY_USED' });
    }

    if (usernameDuplicate) {
      throw new EnvelopError(`Username '${username}' is already taken`, { code: 'CREDENTIAL_ALREADY_USED' });
    }

    return true;
  }

  public async hash(password: string): Promise<string> {
    return argon.hash(password, { type: argon.argon2id });
  }

  public async verify(password: string, hash: string): Promise<boolean> {
    return argon.verify(hash, password, { type: argon.argon2id });
  }

  private generateTokenPair(id: string, roles: UserRole[] = []) {
    return {
      session: this.createSessionToken(id, roles),
      refresh: this.createRefreshToken(id, roles)
    };
  }

  private createSessionToken(id: string, roles: UserRole[]) {
    return this.createToken({ id, roles: roles.map(r => r.name) ?? [] });
  }

  private createRefreshToken(id: string, roles: UserRole[]) {
    return this.createToken({ id }, '60d');
  }

  private createToken(payload: object, expiresIn: string = '1d'): string {
    return jwt.sign(payload, config.secrets.JWT, { expiresIn, algorithm: 'HS512' });
  }

  public decode(token: string): TokenPayload {
    return jwt.verify(token, config.secrets.JWT, { algorithms: ['HS512'] }) as TokenPayload;
  }

  private generateActivationCode() {
    return randomBytes(3).toString('hex').toUpperCase();
  }
}