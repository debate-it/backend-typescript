import mailgun from 'mailgun.js';
import formData from 'form-data';

import { Service } from "typedi";

import { config } from "@/config";
import { User } from '@/models/user';

const MailGun = new mailgun(formData);



@Service()
export class MailService {
  private readonly domain = config.mailgun.domain;
  private readonly client = MailGun.client({ username: 'api', key: config.mailgun.api });

  async sendActivationCode(email: string, code: string) {
    const response = await this.client.messages.create(this.domain, {
      to: email,
      template: "activation",
      from: `Debate It <noreply@mail.debateit.io>`,
      subject: "Debate It User Activation",
      "h:X-Mailgun-Variables": `{"code": "${code}"}`
    });

    console.log(response);
  }

  async sendPasswordReset(user: User, id: string) {
    const link = `https://debateit.io/forgot?id=${id}`;
      
    const response = await this.client.messages.create(this.domain, {
      to: user.email,
      from: `Debate It <noreply@mail.debateit.io>`,
      subject: "Debate It Password Reset",
      template: "password-reset",
      "h:X-Mailgun-Variables": `{"reset-link": "${link}"}`,
    });

    console.log(response);
  }
}