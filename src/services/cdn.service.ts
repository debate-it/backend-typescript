import { Service } from "typedi";

import { v2 as cdn} from 'cloudinary';
import { User } from "@/models/user";
import { FileUpload } from "graphql-upload";

@Service()
export class CDNService {
  upload(user: User, file: FileUpload) {
    return new Promise<string>((resolve, reject) => {
      file
        .createReadStream()
        .pipe(cdn.uploader.upload_stream({}, (err, res) => {
          if (err) reject(err);
          else resolve(res.secure_url);
        }));
    })

  }
}