import firebase from 'firebase-admin';
import { Service } from "typedi";

import { config } from "@/config";

import { User } from "@/models/user";
import { EventRecord, EventType } from "@/models/event";

@Service()
export class FirebaseService {
  private firebase = firebase.initializeApp({
    credential: firebase.credential.cert(config.firebase.credentials)
  });


  async send(user: User, event: EventRecord) {
    if (!user.auth.firebaseToken) {
      return;
    } 

    const message: firebase.messaging.MessagingPayload = {
      notification: {
        title: event.title,
        body: event.message,
      },
      data: {
        link: event.link,
        type: this.mapEventType(event.type)
      }
    };

    await this.firebase.messaging()
      .sendToDevice(user.auth.firebaseToken, message);
  }

  private mapEventType(type: EventType): string {
    switch (type) {
      case EventType.DebateRequestSent:
        return 'DebateRequestSent'
      case EventType.DebateCreated: 
        return 'DebateCreated'
      case EventType.DebateArgumentAdded: 
        return 'DebateArgumentAdded'
      case EventType.DebateFinished: 
        return 'DebateFinished'

      case EventType.PanelRequestSent: 
        return 'PanelRequestSent'
      case EventType.PanelRequestAccepted: 
        return 'PanelRequestAccepted'
      case EventType.PanelStarted: 
        return 'PanelStarted'
      case EventType.PanelFinished: 
        return 'PanelFinished'

      case EventType.RequestRejected: 
        return 'RequestRejected'

      case EventType.ChallengeResponse: 
        return 'ChallengeResponse'
      case EventType.ChallengeFinished: 
        return 'ChallengeFinished';
      case EventType.ChallengeWinnerChosen: 
        return 'ChallengeWinnerChosen'

      case EventType.UserFollow: 
        return 'UserFollow'
        
      case EventType.UserLevelUp: 
        return 'UserLevelUp'

      case EventType.CommentCreated: 
        return 'CommentCreated'

      case EventType.Vote: 
        return 'Vote';

      case EventType.RequestCandidacySubmitted: 
        return 'RequestCandidacySubmitted'
    }
  }
}