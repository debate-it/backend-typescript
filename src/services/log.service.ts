import { Service } from 'typedi';
import { createLogger, transports } from 'winston';

import LokiTransport from 'winston-loki';

import { config } from '@/config';


export const logger = createLogger({
  
  transports: [
    new transports.Console(),
    new LokiTransport({
      host: config.monitor.loki.url,
      interval: 10,
      labels: { source: 'backend' },
    })
  ]
});

@Service()
export class LogService {
  public info(message: object) {
    logger.info(message);
  }

  public warn(message: object) {
    logger.warn(message);
  }
  
  public error(message: object) {
    logger.error(message);
  }
}