import { Inject, Service } from "typedi";

import { User } from "@/models/user";
import { SubscriptionTargetModel } from "@/models/subscription";
import { DebateStatus, Discussion, PanelStatus } from "@/models";

import { SubscriptionRepository } from "@/repositories/subscription.repository";
import { DiscussionRepository } from "@/repositories/discussion.repository";

import { PaginationInfo } from "@/graphql/types/pagination.types";

import { SubscriptionCacheService } from "./subscription.cache.service";
import { SearchService } from "./search.service";


@Service()
export class FeedService {
  @Inject() private readonly subscriptions: SubscriptionRepository;
  @Inject() private readonly discussions: DiscussionRepository;

  @Inject() private readonly cache: SubscriptionCacheService;

  @Inject() private readonly search: SearchService;

  public async fetch(user: User, page: PaginationInfo) {
    const query = await this.getQuery(user);
    return await this.discussions.paginate(query, { ...page, sortBy: 'created', sortDir: 'dsc' });
  }

  private async getQuery(user: User) {
    const subscriptions = await this.getSubscriptions(user);
    const users = [user._id, ...subscriptions.users];

    const query: any[] = [
      {
        'kind': 'Post',
        'author': { $in: users }
      },
      {
        'kind': 'Debate',
        'participants.user': { $in: users },
        'status': { $in: [ DebateStatus.voting, DebateStatus.finished ]}
      },
      {
        'kind': 'Panel',
        'participants.user': { $in: users },
        'status': { $in: [PanelStatus.active, PanelStatus.finished] }
      }
    ];

    if (subscriptions.categories.length > 0) {
      query.push(
        {
          'kind': 'Post',
          'category': { $in: subscriptions.categories }, 
        },
        {
          'kind': 'Debate',
          'category': { $in: subscriptions.categories }, 
          'status': { $in: [ DebateStatus.voting, DebateStatus.finished ]}
        },
        {
          'kind': 'Panel',
          'category': { $in: subscriptions.categories }, 
          'status': { $in: [PanelStatus.active, PanelStatus.finished] }
        }
      );
    }

    return { blocked: false, $or: query };
  }

  private async getSubscriptions(user: User) {
    const subscriptions = await this.cache.getSubscriptions(user);

    if (subscriptions) {
      return subscriptions;
    }

    const records = await this.subscriptions.find({ follower: user, active: true });

    await this.cache.createSubscriptionCache(user, records);

    return {
      categories: records
        .filter(r => r.subscriptionModel === SubscriptionTargetModel.Category)
        .map(r => r.subscription),

      users: records
        .filter(r => r.subscriptionModel === SubscriptionTargetModel.User)
        .map(r => r.subscription)
    };
  }
}
