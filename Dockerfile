FROM node:16-alpine 

LABEL org.opencontainers.image.title="Debate It Backend"
LABEL org.opencontainers.image.vendor="Debate It LLC"

WORKDIR /backend

# Copy package.json and intall all dependencies
COPY [ "package.json", "yarn.lock", "./"]
RUN ["yarn", "install"]

# Copy backend code and remove population module
COPY . ./

RUN ["npm", "run", "build"]

ENV NODE_ENV production
ENV TS_CONFIG_PATH=dist/tsconfig.json
ENV PORT 80

ENTRYPOINT ["node", "-r", "tsconfig-paths/register", "./dist/src/index.js"]
EXPOSE 80/tcp
